package com.kw.framework.gateway.filter;

import com.kw.framework.common.croe.constant.CommonConstant;
import com.kw.framework.gateway.utils.BuildHeaderFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import java.util.LinkedHashMap;
import java.util.UUID;

@Component
@Slf4j
public class HeaderFilter implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        // 封装需要向后续封装的header对象
        LinkedHashMap<String, String> headerMap = new LinkedHashMap<>();
        headerMap.put(CommonConstant.TRACE_ID, UUID.randomUUID().toString());
        exchange = BuildHeaderFilter.chainFilterAndSetHeaders(chain, exchange, headerMap);

        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }


}
