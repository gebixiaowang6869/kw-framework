package com.kw.framework.gateway;

import com.kw.framework.gateway.config.VersionServiceInstanceListSupplierConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClients;

/**
 * 在网关启动类使用注解@LoadBalancerClient指定哪些服务使用自定义负载均衡算法
 * 通过@LoadBalancerClient(value = "auth-service", configuration = VersionServiceInstanceListSupplierConfiguration.class)，对于auth-service启用自定义负载均衡算法；
 * 或通过@LoadBalancerClients(defaultConfiguration = VersionServiceInstanceListSupplierConfiguration.class)为所有服务启用自定义负载均衡算法。
 */
@LoadBalancerClients(defaultConfiguration = VersionServiceInstanceListSupplierConfiguration.class)
@SpringBootApplication
@EnableDiscoveryClient
public class Application {
    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);
    }
}
