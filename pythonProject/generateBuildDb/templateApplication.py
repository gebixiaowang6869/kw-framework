# encoding=utf-8

from jinja2 import Environment, BaseLoader
import pymysql
import datetime
import os
import re


def to_camelcase(text):
    arr = filter(None, text.lower().split('_'))
    res = ''
    j = 0
    for i in arr:
        if j == 0:
            res = i
        else:
            res = res + i[0].upper() + i[1:]
        j += 1
    return res

def doCursor(database,sql):
    db = pymysql.connect(host='gz-cynosdbmysql-grp-019tp05t.sql.tencentcdb.com',
                         port=26409,
                         user='root',
                         password='///520xxx',
                         database=database,
                         charset='utf8',
                         cursorclass=pymysql.cursors.DictCursor)
    try:
        cursor = db.cursor()
        sql = sql
        cursor.execute(sql)
        return cursor.fetchall()
    except Exception:
        print("sql 错误")

def readTableField(tableName):
    return doCursor("kw",f"SHOW FULL COLUMNS FROM {tableName}")

def readTableInfo(tableName):
    return doCursor("information_schema",f"SELECT * FROM information_schema.TABLES WHERE TABLE_NAME = '{tableName}' and table_schema = 'kw'")

def readAllTable():
    return doCursor("information_schema","SELECT * FROM information_schema.TABLES WHERE  table_schema = 'kw'")


def Initial_Letter_Lower_Or_Upper(my_str, is_upper=False):
    if is_upper is False:
        return my_str[:1].lower() + my_str[1:]
    elif is_upper is True:
        return my_str[:1].upper() + my_str[1:]


def changeFiledType(datas):
    for data in datas:
        type = data["Type"]
        if "int" in type:
            data["Type"] = "Integer"
        if "bigint" in type:
            data["Type"] = "Long"
        if "varchar" in type:
            data["Type"] = "String"
        if "char" in type:
            data["Type"] = "String"
        if "blob" in type:
            data["Type"] = "byte[]"
        if "text" in type:
            data["Type"] = "String"
        if "integer" in type:
            data["Type"] = "Integer"
        if "tinyint" in type:
            data["Type"] = "Integer"
        if "smallint" in type:
            data["Type"] = "Integer"
        if "mediumint" in type:
            data["Type"] = "Integer"
        if "bit" in type:
            data["Type"] = "Boolean"
        if "float" in type:
            data["Type"] = "Folat"
        if "double" in type:
            data["Type"] = "Double"
        if "decimal" in type:
            data["Type"] = "BigDecimal"
        if "boolean" in type:
            data["Type"] = "Boolean"
        if "date" in type:
            data["Type"] = "LocalDate"
        if "time" in type:
            data["Type"] = "LocalDateTime"
        if "datetime" in type:
            data["Type"] = "LocalDateTime"
        data["FieldLower"] = data["Field"]
        data["Field"] = to_camelcase(data["Field"])
        data["FieldUper"] = data["Field"][:1].upper() + data["Field"][1:]

    return datas


def create(jinja2Name, path, fileName, result, configuration):
    if not os.path.isdir(path):
        os.makedirs(path)
    with open(jinja2Name, mode="r", encoding="utf-8") as r:
        template_str = r.read()
        tpl = Environment(loader=BaseLoader).from_string(template_str)
        translateStr = tpl.render(datas=result, configuration=configuration)
        with open(path + fileName, mode="w", encoding="utf-8") as w:
            w.write(translateStr)


if __name__ == "__main__":
    tableCreateTime = "2022-11-09"
    tableDb = readAllTable()
    tables = []
    for table in tableDb:
        createTime = table["CREATE_TIME"].strftime("%Y-%m-%d")
        #if createTime == tableCreateTime:
        if table["TABLE_NAME"].startswith("sys_"):
            tables.append(table["TABLE_NAME"])
    projectName = "kw-framework-generateT"
    packageName = "generateT"
    workSpacePath = "D:\\doc\\createProjectTest\\"  # "D:\\workspace\\java\\pyTest\\" #"C:\\Users\\Administrator\\IdeaProjects\\kw-framework\\"
    basicPath = f"{workSpacePath}{projectName}\\{projectName}-service\\src\\main"
    path = basicPath + f"\\java\\com\\kw\\{packageName}\\service\\"
    mapperPath = basicPath + "\\resources\\mapper\\"
    commonPath = f"{workSpacePath}{projectName}\\{projectName}-common\\src\\main\\java\\com\\kw\\{packageName}\\common\\"
    for table in tables:
        tableName = table.capitalize().title().replace("_","")
        logInfo = readTableInfo(table)
        configuration = {
            "basePackage": f"com.kw.{packageName}.service",
            "commonPackage": f"com.kw.{packageName}.common",
            "javaName": tableName,
            "tableName": table,
            "lowerTableName": Initial_Letter_Lower_Or_Upper(tableName),
            "date": datetime.datetime.now(),
            "tableCommont": logInfo[0]['TABLE_COMMENT']
        }
        result = readTableField(tableName=table)
        result = changeFiledType(result)
        translateStr = ""
        create("entity.jinja2", f"{path}entity", f"\\{tableName}.java", result, configuration)
        create("mapper.jinja2", f"{path}mapper", f"\\{tableName}Mapper.java", result, configuration)
        create("controller.jinja2", f"{path}controller", f"\\{tableName}Controller.java", result , configuration)
        create("service.jinja2", f"{path}service", f"\\I{tableName}Service.java", result, configuration)
        create("serviceImpl.jinja2", f"{path}service\\impl", f"\\{tableName}ServiceImpl.java", result , configuration)
        create("mapperXml.jinja2", mapperPath, f"\\{tableName}Mapper.xml", result, configuration)
        create("entitySaveOrUpdate.jinja2", commonPath + "dto\\" + Initial_Letter_Lower_Or_Upper(tableName),f"\\{tableName}SaveOrUpdateDTO.java", result, configuration)
        create("pageVo.jinja2", commonPath + "vo\\" + Initial_Letter_Lower_Or_Upper(tableName),f"\\{tableName}PageReqVO.java", result, configuration)
        create("pageDto.jinja2", commonPath + "dto\\" + Initial_Letter_Lower_Or_Upper(tableName),f"\\{tableName}PageReqDTO.java", result, configuration)
