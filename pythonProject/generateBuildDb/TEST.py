# encoding=utf-8

from jinja2 import Environment, BaseLoader
import pymysql
import datetime
import os
import re


def to_camelcase(text):
    arr = filter(None, text.lower().split('_'))
    res = ''
    j = 0
    for i in arr:
        if j == 0:
            res = i
        else:
            res = res + i[0].upper() + i[1:]
        j += 1
    return res


def readTableField(tableName):

    db = pymysql.connect(host='gz-cynosdbmysql-grp-019tp05t.sql.tencentcdb.com',
                         port=26409,
                         user='root',
                         password='///520xxx',
                         database='kw',
                         charset='utf8',
                         cursorclass=pymysql.cursors.DictCursor)
    try:
        cursor = db.cursor()
        sql = "SHOW FULL COLUMNS FROM {}".format(tableName)
        cursor.execute(sql)
        result = cursor.fetchall()
        return result
    except Exception:
        print("sql 错误")

def Initial_Letter_Lower_Or_Upper(my_str, is_upper=False):
    """
    用于将单词的首字母大写或小写
    :param my_str: 传入的字符串
    :param is_upper: 是否大写(默认为 False)
    :return: str
    """
    if is_upper is False:
        return my_str[:1].lower() + my_str[1:]
    elif is_upper is True:
        return my_str[:1].upper() + my_str[1:]

def changeFiledType(datas):
    for data in datas:
        type = data["Type"]
        if "int" in type:
            data["Type"] = "Integer"
        if "bigint" in type:
            data["Type"] = "Long"
        if "varchar" in type:
            data["Type"] = "String"
        if "char" in type:
            data["Type"] = "String"
        if "blob" in type:
            data["Type"] = "byte[]"
        if "text" in type:
            data["Type"] = "String"
        if "integer" in type:
            data["Type"] = "Integer"
        if "tinyint" in type:
            data["Type"] = "Integer"
        if "smallint" in type:
            data["Type"] = "Integer"
        if "mediumint" in type:
            data["Type"] = "Integer"
        if "bit" in type:
            data["Type"] = "Boolean"
        if "float" in type:
            data["Type"] = "Folat"
        if "double" in type:
            data["Type"] = "Double"
        if "decimal" in type:
            data["Type"] = "BigDecimal"
        if "boolean" in type:
            data["Type"] = "Boolean"
        if "date" in type:
            data["Type"] = "LocalDate"
        if "time" in type:
            data["Type"] = "LocalDateTime"
        if "datetime" in type:
            data["Type"] = "LocalDateTime"

        data["FieldLower"] = data["Field"]
        data["Field"] = to_camelcase(data["Field"])
        data["FieldUper"] = data["Field"][:1].upper() + data["Field"][1:]

    return datas

def readTableInfo(tableName):
    db = pymysql.connect(host='gz-cynosdbmysql-grp-019tp05t.sql.tencentcdb.com',
                         port=26409,
                         user='root',
                         password='///520xxx',
                         database='information_schema',
                         charset='utf8',
                         cursorclass=pymysql.cursors.DictCursor)
    try:
        cursor = db.cursor()
        sql = f"SELECT * FROM information_schema.TABLES WHERE TABLE_NAME = '{tableName}' and table_schema = 'kw'"
        cursor.execute(sql)
        result = cursor.fetchall()
        print(result)
        return result
    except Exception:
        print("sql 错误")

if __name__ == "__main__":
    result = readTableField(tableName="sys_log")
    print(result)
    logInfo = readTableInfo(tableName="sys_log")
    print(logInfo[0]['TABLE_COMMENT'])
