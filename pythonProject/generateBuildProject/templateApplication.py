# encoding=utf-8

from jinja2 import Environment, BaseLoader
import pymysql
import datetime
import os
import re

projectPrefix = "kw-framework"
projectName = "generateT"
basicPath = "D:\\doc\\createProjectTest\\"

#将jinja2模板写到磁盘中
def jinja2OpenWrite(fileName,configuration,targetPath):
    with open(fileName, mode="r", encoding="utf-8") as r:
        template_str = r.read()
        tpl = Environment(loader=BaseLoader).from_string(template_str)
        translateStr = tpl.render(configuration=configuration)
        with open(targetPath, mode="w", encoding="utf-8") as w:
            w.write(translateStr)

#将pom文件写入磁盘
def writePom(path,jinjia2Name):
    global projectName
    configuration = {
        "projectName": projectName
    }
    jinja2OpenWrite(f"{jinjia2Name}.jinja2", configuration, f"{path}\\pom.xml")

#将ignore文件写入磁盘
def writeIgnore(path):
    with open("ignore.txt", mode="r", encoding="utf-8") as r:
        ignoreTxt = r.read()
        with open(path + "\\.gitignore", mode="w", encoding="utf-8") as w:
            w.write(ignoreTxt)

#写文件封装
def doCreate(path,childPath,jinja2):
    os.makedirs(childPath, exist_ok=True)
    writeIgnore(path)
    writePom(path, jinja2)

#创建parent项目
def createParent(path):
    doCreate(path,path,"parentPom")

#创建api项目
def createApi(path):
    doCreate(path,f"{path}\\src\\main\\java\\com\\kw\\generateT\\api","apiPom")

#创建common项目
def createCommon(path):
    doCreate(path,f"{path}\\src\\main\\java\\com\\kw\\generateT\\common","commonPom")

#创建service项目
def createService(path):
    doCreate(path,f"{path}\\src\\main\\java\\com\\kw\\generateT\\service","servicePom")
    os.makedirs(f"{path}\\src\\main\\resources\\mapper", exist_ok=True)
    name = projectName[:1].upper() + projectName[1:]
    configuration = {
        "name": name,
        "projectName": projectName,
        "nacosUrl": "106.52.216.141:8848",
        "nacosNameSpace": "0a96e28f-434a-445e-b38a-c39cbc5590cd",
        "port": "8888"
    }
    jinja2OpenWrite("bootstrap.jinja2",configuration,f"{path}\\src\\main\\resources\\bootstrap.yml")
    jinja2OpenWrite("application.jinja2", configuration, f"{path}\\src\\main\\java\\com\\kw\\generateT\\service\\{name}Application.java")


if __name__ == "__main__":
    workSpaceName = f"{projectPrefix}-{projectName}"
    parentPath = f"{basicPath}{projectPrefix}-{projectName}"

    createParent(parentPath)
    createApi(f"{parentPath}\\{workSpaceName}-api")
    createCommon(f"{parentPath}\\{workSpaceName}-common")
    createService(f"{parentPath}\\{workSpaceName}-service")
