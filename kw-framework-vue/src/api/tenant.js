import axios from '~/axios'

export function list(pageNo,pageSize,query = {}){
    let q = []
    for (const key in query) {
        if(query[key]){
            q.push(`${key}=${encodeURIComponent(query[key])}`)
        }
    }
    let r = q.join("&")
    r = r ? ("&"+r) : ""
    return axios.get(`/admin/sysTenant/?pageNo=${pageNo}&pageSize=${pageSize}${r}`)
}


export function createTenant(data){
    return axios.post("/admin/sysTenant/",data)
}

export function updateTenant(data){
    return axios.put("/admin/sysTenant/",data)
}


export function deleteTenant(id){
    return axios.delete(`/admin/sysTenant/${id}`)
}

export function updateTenantStatus(id,status){
    return axios.put(`/admin/sysTenant/`,{
        id,
        status
    })
}
