import axios from '~/axios'

export function login(username,password){
    // 发送 POST 请求  vue默认所有post请求都是以body形式传递，需要进行转化
    let param = new URLSearchParams()
    param.append('client_id', 'user')
    param.append('grant_type', 'password')
    param.append('client_secret', '123456')
    param.append('username', username)
    param.append('password', password)
    return axios.post(
        '/sso/oauth/token',
        param
    );
}


export function getUserList(pageNo,pageSize,query = {}){
  let q = []
  for (const key in query) {
      if(query[key]){
          q.push(`${key}=${encodeURIComponent(query[key])}`)
      }
  }
  let r = q.join("&")
  r = r ? ("&"+r) : ""
  return axios.get(`/admin/sysUser/page?pageNo=${pageNo}&pageSize=${pageSize}${r}`)
}

export function updateUser(query={}){
  return axios.post('/admin/sysUser/update',JSON.stringify(query))
}

export function getinfo(){
  return axios.get('/admin/sysUser/userInfoByToken');
}


export function getMenus(){
  return axios.get('/admin/sysUser/getUserMenu');
}

export function logout(){
    return axios.post("/admin/logout")
}

export function updatepassword(data){
    return axios.post("/admin/updatepassword",data)
}

export function getManagerList(page,query = {}){
    let q = []
    for (const key in query) {
        if(query[key]){
            q.push(`${key}=${encodeURIComponent(query[key])}`)
        }
    }
    let r = q.join("&")
    r = r ? ("?"+r) : ""

    return axios.get(`/admin/manager/${page}${r}`)
}


export function updateManagerStatus(id,status){
    return axios.post(`/admin/sysUser/update`,{
        id,
        status
    })
}


export function createManager(data){
    return axios.post(`/admin/sysUser/save`,data)
}

export function updateManager(id,data){
    return axios.post(`/admin/sysUser/update`,data)
}

export function deleteManager(id){
    return axios.delete(`/admin/sysUser/deleteById/${id}`)
}