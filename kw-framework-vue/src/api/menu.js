import axios from '~/axios'

export function getMenuList(roleId,tenantId=1){
    return axios.post(`/admin/sys-menu/getMenuTree`,{tenantId,roleId})
}

export function getMenuPage(pageNo,pageSize,tenantId){
    return axios.post(`/admin/sys-menu/getMenuTree`,tenantId)
}




export function createMenu(data){
    return axios.post("/admin/sys-menu/save",data)
}

export function updateMenu(id,data){
    return axios.put("/admin/sys-menu/update",data)
}

export function updateMenuStatus(id,status){
    return axios.put(`/admin/sys-menu/update`,{
        id,
        status
    })
}

export function deleteMenu(id){
    return axios.delete(`/admin/sys-menu/deleteById/${id}`)
}