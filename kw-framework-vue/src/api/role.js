import axios from '~/axios'

export function getRoleList(pageNo,pageSize,query = {}){
    return axios.post(`/admin/sysRole/page`,{
        pageNo,
        pageSize
    })
}

export function createRole(data){
    return axios.post("/admin/sysRole/save",data)
}

export function updateRole(id,data){
    return axios.put("/admin/sysRole/update",data)
}

export function deleteRole(id){
    return axios.delete(`/admin/sysRole/deleteById/${id}`)
}

export function updateRoleStatus(id,status){
    return axios.put(`/admin/sysRole/update`,{
        id,
        status
    })
}

export function setRoleRules(roleId,menuIds){
    return axios.post(`/admin/sysRole/rolePermission`,{
        roleId,menuIds
    })
}