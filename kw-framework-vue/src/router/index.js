import {
    createRouter,
    createWebHashHistory
} from 'vue-router'

import Admin from "~/layouts/admin.vue";
import Index from '~/pages/index.vue'
import Login from '~/pages/login.vue'
import NotFound from '~/pages/404.vue'
import UserList from '~/pages/user/index.vue'
import RoleList from '~/pages/role/list.vue'
import AccessList from '~/pages/access/list.vue'
import Tenant from '~/pages/tenant/list.vue'

// 默认路由，所有用户共享
const routes = [
    {
        path: "/",
        name:"admin",
        component: Admin,
    },
    {
        path: "/login",
        component: Login,
        meta: {
            title: "登录页"
        }
    }, {
        path: '/:pathMatch(.*)*',
        name: 'NotFound',
        component: NotFound
    }]


// 动态路由，用于匹配菜单动态添加路由
const asyncRoutes = [{
    path:"/",
    name:"/",
    component:Index,
    meta:{
        title:"后台首页"
    }
},{
    path:"/user/index",
    name:"/user/index",
    component:UserList,
    meta:{
        title:"用户管理"
    }
},{
    path:"/role/list",
    name:"/role/list",
    component:RoleList,
    meta:{
        title:"角色列表"
    }
}, {
    path: '/menu/index',
    name: '/menu/index',
    component: AccessList,
    meta:{
        title:"菜单列表"
    }
}, {
    path: '/tenant/list',
    name: '/tenant/list',
    component: Tenant,
    meta:{
        title:"商户列表"
    }
}]

export const router = createRouter({
    history: createWebHashHistory(),
    routes
})

// 动态添加路由的方法
export function addRoutes(menus){
    // 是否有新的路由
    let hasNewRoutes = false
    const findAndAddRoutesByMenus = (arr) =>{
        arr.forEach(e=>{
            let item = asyncRoutes.find(o=>o.path == e.path)
            if(item && !router.hasRoute(item.path)){
                router.addRoute("admin",item)
                hasNewRoutes = true
            }
            if(e.children && e.children.length > 0){
                findAndAddRoutesByMenus(e.children)
            }
        })
    }

    findAndAddRoutesByMenus(menus)

    return hasNewRoutes
}