package com.kw.sso.translator;

import com.kw.framework.common.croe.vo.Result;
import com.kw.framework.common.croe.enums.ResultEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.exceptions.UnsupportedGrantTypeException;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;

@Slf4j
public class CustomWebResponseExceptionTranslator implements WebResponseExceptionTranslator {

    @Override
    public ResponseEntity<Result<String>> translate(Exception e) throws Exception {
        log.error("认证服务器异常",e);

        //Result<String> response = resolveException(e);
        if (e instanceof UnsupportedGrantTypeException) {
            //不支持的认证方式
            return new ResponseEntity<Result<String>>(Result.error(ResultEnum.UNSUPPORTED_GRANT_TYPE,ResultEnum.UNSUPPORTED_GRANT_TYPE.getDesc()),HttpStatus.UNAUTHORIZED);
        } else if (e instanceof InvalidGrantException) {
            //用户名或密码异常
            return new ResponseEntity<Result<String>>(Result.error(ResultEnum.USERNAME_OR_PASSWORD_ERROR,ResultEnum.USERNAME_OR_PASSWORD_ERROR.getDesc()),HttpStatus.UNAUTHORIZED);
        }else if(e instanceof InternalAuthenticationServiceException){
            return new ResponseEntity<Result<String>>(Result.error(ResultEnum.USERNAME_OR_PASSWORD_ERROR,ResultEnum.USERNAME_OR_PASSWORD_ERROR.getDesc()),HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<Result<String>>(Result.error("未知错误"),HttpStatus.UNAUTHORIZED);
    }

    /**
     * 构建返回异常
     * @param e exception
     * @return
     */
    private Result<String> resolveException(Exception e) {
        // 初始值 500
        int httpStatus = HttpStatus.UNAUTHORIZED.value();
        //不支持的认证方式
        if (e instanceof UnsupportedGrantTypeException) {
            return Result.error(ResultEnum.UNSUPPORTED_GRANT_TYPE,ResultEnum.UNSUPPORTED_GRANT_TYPE.getDesc());
            //用户名或密码异常
        } else if (e instanceof InvalidGrantException) {
            return Result.error(ResultEnum.USERNAME_OR_PASSWORD_ERROR,ResultEnum.USERNAME_OR_PASSWORD_ERROR.getDesc());
        }
        return Result.error("未知错误");
    }
}