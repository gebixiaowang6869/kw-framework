package com.kw.sso.controller;

import cn.hutool.json.JSONUtil;
import com.kw.admin.api.feign.RemoteSysUserService;
import com.kw.admin.common.dto.user.UserNameTokenDTO;
import com.kw.framework.common.croe.vo.Result;
import com.kw.framework.common.croe.entity.GlobalAuthEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.CheckTokenEndpoint;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping("/oauth")
@Slf4j
public class AuthController{

    //令牌请求的端点
    @Autowired
    private TokenEndpoint tokenEndpoint;
    @Autowired
    private CheckTokenEndpoint checkTokenEndpoint;

    @Autowired
    private RemoteSysUserService remoteSysUserService;


    /**
     * 重写/oauth/token这个默认接口，返回的数据格式统一
     */
    @PostMapping(value = "/token")
    public Result<OAuth2AccessToken> postAccessToken(Principal principal, @RequestParam
            Map<String, String> parameters) throws HttpRequestMethodNotSupportedException {
        OAuth2AccessToken accessToken = tokenEndpoint.postAccessToken(principal, parameters).getBody();
        String token = accessToken.getValue();
        GlobalAuthEntity entity = JSONUtil.toBean((String)accessToken.getAdditionalInformation().get("username"), GlobalAuthEntity.class);
        //记录用户最后登录tokenkai
        remoteSysUserService.updateTokenById(UserNameTokenDTO.builder().id(entity.getId()).token(token).build());
        return Result.success(accessToken);
    }

    /**
     * 重写/oauth/check_token这个默认接口，用于校验令牌，返回的数据格式统一
     */
    @PostMapping(value = "/check_token")
    public Result<Map<String,?>> checkToken(@RequestParam("token") String value)  {
        Map<String, ?> map = checkTokenEndpoint.checkToken(value);
        return Result.success(map);
    }
}