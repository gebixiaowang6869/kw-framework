package com.kw.sso.service;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.json.JSONUtil;
import com.kw.admin.api.feign.RemoteSysUserService;
import com.kw.admin.common.vo.UserInfoVO;
import com.kw.common.security.entity.KwSecurityUser;
import com.kw.framework.common.croe.vo.Result;
import com.kw.framework.common.croe.constant.SecurityConstants;
import com.kw.framework.common.croe.entity.GlobalAuthEntity;
import com.kw.framework.common.croe.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


@Slf4j
@Service
public class  KwUserDetailsService implements UserDetailsService {

	@Autowired
	RemoteSysUserService remoteSysUserService ;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		String name = username.substring(0,username.indexOf("_"));
		String tenantId = username.substring(username.indexOf("_"));
		Result<UserInfoVO> r = remoteSysUserService.userInfo(name,tenantId);
		if(r==null || !r.getStatus().equals(200)|| r.getData() == null){
			throw new BusinessException("获取用户信息失败");
		}
		UserInfoVO userInfoVO = r.getData();
		GlobalAuthEntity user = new GlobalAuthEntity();
		user.setId(userInfoVO.getSysUserVO().getId());
		user.setName(name);
		user.setMiniOpenId(userInfoVO.getSysUserVO().getMiniOpenid());
		user.setTenantId(userInfoVO.getSysUserVO().getTenantId());
		user.setOpenId(userInfoVO.getSysUserVO().getWxOpenid());
		user.setPhone(userInfoVO.getSysUserVO().getPhone());

		Set<String> dbAuthsSet = new HashSet<>();
		if (ArrayUtil.isNotEmpty(userInfoVO.getRoles())) {
			// 获取角色
			Arrays.stream(userInfoVO.getRoles()).forEach(roleId -> dbAuthsSet.add(SecurityConstants.ROLE + roleId));
			// 获取资源
			dbAuthsSet.addAll(Arrays.asList(userInfoVO.getPermissions()));

		}
		Collection<? extends GrantedAuthority> authorities = AuthorityUtils
				.createAuthorityList(dbAuthsSet.toArray(new String[0]));

		return new KwSecurityUser(JSONUtil.toJsonStr(user),userInfoVO.getSysUserVO().getPassword(),authorities);

	}

	public static void main(String[] args) {
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		System.out.println(passwordEncoder.encode("123456"));
	}


}
