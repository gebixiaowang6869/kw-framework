package com.kw.sso.service;

import cn.hutool.json.JSONUtil;
import com.kw.common.security.entity.KwSecurityUser;
import com.kw.framework.common.croe.entity.GlobalAuthEntity;
import com.kw.framework.common.croe.enums.ResultEnum;
import com.kw.framework.common.croe.exception.BusinessException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import lombok.extern.slf4j.Slf4j;

/**
 *
 *  说明：自定义的获取用户信息
 */
@Service
@Slf4j
public class PhoneSmsUserDetailService {


    public UserDetails loadByPhone(String phone, String code) {
        if(phone.equals("18826411788")){
            GlobalAuthEntity user = new GlobalAuthEntity();
            user.setId(-1L);
            user.setName("超级管理员");
            //user.setTenentId(tenentId);
            user.setOpenId("2222");
            user.setPhone(phone);
            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            return new KwSecurityUser(JSONUtil.toJsonStr(user),passwordEncoder.encode("kwdl88"), AuthorityUtils.commaSeparatedStringToAuthorityList("admin"));
        }
        throw new BusinessException(ResultEnum.PHONE_CODE_ERROR);
    }
    //自定义其他登录等




}

