
package com.kw.sso.config;

import com.kw.common.security.entity.KwSecurityUser;
import com.kw.framework.common.croe.constant.SecurityConstants;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * token增强，客户端模式不增强。*/


@Service
public class KwTokenEnhancer implements TokenEnhancer {

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

		final Map<String, Object> additionalInfo = new HashMap<>(8);
		String clientId = authentication.getOAuth2Request().getClientId();
		additionalInfo.put("clientId", clientId);

		// 客户端模式不返回具体用户信息
		if (SecurityConstants.CLIENT_CREDENTIALS.equals(authentication.getOAuth2Request().getGrantType())) {
			((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
			return accessToken;
		}

		KwSecurityUser user = (KwSecurityUser) authentication.getUserAuthentication().getPrincipal();
		//additionalInfo.put(SecurityConstants.DETAILS_USER_ID, user.getUserId());
		additionalInfo.put(SecurityConstants.DETAILS_USER, user.getUsername());
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
		return accessToken;
	}

}
