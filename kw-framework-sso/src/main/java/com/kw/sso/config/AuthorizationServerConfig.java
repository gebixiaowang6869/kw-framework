package com.kw.sso.config;

import cn.hutool.json.JSONUtil;
import com.kw.framework.common.croe.vo.Result;
import com.kw.framework.common.croe.enums.ResultEnum;
import com.kw.sso.filter.CustomClientCredentialsTokenEndpointFilter;
import com.kw.sso.granter.PhoneSmsTokenGranter;
import com.kw.sso.service.PhoneSmsUserDetailService;
import com.kw.sso.service.KwJdbcClientDetailsService;
import com.kw.sso.service.KwCustomTokenServices;
import com.kw.sso.translator.CustomWebResponseExceptionTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.web.client.RestTemplate;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 授权服务器
 * @author kw
 *
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	/**
	 * 自带的东东
	 */
	@Autowired
	private KwJdbcClientDetailsService jdbcClientDetailsService;

	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private PhoneSmsUserDetailService customUserDetailService;
	@Autowired
	private  TokenEnhancer tokenEnhancer;

	/**
	 * end 自带的东东
	 */
	@Autowired
	@Qualifier("kwUserDetailsService")
	private UserDetailsService userDetailsService;
	@Autowired
	private RedisConnectionFactory redisConnectionFactory;


	@LoadBalanced
	@Bean
	public RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate;
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) {

		CustomClientCredentialsTokenEndpointFilter endpointFilter = new CustomClientCredentialsTokenEndpointFilter(security);
		endpointFilter.afterPropertiesSet();
		endpointFilter.setAuthenticationEntryPoint(authenticationEntryPoint());
		security.addTokenEndpointAuthenticationFilter(endpointFilter);

		security.authenticationEntryPoint(authenticationEntryPoint())
				// 开启/oauth/token_key验证端口无权限访问
				.tokenKeyAccess("permitAll()")
				// 开启/oauth/check_token验证端口认证权限访问
				.checkTokenAccess("permitAll()")
		;
	}



	@Bean
	public TokenStore tokenStore(){
		return new RedisTokenStore(redisConnectionFactory);
	}


	@Bean
	public KwCustomTokenServices tokenServices() {
		KwCustomTokenServices tokenServices = new KwCustomTokenServices();
		tokenServices.setTokenStore(tokenStore());
		tokenServices.setSupportRefreshToken(true);
		tokenServices.setReuseRefreshToken(false);
		tokenServices.setClientDetailsService(jdbcClientDetailsService);
		tokenServices.setTokenEnhancer(tokenEnhancer);
		//addUserDetailsService(tokenServices, pigxUserDetailsService);
		return tokenServices;
	}


	/**
	 * 读取数据库中所有的client
	 * @param clients
	 * @throws Exception
	 */
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		// 配置客户端
		clients.withClientDetails(jdbcClientDetailsService);
	}
	@Override
	public void configure(final AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.tokenStore(tokenStore())
				.tokenServices(tokenServices());
		endpoints.authenticationManager(authenticationManager);
		//自定义登录异常
		endpoints.exceptionTranslator(new CustomWebResponseExceptionTranslator());

		//在原有授权的基础上增加自定义手机号短信登录
		List<TokenGranter> tokenGranters = getTokenGranters(endpoints.getTokenServices(), endpoints.getClientDetailsService(), endpoints.getOAuth2RequestFactory());
		//原有的授权endpoints.getTokenGranter()
		tokenGranters.add(endpoints.getTokenGranter());
		endpoints.tokenGranter(new CompositeTokenGranter(tokenGranters));
	}



	/**
	 * 自定义TokenGranter集合
	 */
	private List<TokenGranter> getTokenGranters(AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService, OAuth2RequestFactory requestFactory) {
		return new ArrayList<>(Collections.singletonList(
				new PhoneSmsTokenGranter(tokenServices, clientDetailsService, requestFactory, customUserDetailService)
		));
	}


	@Bean
	public AuthenticationEntryPoint authenticationEntryPoint() {
		return (request, response, e) -> {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			response.setContentType("application/json;charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.write(JSONUtil.toJsonStr(Result.error(ResultEnum.CLIENT_AUTHENTICATION_FAILED,ResultEnum.CLIENT_AUTHENTICATION_FAILED.getDesc())));
			out.flush();
			out.close();
		};
	}


}
