package com.kw.sso.granter;

import java.util.Map;

import com.kw.common.security.entity.KwSecurityUser;
import com.kw.framework.common.croe.enums.ResultEnum;
import com.kw.framework.common.croe.exception.BusinessException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;


/**
 *
 *  说明：
 *     自定义token授予抽象实现
 *     继承抽象类AbstractTokenGranter，我们也是抽象的，后面好扩展
 */
public abstract class CustomAbstractTokenGranter extends AbstractTokenGranter {

    public CustomAbstractTokenGranter(AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService, OAuth2RequestFactory requestFactory, String grantType) {
        super(tokenServices, clientDetailsService, requestFactory, grantType);
    }

    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
        Map<String, String> parameters = tokenRequest.getRequestParameters();
        KwSecurityUser loginUser = (KwSecurityUser)getUserDetails(parameters);
        if (null == loginUser) {
            throw new BusinessException(ResultEnum.CLIENT_AUTHENTICATION_FAILED);
        }
        //Object principal, Object credentials,Collection<? extends GrantedAuthority> authorities
        Authentication userAuth = new UsernamePasswordAuthenticationToken(loginUser,
                loginUser.getPassword(), loginUser.getAuthorities());

        OAuth2Request storedOAuth2Request = getRequestFactory().createOAuth2Request(client, tokenRequest);
        return new OAuth2Authentication(storedOAuth2Request, userAuth);

    }

    /**
     * 自定义获取用户信息
     */
    protected abstract UserDetails getUserDetails(Map<String, String> parameters);
}
