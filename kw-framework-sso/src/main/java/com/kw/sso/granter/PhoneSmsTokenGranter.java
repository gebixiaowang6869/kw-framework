package com.kw.sso.granter;

import java.util.Map;

import com.kw.sso.service.PhoneSmsUserDetailService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

/**
 *
 *  说明：
 *    手机号-短信验证码
 *    定义短信验证码具体逻辑
 */
public class PhoneSmsTokenGranter extends CustomAbstractTokenGranter {

    private static final String PHONE_SMS = "phone_sms";

    private PhoneSmsUserDetailService phoneSmsUserDetailService;

    public PhoneSmsTokenGranter(AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService, OAuth2RequestFactory requestFactory, PhoneSmsUserDetailService customUserDetailService) {
        super(tokenServices, clientDetailsService, requestFactory, PHONE_SMS);
        this.phoneSmsUserDetailService = customUserDetailService;
    }

    @Override
    protected UserDetails getUserDetails(Map<String, String> parameters) {
        String phone = parameters.get("phone");
        String smsCode = parameters.get("sms_code");
        return phoneSmsUserDetailService.loadByPhone(phone,smsCode);
    }
}