package com.kw.admin.api.feign;

import com.kw.admin.common.dto.user.UserNameTokenDTO;
import com.kw.admin.common.vo.UserInfoVO;
import com.kw.framework.common.croe.vo.Result;
import feign.Headers;
import feign.Param;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


@FeignClient(contextId = "remoteSysUserService", value = "admin")
public interface RemoteSysUserService {
    @PostMapping("/admin/sysUser/userInfo/{name}")
    @Headers({"tenantId:{tenantId}"})
    public Result<UserInfoVO> userInfo(@PathVariable(value = "name") String name,@Param("tenantId")String tenantId);


    @PostMapping("/admin/sysUser/updateTokenById")
    public Result updateTokenById(UserNameTokenDTO dto);
}
