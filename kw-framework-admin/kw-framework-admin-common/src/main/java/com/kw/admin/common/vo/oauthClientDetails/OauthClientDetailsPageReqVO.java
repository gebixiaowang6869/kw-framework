package com.kw.admin.common.vo.oauthClientDetails;


import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;


/**
 * <p>
 * OauthClientDetails
 * </p>
 *
 * @author kw
 * @since 2022-10-25 17:09:49.427296
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="oauth_client_details")
public class OauthClientDetailsPageReqVO implements Serializable {

    private static final long serialVersionUID = 1L;


    
        private String clientId;
    
        private String resourceIds;
    
        private String clientSecret;
    
        private String scope;
    
        private String authorizedGrantTypes;
    
        private String webServerRedirectUri;
    
        private String authorities;
    
        private Integer accessTokenValidity;
    
        private Integer refreshTokenValidity;
    
        private String additionalInformation;
    
        private String autoapprove;
    


}