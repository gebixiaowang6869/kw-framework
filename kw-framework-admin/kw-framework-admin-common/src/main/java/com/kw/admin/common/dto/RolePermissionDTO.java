package com.kw.admin.common.dto;

import lombok.Data;

import java.util.List;

@Data
public class RolePermissionDTO {

    private Long roleId ;

    private List<Long> menuIds ;
}
