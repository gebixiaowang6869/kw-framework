package com.kw.admin.common.dto.log;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 日志表
 * </p>
 *
 * @author kw
 * @since 2022-10-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="sys_log")
public class SysLogSaveOrUpdateDTO {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号")
    private Long id;
    private String type;
    private String title;
    private String serviceId;
    private Long createBy;
    private Long updateBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    private String remoteAddr;
    private String userAgent;
    private String requestUri;
    private String method;
    private String params;
    /**
     * 执行时间
     */
    @ApiModelProperty(value = "执行时间")
    private String time;
    private Boolean delFlag;
    private String exception;
    /**
     * 所属租户
     */
    @ApiModelProperty(value = "所属租户")
    private Integer tenantId;


}
