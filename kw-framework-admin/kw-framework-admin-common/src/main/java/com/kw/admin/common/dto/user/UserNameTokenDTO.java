package com.kw.admin.common.dto.user;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserNameTokenDTO {

    private Long id ;

    private String token ;
}
