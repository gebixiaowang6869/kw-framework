package com.kw.admin.common.dto.tenant;

import com.kw.framework.common.croe.param.PageParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class SysTenantPageReqDTO extends PageParam {

    private List<String> startTimeList; // 开始时间起止


    private List<String> endTimeList; // 结束时间起止


    private List<String> createTimeList; // 创建起止


    private List<String> updateTimeList; // 更新时间起止


    @ApiModelProperty(value = "页码")
    private Integer pageNo ;
    @ApiModelProperty(value = "每页条目")
    private Integer pageSize ;

    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer id;
    private String name;
    private String code;
    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startTime;
    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endTime;
    private Integer status;
    /**
     * 创建
     */
    @ApiModelProperty(value = "创建")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    private Long createBy;
    private Long updateBy;
}
