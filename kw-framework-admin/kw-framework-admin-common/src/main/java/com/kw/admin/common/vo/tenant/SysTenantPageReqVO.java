package com.kw.admin.common.vo.tenant;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SysTenantPageReqVO {


    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer id;
    private String name;
    private String code;
    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startTime;
    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endTime;
    private Integer status;
    /**
     * 创建
     */
    @ApiModelProperty(value = "创建")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    private Long createBy;
    private Long updateBy;
}
