package com.kw.admin.common.dto;

import lombok.Data;

@Data
public class MenuTreeDTO {
    private Integer tenantId;
    private Long roleId ;
}
