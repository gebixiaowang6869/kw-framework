package com.kw.admin.common.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


@Data
public class SysUserAddDTO {
    private Long id;
    private String username;
    private String password;
    private String salt;
    private String phone;
    private String avatar;
    /**
     * 部门ID
     */
    @ApiModelProperty(value = "部门ID")
    private Integer deptId;

    private Integer status;
    /**
     * 微信登录openId
     */
    @ApiModelProperty(value = "微信登录openId")
    private String wxOpenid;
    /**
     * 小程序openId
     */
    @ApiModelProperty(value = "小程序openId")
    private String miniOpenid;
    /**
     * QQ openId
     */
    @ApiModelProperty(value = "QQ openId")
    private String qqOpenid;
    /**
     * 码云 标识
     */
    @ApiModelProperty(value = "码云 标识")
    private String giteeLogin;
    /**
     * 开源中国 标识
     */
    @ApiModelProperty(value = "开源中国 标识")
    private String oscId;
    /**
     * 所属租户
     */
    @ApiModelProperty(value = "所属租户")
    private Integer tenantId;
    /**
     * 用户角色
     */
    @ApiModelProperty(value = "用户角色")
    private List<Long> roleIds ;

    private String token ;
}
