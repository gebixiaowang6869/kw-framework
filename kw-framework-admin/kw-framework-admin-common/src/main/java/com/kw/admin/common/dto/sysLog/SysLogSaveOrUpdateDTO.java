package com.kw.admin.common.dto.sysLog;


import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;


/**
 * <p>
 * SysLog
 * </p>
 *
 * @author kw
 * @since 2022-10-25 17:19:31.887031
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="sys_log")
public class SysLogSaveOrUpdateDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    
            @ApiModelProperty(value = "编号")
            private Integer id;
    
            @ApiModelProperty(value = "")
            private String type;
    
            @ApiModelProperty(value = "")
            private String title;
    
            @ApiModelProperty(value = "")
            private String serviceId;
    
            @ApiModelProperty(value = "")
            private Integer createBy;
    
            @ApiModelProperty(value = "")
            private Integer updateBy;
    
            @ApiModelProperty(value = "创建时间")
            private LocalDateTime createTime;
    
            @ApiModelProperty(value = "更新时间")
            private LocalDateTime updateTime;
    
            @ApiModelProperty(value = "")
            private String remoteAddr;
    
            @ApiModelProperty(value = "")
            private String userAgent;
    
            @ApiModelProperty(value = "")
            private String requestUri;
    
            @ApiModelProperty(value = "")
            private String method;
    
            @ApiModelProperty(value = "")
            private String params;
    
            @ApiModelProperty(value = "执行时间")
            private String time;
    
            @ApiModelProperty(value = "")
            private Integer delFlag;
    
            @ApiModelProperty(value = "")
            private String exception;
    
            @ApiModelProperty(value = "所属租户")
            private Integer tenantId;
    



}