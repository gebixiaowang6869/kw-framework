package com.kw.admin.common.vo;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class SysUserPageVO {
    /**
     * 主键ID
     */
    private Long id;
    private String username;
    private String password;
    private String salt;
    private String phone;
    private String avatar;
    private String token ;
    /**
     * 部门ID
     */
    private Integer deptId;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    private LocalDateTime updateTime;
    private Integer status;
    /**
     * 微信登录openId
     */
    private String wxOpenid;
    /**
     * 小程序openId
     */
    private String miniOpenid;
    /**
     * QQ openId
     */
    private String qqOpenid;
    /**
     * 码云 标识
     */
    private String giteeLogin;
    /**
     * 开源中国 标识
     */
    private String oscId;
    /**
     * 所属租户
     */
    private Integer tenantId;
    private Long updateBy;
    private Long createBy;

    private List<Long> roleIds ;
}
