package com.kw.admin.common.vo;

import lombok.Data;


@Data
public class SysUserVO {

    private Long id;
    private String username;
    private String password;
    private String salt;
    private String phone;
    private String avatar;
    private String  token ;
    /**
     * 微信登录openId
     */
    private String wxOpenid;
    /**
     * 小程序openId
     */
    private String miniOpenid;
    /**
     * QQ openId
     */
    private String qqOpenid;
    /**
     * 码云 标识
     */
    private String giteeLogin;
    /**
     * 开源中国 标识
     */
    private String oscId;
    /**
     * 所属租户
     */
    private Integer tenantId;
}
