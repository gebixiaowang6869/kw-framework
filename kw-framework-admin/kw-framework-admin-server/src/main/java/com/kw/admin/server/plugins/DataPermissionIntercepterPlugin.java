package com.kw.admin.server.plugins;

import com.baomidou.mybatisplus.extension.plugins.inner.InnerInterceptor;
import com.google.common.collect.Lists;
import com.kw.framework.mybatis.handler.MyDataPermissionHandler;
import com.kw.framework.mybatis.interfaces.MyDataPermissionInterceptor;
import com.kw.framework.mybatis.plugins.InnerInterceptorPlugin;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DataPermissionIntercepterPlugin implements InnerInterceptorPlugin {
    @Override
    public List<InnerInterceptor> getInnerInterceptors() {
        MyDataPermissionHandler handler = new MyDataPermissionHandler();
        MyDataPermissionInterceptor interceptor = new MyDataPermissionInterceptor(handler);
        return Lists.newArrayList(interceptor);
    }
}
