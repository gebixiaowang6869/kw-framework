package com.kw.admin.server.service;

import com.kw.admin.server.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */

public interface ISysUserRoleService extends IService<SysUserRole> {


        Boolean addSysUserRole(SysUserRole sysUserRole);

        Boolean deleteByIds(String ids);

        Boolean updateSysUserRoleById(SysUserRole sysUserRole);

        List<SysUserRole> getList(LambdaQueryWrapper<SysUserRole> lambda);

        IPage<SysUserRole> getPage(LambdaQueryWrapper<SysUserRole> lambda, Page page);


}
