package com.kw.admin.server.service.impl;

import com.kw.admin.common.dto.RolePermissionDTO;
import com.kw.admin.server.entity.SysRole;
import com.kw.admin.server.entity.SysRoleMenu;
import com.kw.admin.server.mapper.SysRoleMapper;
import com.kw.admin.server.mapper.SysRoleMenuMapper;
import com.kw.admin.server.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;
import java.time.LocalDateTime;

import com.kw.common.security.utils.SecurityUtil;
import com.kw.framework.common.croe.constant.CommonConstant;

/**
 * <p>
 * 系统角色表 服务实现类
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {


    @Autowired
    SysRoleMenuMapper sysRoleMenuMapper;
    @Autowired
    private TokenStore tokenStore ;
    @Override
    public Boolean addSysRole(SysRole sysRole) {
        Long userId = SecurityUtil.getUserId();
        if (userId != null) {
            sysRole.setCreateBy(userId);
        }
        // entity.setId(IdUtils.getNextId());
        LocalDateTime now = LocalDateTime.now();
        sysRole.setCreateTime(now);
        sysRole.setUpdateTime(now);
        sysRole.setDelFlag(CommonConstant.UN_DELETE_FLAG);
        return this.save(sysRole);
    }

    @Override
    public Boolean deleteByIds(String ids) {
        String[] idsStrs = ids.split(",");
        for (String id : idsStrs) {
            this.removeById(Integer.parseInt(id));
        }
        return true;
    }

    @Override
    public Boolean updateSysRoleById(SysRole sysRole) {
        sysRole.setUpdateTime(LocalDateTime.now());
        return this.updateById(sysRole);
    }

    @Override
    public List<SysRole> getList(LambdaQueryWrapper<SysRole> lambda) {
        return this.list(lambda);
    }

    @Override
    public IPage<SysRole> getPage(LambdaQueryWrapper<SysRole> lambda, Page pageParam) {

        IPage<SysRole> page = this.page(pageParam, lambda);
        return page;
    }

    @Override
    public List<SysRole> getRolesByUserId(Long userId,Integer tenantId) {
        return baseMapper.listRolesByUserId(userId,tenantId);
    }




    @Override
    public void updateRolePermission(RolePermissionDTO rolePermissionDTO) {

        LambdaQueryWrapper<SysRoleMenu> sysRoleMenuLambdaQueryWrapper = new LambdaQueryWrapper<>();
        sysRoleMenuLambdaQueryWrapper.eq(SysRoleMenu::getRoleId, rolePermissionDTO.getRoleId());
        sysRoleMenuMapper.delete(sysRoleMenuLambdaQueryWrapper);
        rolePermissionDTO.getMenuIds().forEach(e -> {
            SysRoleMenu menu = new SysRoleMenu();
            menu.setMenuId(e);
            menu.setRoleId(rolePermissionDTO.getRoleId());
            sysRoleMenuMapper.insert(menu);
        });
        /**
         * 用户权限有变更，删除登录状态，需要用户重新登录
         */
      //  tokenStore.removeAccessToken(tokenStore.getAccessToken((OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication()));
    }


}
