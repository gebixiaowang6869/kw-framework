package com.kw.admin.server.service;

import com.kw.admin.common.dto.tenant.SysTenantPageReqDTO;
import com.kw.admin.common.dto.tenant.SysTenantSaveOrUpdateDTO;
import com.kw.admin.common.vo.tenant.SysTenantPageReqVO;
import com.kw.admin.server.entity.SysTenant;
import com.baomidou.mybatisplus.extension.service.IService;

import com.kw.framework.common.croe.vo.PageResultVO;

/**
 * <p>
 * 租户表 服务类
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */

public interface ISysTenantService extends IService<SysTenant> {


    Boolean addSysTenant(SysTenantSaveOrUpdateDTO sysTenant);

    Boolean deleteByIds(String ids);

    Boolean updateSysTenantById(SysTenantSaveOrUpdateDTO sysTenantSaveDTO);


    PageResultVO<SysTenantPageReqVO> getPage(SysTenantPageReqDTO pageParam);


}
