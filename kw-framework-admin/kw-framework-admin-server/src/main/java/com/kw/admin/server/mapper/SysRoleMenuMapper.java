package com.kw.admin.server.mapper;

import com.kw.admin.server.entity.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色菜单表 Mapper 接口
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
