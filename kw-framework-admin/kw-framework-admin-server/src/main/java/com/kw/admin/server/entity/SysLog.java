package com.kw.admin.server.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.kw.framework.mybatis.entity.BaseEntity;
import com.kw.framework.mybatis.handler.AESTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

import javax.servlet.annotation.HandlesTypes;


/**
 * <p>
 * SysLog
 * </p>
 *
 * @author kw
 * @since 2022-10-25 17:19:31.887031
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_log")
@ApiModel(value = "sys_log")
public class SysLog extends BaseEntity<SysLog> implements Serializable {

    private static final long serialVersionUID = 1L;



    /**
     *
     */

    @ApiModelProperty(value = "")
    private String type;

    /**
     *
     */

    @ApiModelProperty(value = "")
   // @TableField(typeHandler = AESTypeHandler.class)
    private String title;

    /**
     *
     */

    @ApiModelProperty(value = "")
    private String serviceId;


    /**
     *
     */

    @ApiModelProperty(value = "")
    private String remoteAddr;

    /**
     *
     */

    @ApiModelProperty(value = "")
    private String userAgent;

    /**
     *
     */

    @ApiModelProperty(value = "")
    private String requestUri;

    /**
     *
     */

    @ApiModelProperty(value = "")
    private String method;

    /**
     *
     */

    @ApiModelProperty(value = "")
    private String params;

    /**
     * 执行时间
     */

    @ApiModelProperty(value = "执行时间")
    private String time;


    /**
     *
     */

    @ApiModelProperty(value = "")
    private String exception;

    /**
     * 所属租户
     */

    @ApiModelProperty(value = "所属租户")
    private Integer tenantId;



}