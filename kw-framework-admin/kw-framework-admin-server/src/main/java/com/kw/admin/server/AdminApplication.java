package com.kw.admin.server;

import com.kw.framework.bootstrap.annotation.BootstrapAnnotation;
import org.springframework.boot.SpringApplication;

@BootstrapAnnotation
public class AdminApplication {

    public static void main(String[] args) {

        SpringApplication.run(AdminApplication.class, args);
    }
}
