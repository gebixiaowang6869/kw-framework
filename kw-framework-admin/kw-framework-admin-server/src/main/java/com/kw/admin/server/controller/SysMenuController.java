package com.kw.admin.server.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.kw.admin.common.dto.MenuTreeDTO;
import com.kw.admin.server.service.ISysUserService;
import com.kw.common.security.annotation.Inner;
import com.kw.common.security.utils.SpringContextHolder;
import com.kw.framework.bootstrap.base.BaseController;
import com.kw.framework.common.croe.exception.BusinessException;
import com.kw.framework.common.excel.controller.BaseExcelController;
import com.kw.framework.common.excel.listener.AbstractAnalysisEventListener;
import com.kw.framework.common.excel.vo.ExcelImportErrorMsgVo;
import com.kw.framework.common.log.annotation.SysLog;
import com.kw.framework.common.redis.cache.KwRedisCache;
import com.kw.framework.common.redis.utils.RedissonUtil;
import com.kw.framework.common.sequence.builder.RedisSequenceBuilder;
import com.kw.framework.mybatis.service.HikariMonitorService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import com.kw.admin.server.service.ISysMenuService;
import com.kw.admin.server.entity.SysMenu;
import com.kw.framework.common.croe.vo.Result;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author kw
 * @since 2022-09-15
 */
@RestController
@RequestMapping("/sys-menu")
@Api(tags = "sys-menu")
public class SysMenuController implements BaseExcelController<DataRow> {

    
    @Autowired
    private ISysMenuService iSysMenuService;
    @Autowired
    private ISysUserService iSysUserService;
    @Autowired
    HikariMonitorService hikariMonitorService ;

    @Autowired
    private KwRedisCache kwRedisCache ;
    @Autowired
    private RedissonUtil redissonUtil ;



    @PostMapping("testEasyExcel")
    @ResponseBody
    @Inner
    Result<List<DataRow>> testEasyExcel(@RequestParam("excel") MultipartFile excel) throws IOException {
        //这个EasyExcelListener里已经写入了监听保存的操作,所以外面不需要自己再处理了
        // 这里默认读取第一个sheet
        List<DataRow> data = getListByInputStream(DataRow.class, excel.getInputStream(), new AbstractAnalysisEventListener<DataRow>(false) {
            @Override
            protected String checkParam(DataRow data) {
                return null;
            }
        });
        return Result.success(data);
    }

   /* @PostMapping("testEasyExcel")
    @ResponseBody
    @Inner
    String testEasyExcel(@RequestParam("excel") MultipartFile excel) throws IOException {
        //这个EasyExcelListener里已经写入了监听保存的操作,所以外面不需要自己再处理了
        // 这里默认读取第一个sheet
        String fileName = "example.xlsx";
        ExcelListener listener = new ExcelListener(false);
        EasyExcel.read(excel.getInputStream(), DataRow.class, listener)
                .sheet()
                .doRead();
        List<DataRow> data = listener.getData();
        return "success";
    }*/



    /**
     * 导出数据
     * */
    @GetMapping("/export/user")
    @Inner
    public void exportUserExcel(HttpServletResponse response) throws IOException {
        List<SysMenu> menuList = iSysMenuService.list();
        exportUserExcel(response,"菜单","sheet",menuList, SysMenu.class);
    }

    @ApiOperation(value = "testSequence")
    @GetMapping("/testSequence")
    public String testSequence(){
        RedisSequenceBuilder builder = new RedisSequenceBuilder();
        String str = builder.kwRedisCache(kwRedisCache).expireAt(DateUtil.endOfDay(new Date())).redissonUtil(redissonUtil).name("测试").ttl("123456#").min(100L).max(105L).recycleAble(true).build().getNext();
        return str ;
    }
    /**
    * 通过id查询
    */
    @ApiOperation(value = "根据Id获取详情")
    @GetMapping("/getById/{id}")
    @Cacheable(key = "#id" , value = "menuCache#120" , cacheManager = "redisCacheManager")
    public Result getById(@PathVariable(value = "id") Integer id){
        Map map = SpringContextHolder.getBeansOfType(RedisCacheManager.class);
        System.out.println(JSONUtil.toJsonStr(map.keySet()));
        return Result.success(iSysMenuService.getById(id));
    }







    /**
     * ceshi
     */
    @ApiOperation(value = "ceshi")
    @GetMapping("ceshi")
    public Result ceshi(){
        return Result.success(hikariMonitorService.HikariMonitor().getActiveConnections());
    }

    /**
    * 新增
    */
    @ApiOperation(value = "新增")
    @PostMapping("/save")
    @SysLog("新增菜单")
    @PreAuthorize("@pms.hasPermission('sys_menu_add')")
    public Result save(@RequestBody SysMenu sysMenu){
        return Result.success( iSysMenuService.addSysMenu(sysMenu));
    }

    /**
    * 通过id删除
    */
    @ApiOperation(value = "删除")
    @DeleteMapping("/deleteById/{id}")
    @SysLog("删除菜单")
    @PreAuthorize("@pms.hasPermission('sys_menu_del')")
    public Result delete(@PathVariable(value = "id") String ids){
        return Result.success( iSysMenuService.deleteByIds(ids));
    }

    /**
    * 修改
    */
    @ApiOperation(value = "修改")
    @PutMapping("/update")
    @SysLog("修改菜单")
    @PreAuthorize("@pms.hasPermission('sys_menu_edit')")
    public Result updateById(@RequestBody SysMenu sysMenu){
        return Result.success(  iSysMenuService.updateSysMenuById(sysMenu));
    }


    /**
    * 查询列表
    */
    @ApiOperation(value = "列表(不分页)")
    @GetMapping("/list")
    @PreAuthorize("@pms.hasPermission('sys_dept_del')")
    public Result list(SysMenuReqVo sysMenu ){
    final LambdaQueryWrapper<SysMenu> lambda = new QueryWrapper<SysMenu>().lambda();
        this.buildCondition(lambda,sysMenu);
        lambda.orderBy(true,false, SysMenu::getId);
        return Result.success( iSysMenuService.getList(lambda));
    }

    /**
     * 返回树形菜单集合
     * @return 当前树形菜单集合
     */
    @ApiOperation(value = "获取树形菜单集合")
    @PostMapping("/getMenuTree")
    public Result getMenuTree(@RequestBody MenuTreeDTO dto) {
        Map<String,Object> map = iSysMenuService.getMenuTree(dto.getTenantId(),Arrays.asList(dto.getRoleId()));
        return Result.success(map);
    }



    /**
    * 分页查询
    */
    @ApiOperation(value = "列表(分页)")
    @GetMapping("/page")
    public Result page(SysMenuReqVo pageParam){
        final LambdaQueryWrapper<SysMenu> lambda = new QueryWrapper<SysMenu>().lambda();
        this.buildCondition(lambda,pageParam);
        lambda.orderBy(true,false, SysMenu::getId);
        return Result.success( iSysMenuService.getPage(lambda,new Page<>(pageParam.getPageNo(), pageParam.getPageSize())));
    }


    @ApiOperation(value = "列表(分页)")
    @PostMapping("/testP")
    public Result testP(String username,String password){
        System.out.println(username);
        System.out.println(password);
       return Result.success(username+password);
    }


    /**
        * 构造查询条件
        * @param lambda
        * @param param
        */
        private void buildCondition(LambdaQueryWrapper<SysMenu> lambda, SysMenuReqVo param){
            if(!StringUtils.isEmpty(param.getId())){
                lambda.eq(SysMenu::getId, param.getId());
            }
            if(!StringUtils.isEmpty(param.getName())){
                lambda.eq(SysMenu::getName, param.getName());
            }
            if(!StringUtils.isEmpty(param.getPermission())){
                lambda.eq(SysMenu::getPermission, param.getPermission());
            }
            if(!StringUtils.isEmpty(param.getPath())){
                lambda.eq(SysMenu::getPath, param.getPath());
            }
            if(!StringUtils.isEmpty(param.getParentId())){
                lambda.eq(SysMenu::getParentId, param.getParentId());
            }
            if(!StringUtils.isEmpty(param.getIcon())){
                lambda.eq(SysMenu::getIcon, param.getIcon());
            }
            if(!StringUtils.isEmpty(param.getSort())){
                lambda.eq(SysMenu::getSort, param.getSort());
            }
            if(!StringUtils.isEmpty(param.getKeepAlive())){
                lambda.eq(SysMenu::getKeepAlive, param.getKeepAlive());
            }
            if(!StringUtils.isEmpty(param.getType())){
                lambda.eq(SysMenu::getType, param.getType());
            }
            if(!StringUtils.isEmpty(param.getCreateTime())){
                lambda.eq(SysMenu::getCreateTime, param.getCreateTime());
            }
            if(!StringUtils.isEmpty(param.getUpdateTime())){
                lambda.eq(SysMenu::getUpdateTime, param.getUpdateTime());
            }
            if(!StringUtils.isEmpty(param.getDelFlag())){
                lambda.eq(SysMenu::getDelFlag, param.getDelFlag());
            }
            if(!StringUtils.isEmpty(param.getTenantId())){
                lambda.eq(SysMenu::getTenantId, param.getTenantId());
            }
            if(!StringUtils.isEmpty(param.getCreateBy())){
                lambda.eq(SysMenu::getCreateBy, param.getCreateBy());
            }
            if(!StringUtils.isEmpty(param.getUpdateBy())){
                lambda.eq(SysMenu::getUpdateBy, param.getUpdateBy());
            }
        }


        /**
         * 请求model
         */
        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        private static class SysMenuReqVo extends SysMenu {
            private List<String> createTimeList; // 创建时间起止


            private List<String> updateTimeList; // 更新时间起止


            @ApiModelProperty(value = "页码")
            private Integer pageNo ;
            @ApiModelProperty(value = "每页条目")
            private Integer pageSize ;
        }


}
