package com.kw.admin.server.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kw.admin.common.dto.tenant.SysTenantPageReqDTO;
import com.kw.admin.common.dto.tenant.SysTenantSaveOrUpdateDTO;
import com.kw.admin.common.vo.tenant.SysTenantPageReqVO;
import com.kw.admin.server.entity.SysTenant;
import com.kw.admin.server.mapper.SysTenantMapper;
import com.kw.admin.server.service.ISysTenantService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kw.framework.common.croe.constant.CacheConstants;
import com.kw.framework.common.croe.vo.PageResultVO;
import com.kw.framework.common.croe.utils.POJOConverter;
import com.kw.framework.mybatis.utils.BeanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import java.time.LocalDateTime;

import com.kw.common.security.utils.SecurityUtil;
import com.kw.framework.common.croe.constant.CommonConstant;

/**
 * <p>
 * 租户表 服务实现类
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */
@Service
public class SysTenantServiceImpl extends ServiceImpl<SysTenantMapper, SysTenant> implements ISysTenantService {


    @Override
    public Boolean addSysTenant(SysTenantSaveOrUpdateDTO sysTenantSaveDTO) {
        SysTenant sysTenant = new SysTenant();
        POJOConverter.copy(sysTenantSaveDTO,sysTenant);
        Long userId = SecurityUtil.getUserId();
        if (userId != null) {
            sysTenant.setCreateBy(userId);
        }
        LocalDateTime now = LocalDateTime.now();
        sysTenant.setCreateTime(now);
        sysTenant.setUpdateTime(now);
        sysTenant.setDelFlag(CommonConstant.UN_DELETE_FLAG);
        return this.save(sysTenant);
    }

    @Override
    public Boolean deleteByIds(String ids) {
        String[] idsStrs = ids.split(",");
        for (String id : idsStrs) {
            deleteById(Integer.parseInt(id));
        }
        return true;
    }

    @CacheEvict(value = CacheConstants.CLIENT_DETAILS_KEY, key = "#id")
    public void deleteById(Integer id) {
        this.removeById(id);
    }

    @Override
    @CacheEvict(value = CacheConstants.CLIENT_DETAILS_KEY, key = "#sysTenantSaveDTO.id")
    public Boolean updateSysTenantById(SysTenantSaveOrUpdateDTO sysTenantSaveDTO) {
        SysTenant sysTenant = new SysTenant();
        POJOConverter.copy(sysTenantSaveDTO,sysTenant);
        sysTenant.setUpdateTime(LocalDateTime.now());
        return this.updateById(sysTenant);
    }


    @Override
    public PageResultVO<SysTenantPageReqVO> getPage(SysTenantPageReqDTO pageParam) {

        final LambdaQueryWrapper<SysTenant> lambda = new QueryWrapper<SysTenant>().lambda();
        this.buildCondition(lambda, pageParam);
        lambda.orderBy(true, false, SysTenant::getId);
        PageResultVO<SysTenantPageReqVO> resultPage = BeanUtils.changePage(SysTenantPageReqVO.class, this.page(new Page<>(pageParam.getPageNo(), pageParam.getPageSize()), lambda));
        return resultPage;
    }



    /**
     * 构造查询条件
     *
     * @param lambda
     * @param param
     */
    private void buildCondition(LambdaQueryWrapper<SysTenant> lambda, SysTenantPageReqDTO param) {
        if (ObjectUtil.isNotEmpty(param.getId())) {
            lambda.eq(SysTenant::getId, param.getId());
        }
        if (ObjectUtil.isNotEmpty(param.getName())) {
            lambda.eq(SysTenant::getName, param.getName());
        }
        if (ObjectUtil.isNotEmpty(param.getCode())) {
            lambda.eq(SysTenant::getCode, param.getCode());
        }
        if (ObjectUtil.isNotEmpty(param.getStartTime())) {
            lambda.eq(SysTenant::getStartTime, param.getStartTime());
        }
        if (ObjectUtil.isNotEmpty(param.getEndTime())) {
            lambda.eq(SysTenant::getEndTime, param.getEndTime());
        }
        if (ObjectUtil.isNotEmpty(param.getStatus())) {
            lambda.eq(SysTenant::getStatus, param.getStatus());
        }
        if (ObjectUtil.isNotEmpty(param.getCreateTime())) {
            lambda.eq(SysTenant::getCreateTime, param.getCreateTime());
        }
        if (ObjectUtil.isNotEmpty(param.getUpdateTime())) {
            lambda.eq(SysTenant::getUpdateTime, param.getUpdateTime());
        }
        if (ObjectUtil.isNotEmpty(param.getCreateBy())) {
            lambda.eq(SysTenant::getCreateBy, param.getCreateBy());
        }
        if (ObjectUtil.isNotEmpty(param.getUpdateBy())) {
            lambda.eq(SysTenant::getUpdateBy, param.getUpdateBy());
        }
    }
}
