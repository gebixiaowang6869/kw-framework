package com.kw.admin.server.service;

import com.kw.admin.common.dto.RolePermissionDTO;
import com.kw.admin.server.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 系统角色表 服务类
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */

public interface ISysRoleService extends IService<SysRole> {


    Boolean addSysRole(SysRole sysRole);

    Boolean deleteByIds(String ids);

    Boolean updateSysRoleById(SysRole sysRole);

    List<SysRole> getList(LambdaQueryWrapper<SysRole> lambda);

    IPage<SysRole> getPage(LambdaQueryWrapper<SysRole> lambda, Page page);


    List<SysRole> getRolesByUserId(Long userId,Integer tenantId);

    /**
     * 修改角色权限
     * @param rolePermissionDTO
     */
    void updateRolePermission(RolePermissionDTO rolePermissionDTO);
}
