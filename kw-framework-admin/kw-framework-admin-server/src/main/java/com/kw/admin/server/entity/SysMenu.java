package com.kw.admin.server.entity;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.kw.framework.common.croe.enums.MenuTypeEnum;
import com.kw.framework.common.excel.convert.EnumToStringConvert;
import com.kw.framework.mybatis.interfaces.DefaultEnumTypeHandler;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 菜单权限表
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName(value="sys_menu", autoResultMap = true)
@ApiModel(value="sys_menu")
@ExcelIgnoreUnannotated
public class SysMenu extends Model<SysMenu> implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 菜单ID
     */
    @ApiModelProperty(value = "菜单ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    //@TableField(typeHandler = AESTypeHandler.class)
    @ExcelProperty(value="菜单名",index = 0)
    private String name;
    @ExcelProperty(value="permission",index = 1)
    private String permission;
    private String path;
    /**
     * 父菜单ID
     */
    @ApiModelProperty(value = "父菜单ID")
    private Long parentId;
    private String icon;
    /**
     * 排序值
     */
    @ApiModelProperty(value = "排序值")
    private Integer sort;
    private String keepAlive;

    @ExcelProperty(value = "类型",index = 2 ,converter = EnumToStringConvert.class)
    private MenuTypeEnum type;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    @TableLogic
    private Integer delFlag;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private Integer tenantId;
    private Long createBy;
    private Long updateBy;
    private Integer status ;



}
