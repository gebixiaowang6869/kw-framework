package com.kw.admin.server.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.kw.admin.server.entity.SysMenu;
import com.kw.admin.server.mapper.SysMenuMapper;
import com.kw.admin.server.service.ISysMenuService;
import com.kw.framework.common.croe.enums.MenuTypeEnum;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜单权限表 服务实现类
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {



    @Override
    public Boolean addSysMenu(SysMenu sysMenu) {
   /*     Long userId = SecurityUtil.getUserId();
        if (userId != null) {
            sysMenu.setCreateBy(userId);
        }
        // entity.setId(IdUtils.getNextId());
        LocalDateTime now = LocalDateTime.now();
        sysMenu.setCreateTime(now);
        sysMenu.setUpdateTime(now);
        sysMenu.setDelFlag(CommonConstant.UN_DELETE_FLAG);*/
        return this.save(sysMenu);
    }

    @Override
    public Boolean deleteByIds(String ids) {
        String[] idsStrs = ids.split(",");
        for (String id : idsStrs) {
            this.removeById(Integer.parseInt(id));
        }
        return true;
    }

    @Override
    public Boolean updateSysMenuById(SysMenu sysMenu) {
        sysMenu.setUpdateTime(LocalDateTime.now());
        return this.updateById(sysMenu);
    }

    @Override
    public List<SysMenu> getList(LambdaQueryWrapper<SysMenu> lambda) {
        return this.list(lambda);
    }

    @Override
    public IPage<SysMenu> getPage(LambdaQueryWrapper<SysMenu> lambda, Page pageParam) {

        IPage<SysMenu> page = this.page(pageParam, lambda);
        return page;
    }

    @Override
    public List<SysMenu> findMenuByRoleId(Long roleId) {
        return baseMapper.findMenuByRoleId(roleId);
    }

    /**
     * 查询菜单
     *
     * @param all      全部菜单
     * @param type     类型
     * @param parentId 父节点ID
     * @return
     */
    @Override
    public List<Tree<Long>> filterMenu(Set<SysMenu> all, String type, Long parentId) {
        List<TreeNode<Long>> collect = all.stream().filter(menuTypePredicate(type)).map(getNodeFunction(null))
                .collect(Collectors.toList());
        Long parent = parentId == null ? -1L : parentId;
        return TreeUtil.build(collect, parent);
    }

    /**
     * 查询菜单
     *
     * @param all 全部菜单
     * @return
     */
    @Override
    public List<Tree<Long>> filterMenu(List<SysMenu> all, List<Long> menuIds) {
        List<TreeNode<Long>> collect = all.stream().map(getNodeFunction(menuIds))
                .collect(Collectors.toList());
        Long parent = -1L;
        return TreeUtil.build(collect, parent);
    }

    @Override
    public Map<String, Object> getMenuTree(Integer tenantId,List<Long> roles) {
        LambdaQueryWrapper<SysMenu> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (tenantId != null) {
            lambdaQueryWrapper.eq(SysMenu::getTenantId, tenantId);
        }
        lambdaQueryWrapper.orderByAsc(SysMenu::getSort);
        List<SysMenu> sysMenuList = list(lambdaQueryWrapper);
        List<SysMenu> menuList = Lists.newArrayList();
        System.out.println(roles.get(0));
        roles.forEach(roleId -> menuList.addAll(findMenuByRoleId(roleId)));
        Map<String, Object> map = new HashMap<>();
        map.put("tree", filterMenu(sysMenuList, menuList.stream().map(SysMenu::getId).collect(Collectors.toList())));
        map.put("roles", menuList.stream().map(SysMenu::getId).collect(Collectors.toList()));
        return map;
    }

    @NotNull
    private Function<SysMenu, TreeNode<Long>> getNodeFunction(List<Long> menuIds) {
        return menu -> {
            TreeNode<Long> node = new TreeNode<>();
            node.setId(menu.getId());
            node.setName(menu.getName());
            node.setParentId(menu.getParentId());
            node.setWeight(menu.getSort());
            // 扩展属性
            Map<String, Object> extra = new HashMap<>();
            extra.put("icon", menu.getIcon());
            extra.put("path", menu.getPath());
            extra.put("type", menu.getType());
            extra.put("permission", menu.getPermission());
            extra.put("label", menu.getName());
            extra.put("sort", menu.getSort());
            extra.put("keepAlive", menu.getKeepAlive());
            extra.put("status", menu.getStatus());
            if (CollectionUtil.isNotEmpty(menuIds) && menuIds.contains(menu.getId())) {
                extra.put("hasMenu", 1);
            } else {
                extra.put("hasMenu", 0);
            }
            node.setExtra(extra);
            return node;
        };
    }

    /**
     * menu 类型断言
     *
     * @param type 类型
     * @return Predicate
     */
    private Predicate<SysMenu> menuTypePredicate(String type) {
        return vo -> {
            if (MenuTypeEnum.TOP_MENU.getMessage().equals(type)) {
                return MenuTypeEnum.TOP_MENU.equals(vo.getType());
            }
            // 其他查询 左侧 + 顶部
            return !MenuTypeEnum.BUTTON.getMessage().equals(vo.getType());
        };
    }


}
