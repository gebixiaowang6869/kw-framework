package com.kw.admin.server.service;

import com.kw.admin.server.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
/**
 * <p>
 * 角色菜单表 服务类
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */

public interface ISysRoleMenuService extends IService<SysRoleMenu> {


        Boolean addSysRoleMenu(SysRoleMenu sysRoleMenu);

        Boolean deleteByIds(String ids);

        Boolean updateSysRoleMenuById(SysRoleMenu sysRoleMenu);

        List<SysRoleMenu> getList(LambdaQueryWrapper<SysRoleMenu> lambda);

        IPage<SysRoleMenu> getPage(LambdaQueryWrapper<SysRoleMenu> lambda, Page page);


}
