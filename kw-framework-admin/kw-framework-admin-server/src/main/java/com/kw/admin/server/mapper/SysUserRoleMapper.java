package com.kw.admin.server.mapper;

import com.kw.admin.server.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
