package com.kw.admin.server.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.kw.admin.server.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统角色表 Mapper 接口
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 通过用户ID，查询角色信息
     * @param userId
     * @return
     */
   // @InterceptorIgnore(tenantLine = "1")
    List<SysRole> listRolesByUserId(@Param("userId") Long userId , @Param("tenantId") Integer tenantId);
}
