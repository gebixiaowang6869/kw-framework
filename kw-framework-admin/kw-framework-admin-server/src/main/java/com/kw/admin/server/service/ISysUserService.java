package com.kw.admin.server.service;

import com.alibaba.excel.EasyExcel;
import com.kw.admin.common.dto.user.SysUserAddDTO;
import com.kw.admin.common.dto.user.UserNameTokenDTO;
import com.kw.admin.common.vo.SysUserPageVO;
import com.kw.admin.common.vo.UserInfoVO;
import com.kw.admin.server.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kw.framework.common.croe.vo.PageResultVO;
import com.kw.framework.mybatis.base.IBaseService;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */

public interface ISysUserService extends IBaseService<SysUser> {
    Boolean addSysUser(SysUserAddDTO sysUserAddDTO);



    Boolean updateSysUserById(SysUserAddDTO sysUserAddDTO);

    List<SysUser> getList(LambdaQueryWrapper<SysUser> lambda);

    PageResultVO<SysUserPageVO> getPage(LambdaQueryWrapper<SysUser> lambda, Page page);

    UserInfoVO userInfo(String name);

    UserInfoVO userInfoByToken();

    Boolean updateTokenById(UserNameTokenDTO dto);
}
