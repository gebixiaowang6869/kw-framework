package com.kw.admin.server.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kw.admin.common.dto.user.SysUserAddDTO;
import com.kw.admin.common.dto.user.UserNameTokenDTO;
import com.kw.admin.common.vo.SysUserPageVO;
import com.kw.admin.common.vo.SysUserVO;
import com.kw.admin.common.vo.UserInfoVO;
import com.kw.admin.server.entity.SysMenu;
import com.kw.admin.server.entity.SysRole;
import com.kw.admin.server.entity.SysUser;
import com.kw.admin.server.entity.SysUserRole;
import com.kw.admin.server.mapper.SysUserMapper;
import com.kw.admin.server.service.ISysMenuService;
import com.kw.admin.server.service.ISysRoleService;
import com.kw.admin.server.service.ISysUserRoleService;
import com.kw.admin.server.service.ISysUserService;
import com.kw.common.security.utils.SecurityUtil;
import com.kw.framework.common.croe.vo.PageResultVO;
import com.kw.framework.common.croe.enums.ResultEnum;
import com.kw.framework.common.croe.exception.BusinessException;
import com.kw.framework.common.croe.utils.POJOConverter;
import com.kw.framework.mybatis.base.BaseServiceImpl;
import com.kw.framework.mybatis.utils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */
@Service
public class SysUserServiceImpl extends BaseServiceImpl<SysUserMapper, SysUser> implements ISysUserService {


    @Autowired
    private ISysRoleService iSysRoleService;


    @Autowired
    private ISysMenuService iSysMenuService;

    @Autowired
    private ISysUserRoleService iSysUserRoleService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public Boolean addSysUser(SysUserAddDTO sysUserAddDTO) {
        Long userId = SecurityUtil.getUserId();

        SysUser sysUser = new SysUser();
        POJOConverter.copy(sysUserAddDTO,sysUser);
        if(ObjectUtil.isNotEmpty(sysUser.getPassword())){
            sysUser.setPassword(passwordEncoder.encode(sysUser.getPassword()));
        }
        //sysUser.setTenantId(TenantContext.get());
        this.save(sysUser);

        /**
         * 设置用户角色
         */
        if(CollectionUtil.isNotEmpty(sysUserAddDTO.getRoleIds())){
            sysUserAddDTO.getRoleIds().forEach(e->{
                SysUserRole userRole = new SysUserRole();
                userRole.setRoleId(e);
                userRole.setUserId(sysUser.getId());
                iSysUserRoleService.save(userRole);
            });
        }
        return true ;
    }



    @Override
    @Transactional
    public Boolean updateSysUserById(SysUserAddDTO sysUserAddDTO) {
        SysUser sysUser = new SysUser();
        POJOConverter.copy(sysUserAddDTO,sysUser);
        if(CollectionUtil.isNotEmpty(sysUserAddDTO.getRoleIds())){
            LambdaQueryWrapper<SysUserRole> sysUserRoleLambdaQueryWrapper = new LambdaQueryWrapper<>();
            sysUserRoleLambdaQueryWrapper.eq(SysUserRole::getUserId,sysUserAddDTO.getId());
            iSysUserRoleService.remove(sysUserRoleLambdaQueryWrapper);
            sysUserAddDTO.getRoleIds().forEach(e->{
                SysUserRole userRole = new SysUserRole();
                userRole.setRoleId(e);
                userRole.setUserId(sysUser.getId());
                iSysUserRoleService.save(userRole);
            });
        }
        return this.updateById(sysUser);
    }

    @Override
    public List<SysUser> getList(LambdaQueryWrapper<SysUser> lambda) {
        return this.list(lambda);
    }

    @Override
    public PageResultVO<SysUserPageVO> getPage(LambdaQueryWrapper<SysUser> lambda, Page pageParam) {

        IPage<SysUser> page = this.page(pageParam, lambda);
        PageResultVO<SysUserPageVO> resultPage = BeanUtils.changePage(SysUserPageVO.class,page);
        resultPage.getRecords().forEach(e->{
            e.setRoleIds(iSysRoleService.getRolesByUserId(e.getId(),e.getTenantId()).stream().map(SysRole::getId).collect(Collectors.toList()));
        });
        return resultPage;
    }

    private SysUser getSysUserByName(String name){
        return this.getOne(Wrappers.<SysUser>query().lambda().eq(SysUser::getUsername, name).last(" limit 1 "));
    }

    @Override
    public UserInfoVO userInfo(String name) {

        SysUser sysUser = this.getSysUserByName(name);
        if(sysUser==null){
            throw new BusinessException(ResultEnum.USERNAME_OR_PASSWORD_ERROR);
        }
        SysUserVO sysUserVO = new SysUserVO();
        POJOConverter.copy(sysUser,sysUserVO);

        UserInfoVO userInfoVO = new UserInfoVO();
        userInfoVO.setSysUserVO(sysUserVO);

        List<Long> roleIds = iSysRoleService.getRolesByUserId(sysUser.getId(),sysUser.getTenantId()).stream().map(SysRole::getId).collect(Collectors.toList()) ;

        userInfoVO.setRoles(ArrayUtil.toArray(roleIds, Long.class));

        Set<String> permissions = new HashSet<>();
        roleIds.forEach(roleId -> {
            List<String> permissionList = iSysMenuService.findMenuByRoleId(roleId).stream()
                    .filter(menu -> StrUtil.isNotEmpty(menu.getPermission())).map(SysMenu::getPermission)
                    .collect(Collectors.toList());
            permissions.addAll(permissionList);
        });

        userInfoVO.setPermissions(ArrayUtil.toArray(permissions, String.class));
        return userInfoVO;
    }

    @Override
    public UserInfoVO userInfoByToken() {
        String name = SecurityUtil.getKwSecurityUser().getName();
        return userInfo(name);
    }

    @Override
    public Boolean updateTokenById(UserNameTokenDTO dto) {
        SysUser sysUser = new SysUser();
        sysUser.setToken(dto.getToken());
        LambdaUpdateWrapper<SysUser> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(SysUser::getId,dto.getId());
        return update(sysUser,updateWrapper);
    }


}
