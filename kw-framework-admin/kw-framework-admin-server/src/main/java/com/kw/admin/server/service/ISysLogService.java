package com.kw.admin.server.service;

import com.kw.admin.common.dto.sysLog.SysLogPageReqDTO;
import com.kw.admin.common.dto.sysLog.SysLogSaveOrUpdateDTO;
import com.kw.admin.common.vo.sysLog.SysLogPageReqVO;
import com.kw.admin.server.entity.SysLog;
import com.baomidou.mybatisplus.extension.service.IService;

import com.kw.framework.common.croe.vo.PageResultVO;
import com.kw.framework.mybatis.base.IBaseService;

/**
 * <p>
 *  SysLog service 接口
 * </p>
 *
 * @author kw
 * @since 2022-10-25 17:19:31.887031
 */

public interface ISysLogService extends IBaseService<SysLog> {


    Boolean addSysLog(SysLogSaveOrUpdateDTO sysLogSaveOrUpdateDTO);


    Boolean updateSysLogById(SysLogSaveOrUpdateDTO sysLogSaveOrUpdateDTO);


    PageResultVO<SysLogPageReqVO> getPage(SysLogPageReqDTO pageParam);


}