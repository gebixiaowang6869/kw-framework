package com.kw.admin.server.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.kw.admin.common.dto.RolePermissionDTO;
import com.kw.framework.common.log.annotation.SysLog;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import com.kw.admin.server.service.ISysRoleService;
import com.kw.admin.server.entity.SysRole;
import com.kw.framework.common.croe.vo.Result;

import java.util.*;

import lombok.Data;


/**
 * @author kw
 * @since 2022-09-15
 */
@RestController
@RequestMapping("/sysRole")
@Api(tags = "sysRole")
public class SysRoleController {


    @Autowired
    private ISysRoleService iSysRoleService;


    /**
     * 通过id查询
     */
    @ApiOperation(value = "根据Id获取详情")
    @GetMapping("/getById/{id}")
    public Result getById(@PathVariable(value = "id") Integer id) {
        return Result.success(iSysRoleService.getById(id));
    }


    /**
     * 修改角色权限
     */
    @ApiOperation(value = "修改角色权限")
    @PostMapping("/rolePermission")
    @SysLog("修改角色权限")
    @PreAuthorize("@pms.hasPermission('sys_role_perm')")
    public Result updateRolePermission(@RequestBody RolePermissionDTO rolePermissionDTO) {
        iSysRoleService.updateRolePermission(rolePermissionDTO);
        return Result.success();
    }

    /**
     * 新增
     */
    @ApiOperation(value = "新增")
    @PostMapping("/save")
    @SysLog("新增角色")
    @PreAuthorize("@pms.hasPermission('sys_role_del')")
    public Result save(@RequestBody SysRole sysRole) {
        return Result.success(iSysRoleService.addSysRole(sysRole));
    }

    /**
     * 通过id删除
     */
    @ApiOperation(value = "删除")
    @SysLog("删除角色")
    @DeleteMapping("/deleteById/{id}")
    @PreAuthorize("@pms.hasPermission('sys_role_add')")
    public Result delete(@PathVariable(value = "id") String ids) {
        return Result.success(iSysRoleService.deleteByIds(ids));
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改")
    @PutMapping("/update")
    @PreAuthorize("@pms.hasPermission('sys_role_edit')")
    public Result updateById(@RequestBody SysRole sysRole) {
        return Result.success(iSysRoleService.updateSysRoleById(sysRole));
    }


    /**
     * 查询列表
     */
    @ApiOperation(value = "列表(不分页)")
    @GetMapping("/list")
    public Result list(SysRoleReqVo sysRole) {
        final LambdaQueryWrapper<SysRole> lambda = new QueryWrapper<SysRole>().lambda();
        this.buildCondition(lambda, sysRole);
        lambda.orderBy(true, false, SysRole::getId);
        return Result.success(iSysRoleService.getList(lambda));
    }

    /**
     * 分页查询
     */
    @ApiOperation(value = "列表(分页)")
    @PostMapping("/page")
    public Result page(@RequestBody SysRoleReqVo pageParam) {
        final LambdaQueryWrapper<SysRole> lambda = new QueryWrapper<SysRole>().lambda();
        this.buildCondition(lambda, pageParam);
        lambda.orderBy(true, false, SysRole::getId);
        return Result.success(iSysRoleService.getPage(lambda, new Page<>(pageParam.getPageNo(), pageParam.getPageSize())));
    }


    /**
     * 构造查询条件
     *
     * @param lambda
     * @param param
     */
    private void buildCondition(LambdaQueryWrapper<SysRole> lambda, SysRoleReqVo param) {
        if (!StringUtils.isEmpty(param.getId())) {
            lambda.eq(SysRole::getId, param.getId());
        }
        if (!StringUtils.isEmpty(param.getRoleName())) {
            lambda.eq(SysRole::getRoleName, param.getRoleName());
        }
        if (!StringUtils.isEmpty(param.getRoleCode())) {
            lambda.eq(SysRole::getRoleCode, param.getRoleCode());
        }
        if (!StringUtils.isEmpty(param.getRoleDesc())) {
            lambda.eq(SysRole::getRoleDesc, param.getRoleDesc());
        }
        if (!StringUtils.isEmpty(param.getDsType())) {
            lambda.eq(SysRole::getDsType, param.getDsType());
        }
        if (!StringUtils.isEmpty(param.getDsScope())) {
            lambda.eq(SysRole::getDsScope, param.getDsScope());
        }
        if (ObjectUtil.isNotEmpty(param.getCreateTime())) {
            lambda.eq(SysRole::getCreateTime, param.getCreateTime());
        }
        if (ObjectUtil.isNotEmpty(param.getUpdateTime())) {
            lambda.eq(SysRole::getUpdateTime, param.getUpdateTime());
        }
        if (ObjectUtil.isNotEmpty(param.getTenantId())) {
            lambda.eq(SysRole::getTenantId, param.getTenantId());
        }
        if (!StringUtils.isEmpty(param.getCreateBy())) {
            lambda.eq(SysRole::getCreateBy, param.getCreateBy());
        }
        if (!StringUtils.isEmpty(param.getUpdateBy())) {
            lambda.eq(SysRole::getUpdateBy, param.getUpdateBy());
        }
    }


    /**
     * 请求model
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    private static class SysRoleReqVo extends SysRole {
        private List<String> createTimeList; // 起止
        private List<String> updateTimeList; // 起止
        @ApiModelProperty(value = "页码")
        private Integer pageNo;
        @ApiModelProperty(value = "每页条目")
        private Integer pageSize;
    }


}
