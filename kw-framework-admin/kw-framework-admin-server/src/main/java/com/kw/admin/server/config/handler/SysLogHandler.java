package com.kw.admin.server.config.handler;

import com.kw.admin.common.dto.sysLog.SysLogSaveOrUpdateDTO;
import com.kw.admin.server.service.ISysLogService;
import com.kw.framework.common.croe.utils.POJOConverter;
import com.kw.framework.common.log.entity.SysLogDTO;
import com.kw.framework.common.log.handler.LogHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SysLogHandler implements LogHandler {

    @Autowired
    ISysLogService iSysLogService ;

    @Override
    public void saveLog(SysLogDTO sysLogDTO) {
        SysLogSaveOrUpdateDTO sysLog = new SysLogSaveOrUpdateDTO();
        POJOConverter.copy(sysLogDTO,sysLog);
        iSysLogService.addSysLog(sysLog);
    }
}
