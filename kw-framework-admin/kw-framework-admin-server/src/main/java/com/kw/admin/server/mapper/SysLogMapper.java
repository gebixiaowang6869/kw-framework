package com.kw.admin.server.mapper;

import com.kw.admin.server.entity.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  SysLog Mapper 接口
 * </p>
 *
 * @author kw
 * @since 2022-10-25 17:19:31.887031
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}