package com.kw.admin.server.service.impl;

import com.kw.admin.server.entity.SysUserRole;
import com.kw.admin.server.mapper.SysUserRoleMapper;
import com.kw.admin.server.service.ISysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {


        @Override
        public Boolean addSysUserRole(SysUserRole sysUserRole) {
            return this.save(sysUserRole);
        }

        @Override
        public Boolean deleteByIds(String ids) {
            String[] idsStrs = ids.split(",");
            for (String id:idsStrs){
                this.removeById(Integer.parseInt(id));
            }
        return true;
        }

        @Override
        public Boolean updateSysUserRoleById(SysUserRole sysUserRole) {
            return this.updateById(sysUserRole);
        }

        @Override
        public List<SysUserRole> getList(LambdaQueryWrapper<SysUserRole> lambda) {
         return this.list(lambda);
        }

        @Override
        public IPage<SysUserRole> getPage(LambdaQueryWrapper<SysUserRole> lambda ,  Page pageParam) {

            IPage<SysUserRole> page = this.page(pageParam, lambda);
            return page;
        }



}
