package com.kw.admin.server.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kw.admin.common.dto.sysLog.SysLogPageReqDTO;
import com.kw.admin.common.dto.sysLog.SysLogSaveOrUpdateDTO;
import com.kw.admin.common.vo.sysLog.SysLogPageReqVO;
import com.kw.admin.server.entity.SysLog;
import com.kw.admin.server.mapper.SysLogMapper;
import com.kw.admin.server.service.ISysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kw.framework.common.croe.utils.POJOConverter;
import com.kw.framework.mybatis.base.BaseServiceImpl;
import com.kw.framework.mybatis.utils.BeanUtils;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import com.kw.framework.common.croe.vo.PageResultVO;
import cn.hutool.core.collection.CollectionUtil;

/**
 * <p>
 * SysLog service实现类
 * </p>
 *
 * @author kw
 * @since 2022-10-25 17:19:31.887031
 */
@Service
@Slf4j
public class SysLogServiceImpl extends BaseServiceImpl<SysLogMapper, SysLog> implements ISysLogService {

    @XxlJob(value="testTraceId")
    public void test(){
        list();
        log.info("hello wor");
    }

    @Override
    public Boolean addSysLog(SysLogSaveOrUpdateDTO sysLogSaveDTO) {
        SysLog sysLog = new SysLog();
        POJOConverter.copy(sysLogSaveDTO, sysLog);
/*        LocalDateTime now = LocalDateTime.now();
        sysLog.setCreateTime(now);
        sysLog.setUpdateTime(now);
        sysLog.setDelFlag(CommonConstant.UN_DELETE_FLAG);*/
        return this.save(sysLog);
    }



    public void deleteById(Integer id) {
        this.removeById(id);
    }

    @Override
    public Boolean updateSysLogById(SysLogSaveOrUpdateDTO sysLogSaveDTO) {
        SysLog sysLog = new SysLog();
        POJOConverter.copy(sysLog, sysLog);
        //sysLog.setUpdateTime(LocalDateTime.now());
        return this.updateById(sysLog);
    }


    @Override
    public PageResultVO<SysLogPageReqVO> getPage(SysLogPageReqDTO pageParam) {

        final LambdaQueryWrapper<SysLog> lambda = new QueryWrapper<SysLog>().lambda();
        this.buildCondition(lambda, pageParam);
        lambda.orderBy(true, false, SysLog::getId);
        PageResultVO<SysLogPageReqVO> resultPage = BeanUtils.changePage(SysLogPageReqVO.class, this.page(new Page<>(pageParam.getPageNo(), pageParam.getPageSize()), lambda));
        return resultPage;
    }


    /**
     * 构造查询条件
     *
     * @param lambda
     * @param param
     */
    private void buildCondition(LambdaQueryWrapper<SysLog> lambda, SysLogPageReqDTO param) {


        if (ObjectUtil.isNotEmpty(param.getId())) {
            lambda.eq(SysLog::getId, param.getId());
        }


        if (ObjectUtil.isNotEmpty(param.getType())) {
            lambda.eq(SysLog::getType, param.getType());
        }


        if (ObjectUtil.isNotEmpty(param.getTitle())) {
            lambda.eq(SysLog::getTitle, param.getTitle());
        }


        if (ObjectUtil.isNotEmpty(param.getServiceId())) {
            lambda.eq(SysLog::getServiceId, param.getServiceId());
        }


        if (ObjectUtil.isNotEmpty(param.getCreateBy())) {
            lambda.eq(SysLog::getCreateBy, param.getCreateBy());
        }


        if (ObjectUtil.isNotEmpty(param.getUpdateBy())) {
            lambda.eq(SysLog::getUpdateBy, param.getUpdateBy());
        }


        if (CollectionUtil.isNotEmpty(param.getCreateTimeList())) {
            lambda.le(SysLog::getCreateTime, param.getCreateTimeList().get(1));
            lambda.ge(SysLog::getCreateTime, param.getCreateTimeList().get(0));
        }


        if (CollectionUtil.isNotEmpty(param.getUpdateTimeList())) {
            lambda.le(SysLog::getUpdateTime, param.getUpdateTimeList().get(1));
            lambda.ge(SysLog::getUpdateTime, param.getUpdateTimeList().get(0));
        }


        if (ObjectUtil.isNotEmpty(param.getRemoteAddr())) {
            lambda.eq(SysLog::getRemoteAddr, param.getRemoteAddr());
        }


        if (ObjectUtil.isNotEmpty(param.getUserAgent())) {
            lambda.eq(SysLog::getUserAgent, param.getUserAgent());
        }


        if (ObjectUtil.isNotEmpty(param.getRequestUri())) {
            lambda.eq(SysLog::getRequestUri, param.getRequestUri());
        }


        if (ObjectUtil.isNotEmpty(param.getMethod())) {
            lambda.eq(SysLog::getMethod, param.getMethod());
        }


        if (ObjectUtil.isNotEmpty(param.getParams())) {
            lambda.eq(SysLog::getParams, param.getParams());
        }


        if (ObjectUtil.isNotEmpty(param.getTime())) {
            lambda.eq(SysLog::getTime, param.getTime());
        }


        if (ObjectUtil.isNotEmpty(param.getDelFlag())) {
            lambda.eq(SysLog::getDelFlag, param.getDelFlag());
        }


        if (ObjectUtil.isNotEmpty(param.getException())) {
            lambda.eq(SysLog::getException, param.getException());
        }


        if (ObjectUtil.isNotEmpty(param.getTenantId())) {
            lambda.eq(SysLog::getTenantId, param.getTenantId());
        }


    }
}