package com.kw.admin.server.service;

import cn.hutool.core.lang.tree.Tree;
import com.kw.admin.server.entity.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 菜单权限表 服务类
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */

public interface ISysMenuService extends IService<SysMenu> {


    Boolean addSysMenu(SysMenu sysMenu);

    Boolean deleteByIds(String ids);

    Boolean updateSysMenuById(SysMenu sysMenu);

    List<SysMenu> getList(LambdaQueryWrapper<SysMenu> lambda);

    IPage<SysMenu> getPage(LambdaQueryWrapper<SysMenu> lambda, Page page);

    List<SysMenu> findMenuByRoleId(Long roleId);

    public List<Tree<Long>> filterMenu(Set<SysMenu> all, String type, Long parentId);

    public List<Tree<Long>> filterMenu(List<SysMenu> all,List<Long> menuIds);

    Map<String, Object> getMenuTree(Integer tenantId,List<Long> rules);
}
