package com.kw.admin.server.entity;

import com.kw.framework.common.croe.enums.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MenuStatusEnum implements BaseEnum<Integer> {
    /**
     * 左侧菜单
     */
    LEFT_MENU(0, "left"),


    /**
     * 按钮
     */
    BUTTON(1, "button");

    /**
     * 类型
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String message;

    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
