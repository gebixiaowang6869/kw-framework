package com.kw.admin.server.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.kw.admin.server.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 菜单权限表 Mapper 接口
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {
    /**
     * 通过角色编号查询URL 权限
     * @param roleId 角色ID
     * @return 菜单列表
     */
    //@InterceptorIgnore(tenantLine = "1")
    List<SysMenu> findMenuByRoleId(@Param("roleId") Long roleId);
}
