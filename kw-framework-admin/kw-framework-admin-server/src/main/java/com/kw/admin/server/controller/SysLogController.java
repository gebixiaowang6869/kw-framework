package com.kw.admin.server.controller;

import com.kw.admin.common.dto.sysLog.SysLogPageReqDTO;
import com.kw.admin.common.dto.sysLog.SysLogSaveOrUpdateDTO;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.kw.admin.server.service.ISysLogService;
import com.kw.framework.common.croe.vo.Result;


/**
 * @author kw
 * @since 2022-09-15
 */
@RestController
@RequestMapping("/sysLog")
@Api(tags = "sysLog")
public class SysLogController {



    @Autowired
    private ISysLogService iSysLogService;



    /**
     * 通过id查询
     */
    @ApiOperation(value = "根据Id获取详情")
    @GetMapping("/{id}")
    public Result getById(@PathVariable(value = "id") Integer id) {
        return Result.success(iSysLogService.getById(id));
    }

    /**
     * 新增
     */
    @ApiOperation(value = "新增")
    @PostMapping("/")
    public Result save(@RequestBody SysLogSaveOrUpdateDTO sysLog) {
        return Result.success(iSysLogService.addSysLog(sysLog));
    }

    /**
     * 通过id删除
     */
    @ApiOperation(value = "删除")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable(value = "id") String ids) {
        iSysLogService.deleteByIds(ids);
        return Result.success();
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改")
    @PutMapping("/")
    public Result updateById(@RequestBody SysLogSaveOrUpdateDTO sysLogSaveDTO) {
        return Result.success(iSysLogService.updateSysLogById(sysLogSaveDTO));
    }


    /**
     * 分页查询
     */
    @ApiOperation(value = "列表(分页)")
    @GetMapping("/")
    public Result page(SysLogPageReqDTO pageParam) {

        return Result.success(iSysLogService.getPage(pageParam));
    }
}