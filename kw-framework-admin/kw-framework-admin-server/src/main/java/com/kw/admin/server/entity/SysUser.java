package com.kw.admin.server.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.kw.framework.mybatis.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_user")
@ApiModel(value="sys_user")
public class SysUser extends BaseEntity<SysUser> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private String username;
    private String password;
    private String salt;
    private String phone;
    private String avatar;
    private String token ;
    /**
     * 部门ID
     */
    @ApiModelProperty(value = "部门ID")
    private Integer deptId;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;
    private Integer status;
    @TableLogic
    private Integer delFlag;
    /**
     * 微信登录openId
     */
    @ApiModelProperty(value = "微信登录openId")
    private String wxOpenid;
    /**
     * 小程序openId
     */
    @ApiModelProperty(value = "小程序openId")
    private String miniOpenid;
    /**
     * QQ openId
     */
    @ApiModelProperty(value = "QQ openId")
    private String qqOpenid;
    /**
     * 码云 标识
     */
    @ApiModelProperty(value = "码云 标识")
    private String giteeLogin;
    /**
     * 开源中国 标识
     */
    @ApiModelProperty(value = "开源中国 标识")
    private String oscId;
    /**
     * 所属租户
     */
    @ApiModelProperty(value = "所属租户")
    private Integer tenantId;
    private Long updateBy;
    private Long createBy;

}
