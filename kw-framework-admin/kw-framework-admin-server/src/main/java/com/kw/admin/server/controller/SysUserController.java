package com.kw.admin.server.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.kw.admin.common.dto.user.SysUserAddDTO;
import com.kw.admin.common.dto.user.UserNameTokenDTO;
import com.kw.admin.common.vo.UserInfoVO;
import com.kw.admin.server.entity.SysMenu;
import com.kw.admin.server.service.ISysMenuService;
import com.kw.framework.bootstrap.base.BaseController;
import com.kw.framework.common.log.annotation.SysLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import com.kw.admin.server.service.ISysUserService;
import com.kw.admin.server.entity.SysUser;
import com.kw.framework.common.croe.vo.Result;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

import lombok.Data;

import javax.servlet.http.HttpServletResponse;


/**
 * @author kw
 * @since 2022-09-15
 */
@RestController
@RequestMapping("/sysUser")
@Api(tags = "sysUser")
@Slf4j
public class SysUserController extends BaseController {


    @Autowired
    private ISysUserService iSysUserService;


    @Autowired
    private ISysMenuService iSysMenuService;




    /**
     * 返回当前用户的树形菜单集合
     * @param type 类型
     * @param parentId 父节点ID
     * @return 当前用户的树形菜单
     */
    @ApiOperation(value = "获取用户基本信息，角色，权限等")
    @GetMapping("/getUserMenu")
    public Result getUserMenu(String type, Long parentId) {
       // log.info("tenantId-----------------"+ TenantContext.get());
        UserInfoVO info = iSysUserService.userInfoByToken();
        // 获取符合条件的菜单
        Set<SysMenu> all = new HashSet<>();
        if(info.getRoles()==null || info.getRoles().length==0){
            return Result.success();
        }
        List<Long> roles = Arrays.asList(info.getRoles());
        roles.forEach(roleId -> all.addAll(iSysMenuService.findMenuByRoleId(roleId)));
        return Result.success(iSysMenuService.filterMenu(all, type, parentId));
    }

    /**
     * 获取用户基本信息，角色，权限等
     */
    @ApiOperation(value = "获取用户基本信息，角色，权限等")
    @PostMapping("/userInfo/{name}")
    public Result userInfo(@PathVariable(value = "name") String name) {
        return Result.success(iSysUserService.userInfo(name));
    }

    @ApiOperation(value = "修改用户最后一次登录token")
    @PostMapping("/updateTokenById")
    public Result updateTokenById(@RequestBody UserNameTokenDTO dto){
        return Result.success(iSysUserService.updateTokenById(dto));
    }


    @ApiOperation(value="根据token获取用户信息")
    @GetMapping("/userInfoByToken")
    public Result userInfoByToken(){
        return Result.success(iSysUserService.userInfoByToken());
    }


    /**
     * 通过id查询
     */
    @ApiOperation(value = "根据Id获取详情")
    @GetMapping("/getById/{id}")
    public Result getById(@PathVariable(value = "id") Integer id) {
        return Result.success(iSysUserService.getById(id));
    }

    /**
     * 新增
     */
    @ApiOperation(value = "新增")
    @PostMapping("/save")
    @SysLog("新增用户")
    @PreAuthorize("@pms.hasPermission('sys_user_add')")
    public Result save(@RequestBody SysUserAddDTO sysUserAddDTO) {
        return Result.success(iSysUserService.addSysUser(sysUserAddDTO));
    }

    /**
     * 通过id删除
     */
    @ApiOperation(value = "删除")
    @DeleteMapping("/deleteById/{id}")
    @SysLog("删除用户")
    @PreAuthorize("@pms.hasPermission('sys_user_del')")
    public Result delete(@PathVariable(value = "id") String ids) {
        iSysUserService.deleteByIds(ids);
        return Result.success();
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改")
    @PostMapping("/update")
    @SysLog("修改用户")
    @PreAuthorize("@pms.hasPermission('sys_user_edit')")
    public Result updateById(@RequestBody SysUserAddDTO sysUserAddDTO) {
        return Result.success(iSysUserService.updateSysUserById(sysUserAddDTO));
    }


    /**
     * 查询列表
     */
    @ApiOperation(value = "列表(不分页)")
    @GetMapping("/list")
    public Result list(SysUserReqVo sysUser) {
        final LambdaQueryWrapper<SysUser> lambda = new QueryWrapper<SysUser>().lambda();
        this.buildCondition(lambda, sysUser);
        lambda.orderBy(true, false, SysUser::getId);
        return Result.success(iSysUserService.getList(lambda));
    }

    /**
     * 分页查询
     */
    @ApiOperation(value = "列表(分页)")
    @GetMapping("/page")
    public Result page(SysUserReqVo pageParam) {
        final LambdaQueryWrapper<SysUser> lambda = new QueryWrapper<SysUser>().lambda();
        this.buildCondition(lambda, pageParam);
        lambda.orderBy(true, false, SysUser::getId);
        return Result.success(iSysUserService.getPage(lambda, new Page<>(pageParam.getPageNo(), pageParam.getPageSize())));
    }


    /**
     * 构造查询条件
     *
     * @param lambda
     * @param param
     */
    private void buildCondition(LambdaQueryWrapper<SysUser> lambda, SysUserReqVo param) {
        if (!ObjectUtil.isEmpty(param.getId())) {
            lambda.eq(SysUser::getId, param.getId());
        }
        if (!StringUtils.isEmpty(param.getUsername())) {
            lambda.eq(SysUser::getUsername, param.getUsername());
        }
        if (!StringUtils.isEmpty(param.getPassword())) {
            lambda.eq(SysUser::getPassword, param.getPassword());
        }
        if (!StringUtils.isEmpty(param.getSalt())) {
            lambda.eq(SysUser::getSalt, param.getSalt());
        }
        if (!StringUtils.isEmpty(param.getPhone())) {
            lambda.eq(SysUser::getPhone, param.getPhone());
        }
        if (!StringUtils.isEmpty(param.getAvatar())) {
            lambda.eq(SysUser::getAvatar, param.getAvatar());
        }
        if (!StringUtils.isEmpty(param.getDeptId())) {
            lambda.eq(SysUser::getDeptId, param.getDeptId());
        }
        if (!StringUtils.isEmpty(param.getCreateTime())) {
            lambda.eq(SysUser::getCreateTime, param.getCreateTime());
        }
        if (!StringUtils.isEmpty(param.getUpdateTime())) {
            lambda.eq(SysUser::getUpdateTime, param.getUpdateTime());
        }
        if (!StringUtils.isEmpty(param.getStatus())) {
            lambda.eq(SysUser::getStatus, param.getStatus());
        }
        if (!StringUtils.isEmpty(param.getDelFlag())) {
            lambda.eq(SysUser::getDelFlag, param.getDelFlag());
        }
        if (!StringUtils.isEmpty(param.getWxOpenid())) {
            lambda.eq(SysUser::getWxOpenid, param.getWxOpenid());
        }
        if (!StringUtils.isEmpty(param.getMiniOpenid())) {
            lambda.eq(SysUser::getMiniOpenid, param.getMiniOpenid());
        }
        if (!StringUtils.isEmpty(param.getQqOpenid())) {
            lambda.eq(SysUser::getQqOpenid, param.getQqOpenid());
        }
        if (!StringUtils.isEmpty(param.getGiteeLogin())) {
            lambda.eq(SysUser::getGiteeLogin, param.getGiteeLogin());
        }
        if (!StringUtils.isEmpty(param.getOscId())) {
            lambda.eq(SysUser::getOscId, param.getOscId());
        }
        if (!StringUtils.isEmpty(param.getTenantId())) {
            lambda.eq(SysUser::getTenantId, param.getTenantId());
        }
        if (!StringUtils.isEmpty(param.getUpdateBy())) {
            lambda.eq(SysUser::getUpdateBy, param.getUpdateBy());
        }
        if (!StringUtils.isEmpty(param.getCreateBy())) {
            lambda.eq(SysUser::getCreateBy, param.getCreateBy());
        }
    }


    /**
     * 请求model
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    private static class SysUserReqVo extends SysUser {
        private List<String> createTimeList; // 创建时间起止


        private List<String> updateTimeList; // 修改时间起止


        @ApiModelProperty(value = "页码")
        private Integer pageNo;
        @ApiModelProperty(value = "每页条目")
        private Integer pageSize;
    }


}
