package com.kw.admin.server.controller;

import com.kw.admin.common.dto.tenant.SysTenantPageReqDTO;
import com.kw.admin.common.dto.tenant.SysTenantSaveOrUpdateDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.kw.admin.server.service.ISysTenantService;
import com.kw.framework.common.croe.vo.Result;


/**
 * @author kw
 * @since 2022-09-15
 */
@RestController
@RequestMapping("/sysTenant")
@Api(tags = "sysTenant")
public class SysTenantController {


    @Autowired
    private ISysTenantService iSysTenantService;


    /**
     * 通过id查询
     */
    @ApiOperation(value = "根据Id获取详情")
    @GetMapping("/{id}")
    public Result getById(@PathVariable(value = "id") Integer id) {
        return Result.success(iSysTenantService.getById(id));
    }

    /**
     * 新增
     */
    @ApiOperation(value = "新增")
    @PostMapping("/")
    public Result save(@RequestBody SysTenantSaveOrUpdateDTO sysTenant) {
        return Result.success(iSysTenantService.addSysTenant(sysTenant));
    }

    /**
     * 通过id删除
     */
    @ApiOperation(value = "删除")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable(value = "id") String ids) {
        return Result.success(iSysTenantService.deleteByIds(ids));
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改")
    @PutMapping("/")
    public Result updateById(@RequestBody SysTenantSaveOrUpdateDTO sysTenantSaveDTO) {
        return Result.success(iSysTenantService.updateSysTenantById(sysTenantSaveDTO));
    }


    /**
     * 分页查询
     */
    @ApiOperation(value = "列表(分页)")
    @GetMapping("/")
    public Result page(SysTenantPageReqDTO pageParam) {

        return Result.success(iSysTenantService.getPage(pageParam));
    }





}
