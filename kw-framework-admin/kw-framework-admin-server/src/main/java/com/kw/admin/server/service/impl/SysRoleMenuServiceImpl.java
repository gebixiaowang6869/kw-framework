package com.kw.admin.server.service.impl;

import com.kw.admin.server.entity.SysRoleMenu;
import com.kw.admin.server.mapper.SysRoleMenuMapper;
import com.kw.admin.server.service.ISysRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

import com.kw.common.security.utils.SecurityUtil;

/**
 * <p>
 * 角色菜单表 服务实现类
 * </p>
 *
 * @author kw
 * @since 2022-09-15
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {


        @Override
        public Boolean addSysRoleMenu(SysRoleMenu sysRoleMenu) {
            Long userId = SecurityUtil.getUserId() ;
            return this.save(sysRoleMenu);
        }

        @Override
        public Boolean deleteByIds(String ids) {
            String[] idsStrs = ids.split(",");
            for (String id:idsStrs){
                this.removeById(Integer.parseInt(id));
            }
        return true;
        }

        @Override
        public Boolean updateSysRoleMenuById(SysRoleMenu sysRoleMenu) {
            return this.updateById(sysRoleMenu);
        }

        @Override
        public List<SysRoleMenu> getList(LambdaQueryWrapper<SysRoleMenu> lambda) {
         return this.list(lambda);
        }

        @Override
        public IPage<SysRoleMenu> getPage(LambdaQueryWrapper<SysRoleMenu> lambda ,  Page pageParam) {

            IPage<SysRoleMenu> page = this.page(pageParam, lambda);
            return page;
        }



}
