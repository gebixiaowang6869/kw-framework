package com.kw.framework.userCommon.vo.api;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserListVO implements Serializable {

    private Long id;

    private String username;

    private String password;

    private String salt;

    private String phone;

    private String avatar;

    private Integer deptId;

    private String lockFlag;

    private String wxOpenid;

    private String miniOpenid;

    private String qqOpenid;

    private String giteeLogin;

    private String oscId;

    private Integer tenantId;

}
