package com.kw.framework.userCommon.dto.api;

import lombok.Data;

/**
 *
 */
@Data
public class UserListDTO {

    private Long id ;

    private String username ;
}
