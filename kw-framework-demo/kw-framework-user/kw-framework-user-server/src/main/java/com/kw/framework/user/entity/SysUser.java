package com.kw.framework.user.entity;

import com.kw.framework.mybatis.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
* <p>
    * 用户表
    * </p>
*
* @author kw
* @since 2022-08-30
*/
    @Data
        @EqualsAndHashCode(callSuper = false)
    @Accessors(chain = true)
    @ApiModel(value="SysUser对象", description="用户表")
    public class SysUser extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    private String username;

    private String password;

    private String salt;

    private String phone;

    private String avatar;

            @ApiModelProperty(value = "部门ID")
    private Integer deptId;


    private String lockFlag;


            @ApiModelProperty(value = "微信登录openId")
    private String wxOpenid;

            @ApiModelProperty(value = "小程序openId")
    private String miniOpenid;

            @ApiModelProperty(value = "QQ openId")
    private String qqOpenid;

            @ApiModelProperty(value = "码云 标识")
    private String giteeLogin;

            @ApiModelProperty(value = "开源中国 标识")
    private String oscId;

            @ApiModelProperty(value = "所属租户")
    private Integer tenantId;


}
