package com.kw.framework.user.service;

import com.kw.framework.mybatis.base.IBaseService;
import com.kw.framework.user.entity.SysUser;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author kw
 * @since 2022-08-30
 */
public interface ISysUserService extends IBaseService<SysUser> {

}
