package com.kw.framework.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kw.framework.user.entity.SysUser;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author kw
 * @since 2022-08-30
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
