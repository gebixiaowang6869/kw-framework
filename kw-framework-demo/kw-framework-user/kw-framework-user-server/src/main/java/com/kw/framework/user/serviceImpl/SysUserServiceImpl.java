package com.kw.framework.user.serviceImpl;

import com.kw.framework.mybatis.base.BaseServiceImpl;
import com.kw.framework.user.entity.SysUser;
import com.kw.framework.user.mapper.SysUserMapper;
import com.kw.framework.user.service.ISysUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author kw
 * @since 2022-08-30
 */
@Service
public class SysUserServiceImpl extends BaseServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

}
