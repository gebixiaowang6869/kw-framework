package com.kw.framework.user;

import com.kw.framework.common.croe.exception.GlobalExceptionHandler;
import com.kw.framework.common.swagger.annotation.EnableKwSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@EnableDiscoveryClient
@EnableKwSwagger2
@EnableFeignClients(basePackages = "com.kw")
@Import(GlobalExceptionHandler.class)
//@EnableKwSecurtity
public class Application {
    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);
    }
}
