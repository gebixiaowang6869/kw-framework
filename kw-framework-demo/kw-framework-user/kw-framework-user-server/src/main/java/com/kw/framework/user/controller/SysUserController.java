package com.kw.framework.user.controller;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.kw.framework.bootstrap.base.BaseController;
import com.kw.framework.common.croe.vo.Result;
import com.kw.framework.common.croe.exception.BusinessException;
import com.kw.framework.common.croe.utils.POJOConverter;
import com.kw.framework.user.entity.SysUser;
import com.kw.framework.user.service.ISysUserService;
import com.kw.framework.userCommon.dto.api.UserListDTO;
import com.kw.framework.userCommon.vo.api.UserListVO;
import com.kw.framwork.userApi.feign.RemoteUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author kw
 * @since 2022-08-30
 */
@RestController
@RequestMapping("/sys-user")
public class SysUserController extends BaseController {

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private RemoteUserService remoteUserService;

    AtomicInteger count = new AtomicInteger(0);

    @GetMapping("getUserList")
    public List<UserListVO> getUserList(UserListDTO userListDTO){
        LambdaQueryWrapper<SysUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if(StrUtil.isNotEmpty(userListDTO.getUsername())){
            lambdaQueryWrapper.eq(SysUser::getUsername,userListDTO.getUsername());
        }
        if(ObjectUtil.isNotEmpty(userListDTO.getId())){
            lambdaQueryWrapper.eq(SysUser::getId,userListDTO.getId());
        }

        List<SysUser> list = sysUserService.list(lambdaQueryWrapper);
        if(count.get()<3){
            count.addAndGet(1);
            throw new BusinessException("feign error");
        }

        return POJOConverter.copyList(list,UserListVO.class);
    }


    @GetMapping("getFeign")
    public Result<List<UserListVO>> getFeign(UserListDTO userListDTO){
        return Result.success(remoteUserService.list(userListDTO));
    }

  /*  @GetMapping("test")
    public String getTest(){
        GlobalAuthEntity entity = SecurityUtil.getKwSecurityUser();
        return JSON.toJSONString(entity);
    }
*/
}
