package com.kw.framwork.userApi.feign;

import com.kw.framework.userCommon.dto.api.UserListDTO;
import com.kw.framework.userCommon.vo.api.UserListVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(contextId = "sysUser", value = "user")
public interface RemoteUserService {
    @GetMapping("/user/sys-user/getUserList")
    public List<UserListVO> list(@SpringQueryMap UserListDTO userListDTO);
}
