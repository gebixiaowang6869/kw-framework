package com.kw.framework.common.sequence.manager;

public interface SequenceMgr {

    public Long getNext(String name);

    public void init();

}
