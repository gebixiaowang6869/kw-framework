package com.kw.framework.common.sequence.builder;

import com.kw.framework.common.sequence.sequence.Sequence;

public interface SequenceBuilder {

    public Sequence build();
}
