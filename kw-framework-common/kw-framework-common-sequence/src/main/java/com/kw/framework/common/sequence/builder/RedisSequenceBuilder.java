package com.kw.framework.common.sequence.builder;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.kw.framework.common.croe.exception.BusinessException;
import com.kw.framework.common.redis.cache.KwRedisCache;
import com.kw.framework.common.redis.utils.RedissonUtil;
import com.kw.framework.common.sequence.manager.impl.RedisSequenceMgr;
import com.kw.framework.common.sequence.sequence.Sequence;
import com.kw.framework.common.sequence.sequence.impl.DefaultSequenceImpl;

import java.util.Date;

/**
 * 发号器
 */
public class RedisSequenceBuilder implements SequenceBuilder{


    private KwRedisCache kwRedisCache ;

    private RedissonUtil redissonUtil ;


    /**
     * 流水号最小值
     */
    private Long min = 1L;
    /**
     * 流水号最大值
     */
    private Long max = 100000L;
    /**
     * 设置过期时间，作用于需要定期重新计算流水号的业务
     */
    private String ttl = "-1";

    /**
     * 超过区间值是否从最小值重新开始算
     */
    private Boolean recycleAble = false ;
    private Date expireAt ;

    private String name ;

    public Sequence build(){
        Assert.notBlank(name);
       /* if(StrUtil.isEmpty(name)){
            throw new BusinessException("name 不能为空");
        }*/
        RedisSequenceMgr mgr = new RedisSequenceMgr();
        mgr.setMax(this.max);
        mgr.setMin(this.min);
        mgr.setKwRedisCache(this.kwRedisCache);
        mgr.setRedissonUtil(this.redissonUtil);
        mgr.setTtl(this.ttl);
        mgr.setExpireAt(this.expireAt);
        mgr.setRecycleAble(this.recycleAble);
        DefaultSequenceImpl defaultSequence = new DefaultSequenceImpl();
        defaultSequence.setName(this.name);
        defaultSequence.setManager(mgr);
        return defaultSequence ;
    }

    public RedisSequenceBuilder kwRedisCache(KwRedisCache kwRedisCache){
        this.kwRedisCache = kwRedisCache ;
        return this ;
    }

    public RedisSequenceBuilder redissonUtil(RedissonUtil redissonUtil){
        this.redissonUtil = redissonUtil ;
        return this ;
    }

    public RedisSequenceBuilder min(Long min){
        this.min = min ;
        return this ;
    }

    public RedisSequenceBuilder max(Long max){
        this.max = max ;
        return this ;
    }

    public RedisSequenceBuilder ttl(String ttl){
        this.ttl = ttl ;
        return this ;
    }

    public RedisSequenceBuilder recycleAble(Boolean recycleAble){
        this.recycleAble = recycleAble ;
        return this ;
    }

    public RedisSequenceBuilder name(String name){
        this.name = name ;
        return this ;
    }

    public RedisSequenceBuilder expireAt(Date expireAt){
        this.expireAt = expireAt ;
        return this ;
    }
}
