package com.kw.framework.common.sequence.sequence.impl;

import com.kw.framework.common.sequence.manager.SequenceMgr;
import com.kw.framework.common.sequence.sequence.DefaultSequence;

public class DefaultSequenceImpl implements DefaultSequence {

    private SequenceMgr manager ;
    private String name ;

    @Override
    public String getNext() {
        return manager.getNext(this.name)+"";
    }

    @Override
    public void setName(String name) {
        this.name = name ;
    }

    @Override
    public void setManager(SequenceMgr manager) {
        this.manager = manager ;
    }
}
