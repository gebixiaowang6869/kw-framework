package com.kw.framework.common.sequence.sequence;

import com.kw.framework.common.sequence.manager.SequenceMgr;

public interface DefaultSequence extends Sequence{
    public void setName(String name);

    public void setManager(SequenceMgr manager);
}
