package com.kw.framework.common.sequence.manager.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.kw.framework.common.croe.constant.CacheConstants;
import com.kw.framework.common.croe.exception.BusinessException;
import com.kw.framework.common.redis.cache.KwRedisCache;
import com.kw.framework.common.redis.utils.RedissonUtil;
import com.kw.framework.common.sequence.manager.SequenceMgr;
import lombok.Data;

import java.util.Date;
import java.util.concurrent.locks.Lock;

/**
 * demo:
 RedisSequenceBuilder builder = new RedisSequenceBuilder();
 String str = builder.kwRedisCache(kwRedisCache).expireAt(DateUtil.endOfDay(new Date())).redissonUtil(redissonUtil).name("测试").ttl("123456#").min(100L).max(105L).recycleAble(true).build().getNext();
 */
@Data
public class RedisSequenceMgr implements SequenceMgr {


    private KwRedisCache kwRedisCache ;

    private RedissonUtil redissonUtil ;

    private final static String KEY_PREFIX = "kw::sequence::" ;

    /**
     * 流水号最小值
     */
    private Long min ;
    /**
     * 流水号最大值
     */
    private Long max ;
    /**
     * 设置过期时间，作用于需要定期重新计算流水号的业务.或许要每次获取都更新ttl则需要在过期时间后面加上#
     */
    private String ttl ;

    /**
     * 设置过期时间，优先级高于ttl
     */
    private Date expireAt ;
    /**
     * 超过区间值是否从最小值重新开始算
     */
    private Boolean recycleAble ;

    @Override
    public Long getNext(String name) {

        String key = KEY_PREFIX+name;
        boolean checkExits = false ;
        /**
         * 判断是否有设置过期时间，且只需要第一次获取号的时候需要设置过期时间
         */
        if(expireAt!=null || (!ttl.contains("#") && ObjectUtil.notEqual(ttl,"-1"))){
            checkExits = kwRedisCache.exists(key);
        }
        Long nextVal = kwRedisCache.incr(key);
        if(nextVal>max){
            /**
             * 发号器发号已超过最大值，判断是否需要重置并发号
             */
            if(recycleAble){
                Lock lock = null ;
                try{
                    lock = redissonUtil.lock(CacheConstants.LOCK_KEY_PREFIX+key);
                    if(lock!=null){
                        /**
                         *  双重判断，避免多线程获取超数时多次重置序列号
                         */
                        nextVal = kwRedisCache.incr(key);
                        if(nextVal>max){
                            nextVal = kwRedisCache.decrBy(key,nextVal-min);
                        }
                    }
                }catch (Exception e ){
                    e.printStackTrace();
                    throw new BusinessException("发号器发号获取失败");
                } finally{
                    if(lock!=null){
                        lock.unlock();
                    }
                }
            }else{
               throw new BusinessException("序列号已经发完，最大的序列号："+max);
            }
        }
        if(expireAt!=null && !checkExits){
            kwRedisCache.expireAt(key,expireAt) ;
        }else{
            /**
             * 判断是否每次获取序列号要不要更新ttl时间
             */
            if(ttl.contains("#")){
                String ttlVal = ttl.substring(0,ttl.length()-1);
                kwRedisCache.expire(key,Long.parseLong(ttlVal)) ;
            }else if(ObjectUtil.notEqual(ttl,"-1") && !checkExits){
                kwRedisCache.expire(key,Long.parseLong(ttl)) ;
            }
        }

        return nextVal;
    }

    @Override
    public void init() {
        if(kwRedisCache==null ||redissonUtil==null ){
            throw new SecurityException("kwRedisCache is null or redissonUtil is null");
        }
    }

}
