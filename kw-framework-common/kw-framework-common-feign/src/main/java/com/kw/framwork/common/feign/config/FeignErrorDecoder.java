package com.kw.framwork.common.feign.config;

import cn.hutool.json.JSONUtil;
import com.kw.framework.common.croe.vo.Result;
import com.kw.framework.common.croe.exception.FeignTransferException;
import feign.Response;
import feign.Util;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.nio.charset.Charset;

@Slf4j
public class FeignErrorDecoder implements ErrorDecoder {


    private final ErrorDecoder defaultErrorDecoder = new Default();


    @Override
    public Exception decode(String methodKey, Response response) {

       /* Exception exception = defaultErrorDecoder.decode(methodKey, response);

        if(exception instanceof RetryableException){
            return exception;
        }*/
      /*  if(response.status() == 504){
            return new RetryableException(response.status(),"504 error", response.request().httpMethod(), null );
        }*/

        log.info("feign transfer error : {} ", methodKey);
        try {
            String body = Util.toString(response.body().asReader(Charset.defaultCharset()));
            Result result = JSONUtil.toBean(body, Result.class);
            log.info("feign error result : {} ", body);
            if (response.status() == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                return new FeignTransferException(result.getMsg());
            }
            return new FeignTransferException(body);
        } catch (Exception e) {
            return e;
        }
    }
}