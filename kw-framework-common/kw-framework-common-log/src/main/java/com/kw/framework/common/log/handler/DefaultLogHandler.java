package com.kw.framework.common.log.handler;

import cn.hutool.json.JSONUtil;
import com.kw.framework.common.log.entity.SysLogDTO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DefaultLogHandler implements LogHandler{
    @Override
    public void saveLog(SysLogDTO sysLogDTO) {
        log.info("operate log： {}", JSONUtil.toJsonStr(sysLogDTO));
    }
}
