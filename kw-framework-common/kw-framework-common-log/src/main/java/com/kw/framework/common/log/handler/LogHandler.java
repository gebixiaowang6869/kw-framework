package com.kw.framework.common.log.handler;

import com.kw.framework.common.log.entity.SysLogDTO;

/**
 * 日志打印处理类，可通过实现此类达到自定义处理日志记录
 */
public interface LogHandler {
    void saveLog(SysLogDTO sysLogDTO);
}
