package com.kw.framework.common.log.util;

import cn.hutool.core.util.URLUtil;
import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.http.HttpUtil;
import com.kw.common.security.utils.SecurityUtil;
import com.kw.framework.common.croe.entity.GlobalAuthEntity;
import com.kw.framework.common.log.entity.SysLogDTO;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
@UtilityClass
public class SysLogUtils {

	public SysLogDTO getSysLog(ProceedingJoinPoint point) {
		HttpServletRequest request = ((ServletRequestAttributes) Objects
				.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
		SysLogDTO sysLog = new SysLogDTO();
		sysLog.setCreateBy(Objects.requireNonNull(SecurityUtil.getUserId()));
		sysLog.setType(LogTypeEnum.NORMAL.getType());
		sysLog.setRemoteAddr(ServletUtil.getClientIP(request));
		sysLog.setRequestUri(URLUtil.getPath(request.getRequestURI()));
		sysLog.setMethod(request.getMethod());
		sysLog.setUserAgent(request.getHeader("user-agent"));
		sysLog.setTenantId(SecurityUtil.getTenantId());
		String method = request.getMethod();
		Object[] args = point.getArgs();
		String params = "";
		// 获取请求参数集合并进行遍历拼接
		if (args.length > 0) {
			if ("POST".equals(method) || "PUT".equals(method) || "DELETE".equals(method)) {
				Object object = args[0];
				// request/response无法使用toJSON
				if (!(object instanceof HttpServletRequest) && !(object instanceof HttpServletResponse)) {
					Map<String, Object> map = getKeyAndValue(object);
					params = HttpUtil.toParams(map);
				}
			} else if ("GET".equals(method)) {
				params = HttpUtil.toParams(request.getParameterMap());
			}
		}
		sysLog.setParams(params);
		sysLog.setServiceId(getClientId());
		return sysLog;
	}

	public SysLogDTO getSysLog(HttpServletRequest request, Long username) {
		SysLogDTO sysLog = new SysLogDTO();
		sysLog.setCreateBy(username);
		sysLog.setType(LogTypeEnum.NORMAL.getType());
		sysLog.setRemoteAddr(ServletUtil.getClientIP(request));
		sysLog.setRequestUri(URLUtil.getPath(request.getRequestURI()));
		sysLog.setMethod(request.getMethod());
		sysLog.setUserAgent(request.getHeader("user-agent"));
		sysLog.setParams(HttpUtil.toParams(request.getParameterMap()));
		sysLog.setServiceId(getClientId());
		return sysLog;
	}

	/**
	 * 获取客户端
	 * @return clientId
	 */
	private String getClientId() {
		return SecurityUtil.getTenantId()+"";
	}

	/**
	 * 获取用户名称
	 * @return username
	 */
	private String getUsername() {
		return SecurityUtil.getUserName();
	}


	public static GlobalAuthEntity getKwSecurityUser() {
		return SecurityUtil.getKwSecurityUser();
	}

	private static Map<String, Object> getKeyAndValue(Object obj) {
		Map<String, Object> map = new HashMap<String, Object>();
		// 得到类对象
		Class userCla = (Class) obj.getClass();
		/* 得到类中的所有属性集合 */
		Field[] fs = userCla.getDeclaredFields();
		for (int i = 0; i < fs.length; i++) {
			Field f = fs[i];
			f.setAccessible(true); // 设置些属性是可以访问的
			Object val = new Object();
			try {
				val = f.get(obj);
				// 得到此属性的值
				map.put(f.getName(), val);// 设置键值
			} catch (IllegalArgumentException e) {
				log.error("LogAspectError1", e);
			} catch (IllegalAccessException e) {
				log.error("LogAspectError2", e);
			}

		}
		return map;
	}


	public static String getIpAddr(HttpServletRequest request) {
		String ipAddress = request.getHeader("x-forwarded-for");
		if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("Proxy-Client-IP");
		}
		if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getRemoteAddr();
			String localIp = "127.0.0.1";
			String localIpv6 = "0:0:0:0:0:0:0:1";
			if (ipAddress.equals(localIp) || ipAddress.equals(localIpv6)) {
				// 根据网卡取本机配置的IP
				InetAddress inet = null;
				try {
					inet = InetAddress.getLocalHost();
					ipAddress = inet.getHostAddress();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
			}
		}
		// 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
		String ipSeparate = ",";
		int ipLength = 15;
		if (ipAddress != null && ipAddress.length() > ipLength) {
			if (ipAddress.indexOf(ipSeparate) > 0) {
				ipAddress = ipAddress.substring(0, ipAddress.indexOf(ipSeparate));
			}
		}
		return ipAddress;
	}
}
