package com.kw.framework.common.log.entity;

import lombok.Data;
import java.time.LocalDateTime;

@Data
public class SysLogDTO {

	/**
	 * 编号
	 */
	private Long id;

	/**
	 * 日志类型
	 */
	private String type;

	/**
	 * 日志标题
	 */
	private String title;

	/**
	 * 创建者
	 */
	private Long createBy;

	/**
	 * 更新时间
	 */
	private LocalDateTime updateTime;

	/**
	 * 操作IP地址
	 */
	private String remoteAddr;

	/**
	 * 用户代理
	 */
	private String userAgent;

	/**
	 * 请求URI
	 */
	private String requestUri;

	/**
	 * 操作方式
	 */
	private String method;

	/**
	 * 操作提交的数据
	 */
	private String params;

	/**
	 * 执行时间
	 */
	private Long time;

	/**
	 * 异常信息
	 */
	private String exception;

	/**
	 * 服务ID
	 */
	private String serviceId;

	/**
	 * 创建时间区间 [开始时间，结束时间]
	 */
	private LocalDateTime[] createTime;

	/**
	 * 租户编号
	 */
	private Integer tenantId;

}
