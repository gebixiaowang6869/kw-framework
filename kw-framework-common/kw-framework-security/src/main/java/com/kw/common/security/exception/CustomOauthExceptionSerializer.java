package com.kw.common.security.exception;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.kw.framework.common.croe.enums.ResultEnum;

import java.io.IOException;

/**
 * @author wangkeng
 * @create 2021-07-16 16:27
 */
public class CustomOauthExceptionSerializer extends StdSerializer<CustomOauthException> {

    public CustomOauthExceptionSerializer() {
        super(CustomOauthException.class);
    }

    @Override
    public void serialize(CustomOauthException value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        //value内容适当的做一些错误类型判断
        gen.writeStartObject();
        gen.writeObjectField("status", ResultEnum.CLIENT_AUTHENTICATION_FAILED);
        gen.writeObjectField("msg",ResultEnum.CLIENT_AUTHENTICATION_FAILED.getDesc());
        gen.writeStringField("data", value.getoAuth2ErrorCode());
        gen.writeEndObject();
    }
}