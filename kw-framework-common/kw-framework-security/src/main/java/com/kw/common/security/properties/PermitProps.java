package com.kw.common.security.properties;


import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lza
 * @date 2019/5/13
 * <p>
 * 资源服务器对外直接暴露URL,如果设置contex-path 要特殊处理
 */
/*@Slf4j
@Configuration
@ConditionalOnExpression("!'${security.oauth2.client.ignore-urls}'.isEmpty()")
@ConfigurationProperties(prefix = "security.oauth2.client")
@RefreshScope*/
public class PermitProps {

	@Getter
	@Setter
	private List<String> ignoreUrls = new ArrayList<>();

	public static void main(String[] args) {
		String a = "1234";
		System.out.println(a.substring(0,a.length()-1));
	}

}
