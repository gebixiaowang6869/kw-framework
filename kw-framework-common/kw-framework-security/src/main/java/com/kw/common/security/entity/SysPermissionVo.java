package com.kw.common.security.entity;

import lombok.Data;

import java.util.List;

/**
 * @author wangkeng
 * @create 2021-07-19 10:26
 * 菜单列表
 */
@Data
public class SysPermissionVo {
    private Long id ;

    private String name;
    /**
     * 菜单等级
     */
    private Integer level;
    /**
     * 图标
     */
    private String icon;
    /**
     * 描述
     */
    private String descr;
    /**
     * 接口地址
     */
    private String link;
    /**
     * 父类菜单
     */
    private Long parentId;
    /**
     * 租户id
     */
    private Integer tenantId;

    private boolean checkBox ;

    /**
     * 是否可用（0否  1是）
     */
    private Integer status;

    private List<SysPermissionVo> permissionList ;
}
