package com.kw.common.security.utils;


import cn.hutool.json.JSONUtil;
import com.kw.framework.common.croe.entity.GlobalAuthEntity;
import com.kw.framework.common.croe.exception.BusinessException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;


/**
 * @Author kw
 * @Description 根据SecurityContext 获取信息
 */
public class SecurityUtil{

	 /**
     * 获取SecurityUser
     * @return
     */
    public static GlobalAuthEntity getKwSecurityUser() {
        if(!hasAuthenticated()){
            throw new BusinessException("用户未登录");
        }
        try{
            GlobalAuthEntity user  = JSONUtil.toBean(SecurityContextHolder.getContext().getAuthentication().getName(),GlobalAuthEntity.class);
            if(user==null){
                throw new BusinessException("未登录！");
            }
            return user ;
        }catch (Exception e){
            throw new BusinessException("权限校验失败");
        }
    }

    public static String getUserName(){
        try{
            String name = JSONUtil.toBean(SecurityContextHolder.getContext().getAuthentication().getName(),GlobalAuthEntity.class).getName();
            return name ;
        }catch (Exception e){
            return null ;
        }
    }



    public static String getUserPhone(){return getKwSecurityUser().getPhone();}

    public static String getOpenId(){
        return getKwSecurityUser().getOpenId();
    }

    public static Long getUserId(){
        try{
            Long userId = JSONUtil.toBean(SecurityContextHolder.getContext().getAuthentication().getName(),GlobalAuthEntity.class).getId();
            return userId;
        }catch (Exception e){
            return null ;
        }
    }

    public static Integer getTenantId(){
        try{
            Integer tenentId = JSONUtil.toBean(SecurityContextHolder.getContext().getAuthentication().getName(),GlobalAuthEntity.class).getTenantId();
            return tenentId;
        }catch (Exception e){
            return null ;
        }
    }
    /**
     * 判断该用户是否登录
     * @return
     */
    public static boolean hasAuthenticated() {
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        return authentication.isAuthenticated();
    }

}