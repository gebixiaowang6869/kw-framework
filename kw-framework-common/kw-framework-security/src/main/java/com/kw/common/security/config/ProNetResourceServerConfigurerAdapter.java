package com.kw.common.security.config;

import com.kw.common.security.translator.CustomExceptionTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint;
import org.springframework.security.oauth2.provider.token.*;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * 当本地没有连接redis时去到授权服务器进行check-token
 */
@Configuration
@RefreshScope
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
@ConditionalOnProperty(prefix = "security.oauth2",name = "remote-check-token",havingValue = "true")
public class ProNetResourceServerConfigurerAdapter extends ResourceServerConfigurerAdapter {


    @Resource
    private PermitAllUrlResolver permitAllUrlResolver;

    @Value("${security.oauth2.resourceId}")
    private String resourceId;

    @Value("${security.oauth2.checkTokenUrl:http://192.168.2.64:8088/sso/oauth/check_token}")
    private String checkTokenUrl ;

    @Value("${security.oauth2.clientId:user}")
    private String clientId ;

    @Value("${security.oauth2.clientSecurity:123456}")
    private String clientSecurity ;

    @Resource
    RestTemplate restTemplate ;

    /**
     * 配置校验token方式
     * @param resources
     * @throws Exception
     */
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        OAuth2AuthenticationEntryPoint authenticationEntryPoint = new OAuth2AuthenticationEntryPoint();
        authenticationEntryPoint.setExceptionTranslator(new CustomExceptionTranslator());
        resources.authenticationEntryPoint(authenticationEntryPoint);
        resources.resourceId(resourceId);
        resources.tokenServices(tokenServices());
    }
    @Bean
    public AccessTokenConverter accessTokenConverter() {
        return new DefaultAccessTokenConverter();
    }

    @Bean
    public ResourceServerTokenServices tokenServices() {
        KwRemoteTokenServices remoteTokenServices = new KwRemoteTokenServices();
        remoteTokenServices.setCheckTokenEndpointUrl(checkTokenUrl);
//这里硬编码客户端信息，服务端硬编码保存在内存里，生产上请修改
        remoteTokenServices.setClientId(clientId);
        remoteTokenServices.setClientSecret(clientSecurity);
        remoteTokenServices.setRestTemplate(restTemplate);
     //  remoteTokenServices.setAccessTokenConverter(accessTokenConverter());
        return remoteTokenServices;
    }


/*    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        OAuth2AuthenticationEntryPoint authenticationEntryPoint = new OAuth2AuthenticationEntryPoint();
        authenticationEntryPoint.setExceptionTranslator(new CustomExceptionTranslator());
        resources.authenticationEntryPoint(authenticationEntryPoint);
        resources.tokenExtractor(tokenExtractor)
                .tokenServices(resourceServerTokenServices);
    }*/

    /**
     * 对匹配的资源进行放行
     * @param http
     * @throws Exception
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {

        // 允许使用iframe 嵌套，避免swagger-ui 不被加载的问题
        http.headers().frameOptions().disable();
        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry = http
                .authorizeRequests();
        // 配置对外暴露接口
        permitAllUrlResolver.registry(registry);
        registry.anyRequest().authenticated().and().csrf().disable();
    }
}
