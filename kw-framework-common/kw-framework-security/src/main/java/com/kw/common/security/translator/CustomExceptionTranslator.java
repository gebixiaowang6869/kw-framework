package com.kw.common.security.translator;

import com.kw.common.security.exception.CustomOauthException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.DefaultThrowableAnalyzer;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;
import org.springframework.security.web.util.ThrowableAnalyzer;
import org.springframework.web.HttpRequestMethodNotSupportedException;

/**
 * @author wangkeng
 * @create 2021-07-16 15:34
 */
public class CustomExceptionTranslator extends DefaultWebResponseExceptionTranslator {

    private ThrowableAnalyzer throwableAnalyzer = new DefaultThrowableAnalyzer();


    @Override
    public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
        ResponseEntity<OAuth2Exception> translate = super.translate(e);
        OAuth2Exception body = translate.getBody();



        Throwable[] causeChain = throwableAnalyzer.determineCauseChain(e);

        Exception ase = (AuthenticationException) throwableAnalyzer.getFirstThrowableOfType(AuthenticationException.class,
                causeChain);
        String msg = null;
        if (ase != null) {
           msg = "AuthenticationException";
           return response(body,translate,msg);
        }

        ase = (AccessDeniedException) throwableAnalyzer
                .getFirstThrowableOfType(AccessDeniedException.class, causeChain);
        if (ase != null) {
            msg = "AccessDeniedException";
            return response(body,translate,msg);
        }

        ase = (InvalidGrantException) throwableAnalyzer
                .getFirstThrowableOfType(InvalidGrantException.class, causeChain);
        if (ase != null) {
            msg = "InvalidGrantException";
            return response(body,translate,msg);
        }

        ase = (InsufficientAuthenticationException) throwableAnalyzer
                .getFirstThrowableOfType(InsufficientAuthenticationException.class, causeChain);
        if (ase != null) {
            msg = "身份验证错误，请检查是否有传递token或者token是否合法";
            return response(body,translate,msg);
        }



       /* ase = (HttpRequestMethodNotSupportedException) throwableAnalyzer.getFirstThrowableOfType(HttpRequestMethodNotSupportedException.class, causeChain);
        if (ase != null) {
            msg = "HttpRequestMethodNotSupportedException";
            return response(body,translate,msg);
        }*/

        ase = (OAuth2Exception) throwableAnalyzer.getFirstThrowableOfType(
                OAuth2Exception.class, causeChain);

        if (ase != null) {
            msg = "OAuth2Exception";
            return response(body,translate,msg);
        }

        if(msg==null){
            msg = "server_error";
        }
        return response(body,translate,msg);
    }

    private ResponseEntity<OAuth2Exception> response( OAuth2Exception body,   ResponseEntity<OAuth2Exception> translate , String msg){
        CustomOauthException customOauthException = new CustomOauthException(msg,body.getMessage(),
                body.getHttpErrorCode());
        ResponseEntity<OAuth2Exception> response = new ResponseEntity<>(customOauthException, translate.getHeaders(),
                translate.getStatusCode());
        return response;
    }



}