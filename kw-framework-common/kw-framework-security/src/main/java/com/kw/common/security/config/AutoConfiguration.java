package com.kw.common.security.config;

import com.kw.common.security.service.PermissionService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

@ComponentScan({"com.kw.common.security"})
public class AutoConfiguration {


    @Bean(name="pms")
    @ConditionalOnMissingBean
    public PermissionService getPermissionService(){
        return new PermissionService();
    }

    @Bean
    @ConditionalOnMissingBean
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }

    @Bean
    @ConditionalOnMissingBean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
