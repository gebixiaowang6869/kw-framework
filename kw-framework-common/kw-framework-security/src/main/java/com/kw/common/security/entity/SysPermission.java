package com.kw.common.security.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author kw
 * @since 2021-07-14
 */
@Data
public class SysPermission implements Serializable{

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 资源名称
     */
    private String name;
    /**
     * 菜单等级
     */
    private Integer level;
    /**
     * 图标
     */
    private String icon;
    /**
     * 描述
     */
    private String descr;
    /**
     * 接口地址
     */
    private String link;
    /**
     * 父类菜单
     */
    private Long parentId;
    /**
     * 租户id
     */
    private Integer tenantId;
    /**
     * 是否可用（0否  1是）
     */
    private Integer status;
    /**
     * 是否删除（0否 1是）
     */
    private Integer deleteFlag;
    /**
     * 排序
     */
    private Integer sort;


}
