package com.kw.framework.common.redis.expression;

import org.aspectj.lang.JoinPoint;

public interface KeyResolver {

	/**
	 * 解析处理 key
	 * @param key key
	 * @param point 接口切点信息
	 * @return 处理结果
	 */
	String resolver(String key, JoinPoint point);

}
