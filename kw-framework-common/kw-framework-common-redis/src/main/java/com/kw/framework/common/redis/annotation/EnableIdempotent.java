package com.kw.framework.common.redis.annotation;


import com.kw.framework.common.redis.IdempotentAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({IdempotentAutoConfiguration.class})
public @interface EnableIdempotent {
}
