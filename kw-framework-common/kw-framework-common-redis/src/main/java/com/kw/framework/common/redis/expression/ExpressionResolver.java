package com.kw.framework.common.redis.expression;

import cn.hutool.json.JSONUtil;
import com.google.common.collect.Lists;
import com.kw.framework.common.redis.annotation.Idempotent;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author lengleng
 * <p>
 * 默认key 抽取， 优先根据 spel 处理
 * @date 2020-09-25
 */
public class ExpressionResolver implements KeyResolver {

	private static final SpelExpressionParser PARSER = new SpelExpressionParser();

	private static final LocalVariableTableParameterNameDiscoverer DISCOVERER = new LocalVariableTableParameterNameDiscoverer();

	@Override
	public String resolver(String key, JoinPoint point) {
		Object[] arguments = point.getArgs();
		String[] params = DISCOVERER.getParameterNames(getMethod(point));
		StandardEvaluationContext context = new StandardEvaluationContext();

		for (int len = 0; len < params.length; len++) {

			context.setVariable(params[len], arguments[len]);

		}


		Expression expression = PARSER.parseExpression(key);
		return expression.getValue(context, String.class);
	}

	/**
	 * 根据切点解析方法信息
	 * @param joinPoint 切点信息
	 * @return Method 原信息
	 */
	private Method getMethod(JoinPoint joinPoint) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		if (method.getDeclaringClass().isInterface()) {
			try {
				method = joinPoint.getTarget().getClass().getDeclaredMethod(joinPoint.getSignature().getName(),
						method.getParameterTypes());
			}
			catch (SecurityException | NoSuchMethodException e) {
				throw new RuntimeException(e);
			}
		}
		return method;
	}


	class user{
		public String name ;
		public List<String> names ;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public List<String> getNames() {
			return names;
		}

		public void setNames(List<String> names) {
			this.names = names;
		}
	}

	public void a(){
		EvaluationContext context = new StandardEvaluationContext();
		List<user> users = new ArrayList<>();
		for(int i = 0 ; i<10;i++){
			user u = new user();
			u.setName("name"+i);
			u.setNames(Lists.newArrayList("name"+i,"name2"+i));
			users.add(u);
		}
		for(user u:users){
			//1、设置变量
			context.setVariable("user", u);
			ExpressionParser parser = new SpelExpressionParser();
			//2、表达式中以#varName的形式使用变量
			Expression expression = parser.parseExpression("#user");
			//3、在获取表达式对应的值时传入包含对应变量定义的EvaluationContext
			List<Object> userName = expression.getValue(context, List.class);

			userName.forEach(e->{
				context.setVariable("userName",e);
				Expression expression1 = parser.parseExpression("#userName.name + '-' + #userName.name");
				System.out.println(expression1.getValue(context, String.class));
			});
			//表达式中使用变量，并在获取值时传递包含对应变量定义的EvaluationContext。
			System.out.println(JSONUtil.toJsonStr(userName));
		}

	}
	public static void main(String[] args) {
		ExpressionResolver e = new ExpressionResolver();
		e.a();


	}
}