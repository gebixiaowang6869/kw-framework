package com.kw.framework.common.redis.aspect;

import com.kw.framework.common.croe.constant.RedisKey;
import com.kw.framework.common.croe.exception.BusinessException;
import com.kw.framework.common.croe.exception.IdempotentException;
import com.kw.framework.common.redis.annotation.Idempotent;
import com.kw.framework.common.redis.annotation.RedisLock;
import com.kw.framework.common.redis.expression.KeyResolver;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RMapCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * redis分布式锁拦截器
 *
 * @author kw
 */
@Aspect
public class RedisLockAspect {


	@Resource
	private Redisson redisson;

	@Resource
	private KeyResolver keyResolver;

	@Pointcut("@annotation(com.kw.framework.common.redis.annotation.RedisLock)")
	public void pointCut() {
	}

	@Around("pointCut()")
	public Object around(ProceedingJoinPoint joinPoint) throws Throwable {


		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		if (!method.isAnnotationPresent(RedisLock.class)) {
			return null;
		}
		RedisLock redisLock = method.getAnnotation(RedisLock.class);

		String key;

		// 若没有配置加锁key，则使用 className+methodName作为区分
		if (StringUtils.isEmpty(redisLock.key())) {
			String methodName = signature.getName();
			String className = signature.getClass().getName();
			key = className+"."+methodName;
		}
		else {
			key = keyResolver.resolver(redisLock.key(), joinPoint);
		}
		if(key==null){
			return joinPoint.proceed();
		}
		RLock lock = redisson.getLock(RedisKey.REDIS_LOCK+redisLock.prefix()+":"+key);
		try {
			lock.tryLock(redisLock.expireTime(),redisLock.timeUnit());
		}catch (Exception e){
			throw new BusinessException(redisLock.info());
		}

		try{
			Object result = joinPoint.proceed();
			return result;
		}finally {
			lock.unlock();
		}
	}



}
