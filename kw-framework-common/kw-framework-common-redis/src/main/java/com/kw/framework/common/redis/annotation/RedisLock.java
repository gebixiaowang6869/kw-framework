package com.kw.framework.common.redis.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * @author ITyunqing
 */
@Inherited
@Target(ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface RedisLock {


	/**
	 * 加锁key，使用spring el表达式 用#来引用方法参数
	 * @return Spring-EL expression
	 */
	String key();

	/**
	 * 有效期 默认：1 有效期要大于程序执行时间，否则请求还是可能会进来
	 * @return expireTime
	 */
	int expireTime() default 5;

	/**
	 * 时间单位 默认：s
	 * @return TimeUnit
	 */
	TimeUnit timeUnit() default TimeUnit.SECONDS;

	/**
	 * 提示信息，可自定义
	 * @return String
	 */
	String info() default "操作失败，加锁失败";


	String prefix() default "default";


}
