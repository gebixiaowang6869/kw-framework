package com.kw.framework.common.redis.utils;

import com.kw.framework.common.croe.exception.BusinessException;
import com.kw.framework.common.croe.utils.AssertUtil;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * @ClassName: RedissonUtil
 * @Description:
 * @Author: chenxiang
 * @Date: 2021/05/25 10:41
 * @Version: 1.0
 */
@Slf4j
@Getter
@RequiredArgsConstructor
public class RedissonUtil {

    private final RedissonClient redissonClient;

    /**
     * 上锁
     * (推荐) 开启看门狗
     * @param key
     * @param func
     * @return
     */
    public Boolean lock(String key,Supplier<Boolean> func) {
        RLock lock = redissonClient.getLock(key);
        try {
            //waitTime 等待时间设置为0,获取不到直接返回
            boolean result = lock.tryLock(0L, TimeUnit.SECONDS);
            AssertUtil.assertTrue(result,"获取锁失败");
            return func.get();
        } catch (Exception e){
            log.error("未知错误",e);
            throw new RuntimeException(e);
        } finally {
            this.unLock(lock);
        }
    }

    /**
     * 获取锁,成功会返回一个锁的实例
     * (推荐) 开启看门狗
     * 默认锁的过期时间是30秒,如果redisson的实例没有关闭,就会不断是延长锁的时间
     * 一定要在finally{ RedissonUtil.unLock(lock) }释放锁
     * @param key
     * @return
     */
    public RLock lock(String key){
        RLock lock = redissonClient.getLock(key);
        try {
            //waitTime 等待时间设置为0,获取不到直接返回
            boolean result = lock.tryLock(0L, TimeUnit.SECONDS);
            return result ? lock : null;
        } catch (InterruptedException e) {
            log.error("获取锁失败",e);
            return null;
        }
    }

    /**
     * 批量获取锁,成功会返回锁的集合,失败为空
     * (推荐) 开启看门狗
     * 默认锁的过期时间是30秒,如果redisson的实例没有关闭,就会不断是延长锁的时间
     * 一定要在finally{ RedissonUtil.BatchUnLock(List<lock>) }释放锁
     * @param keys
     * @return
     */
    public List<RLock> batchLock(List<String> keys){
        List<RLock> locks = new ArrayList<>();
        for (String key : keys) {
            try {
                RLock lock = this.lock(key);
                AssertUtil.notNull(lock,"获取锁失败");
                locks.add(lock);
            } catch (Exception e) {
                try {
                    log.error("获取锁失败",e);
                    return null;
                }finally {
                    this.batchUnLock(locks);
                }
            }
        }
        return locks;
    }

    /**
     * 获取锁,成功会返回一个锁的实例
     * 设置了过期时间看门狗就会失效
     * @param key 过期时间
     * @param time
     * @return
     */
    public RLock lock(String key,Long time){
        RLock lock = redissonClient.getLock(key);
        try {
            AssertUtil.assertTrue(time > 0L,"锁必须设置过期时间");
            //waitTime 等待时间设置为0,获取不到直接返回
            boolean result = lock.tryLock(0L, time, TimeUnit.SECONDS);
            return result ? lock : null;
        } catch (InterruptedException e) {
            log.error("获取锁失败",e);
            return null;
        }
    }

    /**
     * 获取锁,成功会返回一个锁的实例
     * 设置了过期时间看门狗就会失效
     * @param key 过期时间
     * @param waitTime 等待时间
     * @param leaseTime 锁过期时间
     * @return
     */
    public RLock lock(String key, long waitTime, long leaseTime){
        RLock lock = redissonClient.getLock(key);
        try {
            AssertUtil.assertTrue(leaseTime > 0L,"锁必须设置过期时间");
            //waitTime 等待时间设置为0,获取不到直接返回
            boolean result = lock.tryLock(waitTime, leaseTime, TimeUnit.SECONDS);
            return result ? lock : null;
        } catch (InterruptedException e) {
            log.error("获取锁失败",e);
            return null;
        }
    }

    public Boolean lock(String key, long waitTime, long leaseTime,Supplier<Boolean> func){

        RLock lock = redissonClient.getLock(key);
        try {
            AssertUtil.assertTrue(leaseTime > 0L,"锁必须设置过期时间");
            boolean result = lock.tryLock(waitTime, leaseTime, TimeUnit.SECONDS);
            AssertUtil.assertTrue(result,"获取锁失败");
            return func.get();
        } catch (BusinessException be){
            throw be;
        } catch (Exception e) {
            log.error("执行失败",e);
            throw new BusinessException("未知错误");
        }finally {
            this.unLock(lock);
        }
    }

    public Boolean lock(String key, Long time, Supplier<Boolean> func) {
        RLock lock = redissonClient.getLock(key);
        try {
            AssertUtil.assertTrue(time > 0L,"锁必须设置过期时间");
            boolean result = lock.tryLock(0L, time, TimeUnit.SECONDS);
            AssertUtil.assertTrue(result,"获取锁失败");
            return func.get();
        } catch (BusinessException be){
            throw be;
        } catch (Exception e) {
            log.error("未知错误",e);
            throw new RuntimeException(e);
        }finally {
            this.unLock(lock);
        }
    }

    /**
     * 批量获取锁,成功会返回锁的集合,失败为空
     * 设置了过期时间看门狗就会失效
     * @param keys 过期时间
     * @param time
     * @return
     */
    public List<RLock> batchLock(List<String> keys,Long time){
        List<RLock> locks = new ArrayList<>();
        for (String key : keys) {
            AssertUtil.assertTrue(time > 0L,"锁必须设置过期时间");
            RLock lock = redissonClient.getLock(key);
            try {
                AssertUtil.assertTrue(this.lock(lock,time),"获取锁失败");
                locks.add(lock);
            } catch (Exception e) {
                try {
                    log.error("获取锁失败",e);
                    return null;
                }finally {
                    this.batchUnLock(locks);
                }
            }
        }
        return locks;
    }

    /**
     * 批量释放锁
     * @param locks 锁的集合
     */
    public void batchUnLock(List<RLock> locks){
        locks.forEach(lock -> {
            this.unLock(lock);
        });
    }

    public boolean lock (RLock lock,Long time) throws InterruptedException {
        return lock.tryLock(0L, time, TimeUnit.SECONDS);
    }

    /**
     * 释放锁
     * @param lock
     */
    public void unLock(RLock lock){
        try {
            if(null != lock && lock.isLocked() && lock.isHeldByCurrentThread()){
                lock.unlock();
            }
        }catch (Exception e){
            log.error("释放锁失败:"+ lock.getName());
        }
    }
}
