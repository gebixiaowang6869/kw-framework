package com.kw.framework.common.redis.annotation;

import com.kw.framework.common.redis.config.RedisTemplateConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({RedisTemplateConfiguration.class})
public @interface EnableKwRedis {
}
