package com.kw.framework.bootstrap.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.TimeZone;

/**
 * jackson 配置
 *
 * @author caixinjiang
 */
@Configuration(proxyBeanMethods = false)
public class JacksonAutoConfiguration {

    /**
     * 自定义 jackson 转换器. 默认字符集设置为 UTF-8
     * http 的 content-type 响应头, 从 application/json 改为 application/json;charset=UTF-8
     * 解决旧版本 chrome 调试网络的乱码问题
     */
    @Bean
    @ConditionalOnMissingBean(value = MappingJackson2HttpMessageConverter.class,
            ignoredType = {
                    "org.springframework.hateoas.server.mvc.TypeConstrainedMappingJackson2HttpMessageConverter",
                    "org.springframework.data.rest.webmvc.alps.AlpsJsonHttpMessageConverter"})
    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter(ObjectMapper objectMapper) {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter(objectMapper);
        converter.setDefaultCharset(StandardCharsets.UTF_8);
        return converter;
    }

    /**
     * 定制 jackson
     * 1. 统一处理 long 转为 string, 前端 js 只能处理到 15 位的 long 精度
     * 2. 设置时区为 GMT+8
     * 3. double/BigDecimal 不用科学计数法序列化
     */
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer customJackson() {
        return builder -> {
            builder.serializerByType(Long.TYPE, ToStringSerializer.instance);
            builder.serializerByType(Long.class, ToStringSerializer.instance);

            TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
            builder.timeZone("GMT+8");

            builder.serializerByType(Double.TYPE, new DoubleSerializer());
            builder.serializerByType(Double.class, new DoubleSerializer());
            builder.featuresToEnable(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN);
        };
    }


    static class DoubleSerializer extends JsonSerializer<Double> {
        @Override
        public void serialize(Double value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            BigDecimal d = BigDecimal.valueOf(value);
            gen.writeNumber(d.stripTrailingZeros().toPlainString());
        }

        @Override
        public Class<Double> handledType() {
            return Double.class;
        }
    }

}
