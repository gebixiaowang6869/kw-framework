package com.kw.framework.bootstrap.annotation;

import com.kw.common.security.annotation.EnableKwSecurtity;
import com.kw.framework.common.croe.exception.GlobalExceptionHandler;
import com.kw.framework.common.swagger.annotation.EnableKwSwagger2;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.kw")
@Import(GlobalExceptionHandler.class)
@EnableKwSecurtity
@EnableCaching
@EnableKwSwagger2
public @interface BootstrapAnnotation {
}
