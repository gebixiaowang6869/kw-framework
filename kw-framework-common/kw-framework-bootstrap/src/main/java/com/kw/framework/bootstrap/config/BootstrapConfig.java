package com.kw.framework.bootstrap.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.kw.framework.bootstrap.aspect.LogAspect;
import com.kw.framework.bootstrap.filter.TenantFilter;
import com.kw.framework.bootstrap.filter.TraceIdFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.List;

@Slf4j
@Configuration
@Import(JacksonAutoConfiguration.class)
public class BootstrapConfig {
    @Bean
    public TraceIdFilter traceIdFilter() {
        TraceIdFilter filter = new TraceIdFilter();
        log.info("TraceIdFilter [{}]", filter);
        return filter;
    }


    @Bean
    public LogAspect logAspect() {
        LogAspect aspect = new LogAspect();
        log.info("LogAspect [{}]", aspect);
        return aspect;
    }
    @Bean
    public TenantFilter tenantFilter() {
        TenantFilter tenantFilter = new TenantFilter();
        log.info("tenantFilter [{}]", tenantFilter);
        return tenantFilter;
    }


    @LoadBalanced
    @Bean
    @ConditionalOnMissingBean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> converters = restTemplate.getMessageConverters();
        for (HttpMessageConverter<?> converter : converters) {
            if (converter instanceof MappingJackson2HttpMessageConverter) {
                MappingJackson2HttpMessageConverter jsonConverter = (MappingJackson2HttpMessageConverter) converter;
                jsonConverter.setObjectMapper(new ObjectMapper());
                jsonConverter.setSupportedMediaTypes(ImmutableList.of(new MediaType("application", "json", Charset.forName("UTF-8")), new MediaType("text", "javascript",  Charset.forName("UTF-8"))));
            }
        }
        return restTemplate;
    }


}
