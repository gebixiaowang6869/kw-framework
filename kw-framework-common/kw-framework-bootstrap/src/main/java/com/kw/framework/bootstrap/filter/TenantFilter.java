package com.kw.framework.bootstrap.filter;

import cn.hutool.core.util.StrUtil;
import com.kw.common.security.utils.SecurityUtil;
import com.kw.framework.common.croe.context.UserContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Slf4j
@WebFilter(filterName = "tenantFilter", urlPatterns = "/*")
@Order(1)
public class TenantFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        Integer tenantId = SecurityUtil.getTenantId();
        if(tenantId==null){
            HttpServletRequest request = (HttpServletRequest)servletRequest;
            String tenantIdStr = request.getHeader("tenantId");
            if(StrUtil.isNotEmpty(tenantIdStr)){
                tenantId = Integer.parseInt(tenantIdStr);
            }
        }
        UserContext.setTenant(tenantId);
        UserContext.setTenantName(SecurityUtil.getUserName());
        UserContext.setUserId(SecurityUtil.getUserId());

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        UserContext.clean();
    }


}