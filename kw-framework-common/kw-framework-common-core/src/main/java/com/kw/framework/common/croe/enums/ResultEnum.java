package com.kw.framework.common.croe.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * 返回code枚举
 * @author wang
 *
 */
@Getter
@AllArgsConstructor
public enum ResultEnum implements BaseEnum<Integer>{
    // 成功
    SUCCESS(200, "成功"),
    // token异常  3开头
    TOKEN_PAST(301, "token过期"),
    TOKEN_ERROR(302, "token异常"),
    // 登录异常
    LOGIN_ERROR(303, "登录异常"),
    REMOTE_ERROR(304, "异地登录"),

    LOGIN_NAME(305, "用户名错误"),
    LOGIN_NAME_NULL(306, "用户名为空"),
    LOGIN_PASSWORD(307, "密码错误"),
    LOGIN_CODE(308, "验证码错误"),
    LOGOUT_CODE(309, "退出失败，token 为空"),

    REGISTRY_FAIL(310, "注册失败"),
    MOBILE_NOT_AUTH(311, "微信手机未授权"),
    MOBILE_AUTH_FAIL(312, "微信手机授权失败"),

    // crud异常，4开头
    BAD_REQUEST(400, "请求参数有误"),
    CRUD_SAVE_FAIL(403, "添加失败"),
    CRUD_UPDATE_FAIL(404, "更新失败"),
    CRUD_DELETE_FAIL(405, "删除失败"),
    CRUD_DELETE_NOT(406, "不允许删除"),
    CRUD_VALID_NOT(407, "字段校验异常"),
    CRUD_NOT_OPERATE(408, "无操作权限"),
    CRUD_LOCK_OPERATE(409, "没有获取到锁"),

    // 默认错误
    ERROR(500, "错误"),
    SYSTEM_ERROR(-599, "网络繁忙，请稍候重试"),


    // 1000-2000 为sso异常 ///////////////////////////////
    CLIENT_AUTHENTICATION_FAILED(1001,"客户端认证失败"),
    USERNAME_OR_PASSWORD_ERROR(1002,"用户名或密码错误"),
    UNSUPPORTED_GRANT_TYPE(1003, "不支持的认证模式"),
    PHONE_CODE_ERROR(1004, "验证码错误");


    private Integer code;

    private String desc;

    @Override
    public String getMessage() {
        return desc;
    }



}
