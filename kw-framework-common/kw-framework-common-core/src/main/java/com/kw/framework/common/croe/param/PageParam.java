package com.kw.framework.common.croe.param;

import lombok.Data;

import java.io.Serializable;

@Data
public class PageParam implements Serializable {
	private Integer pageNo ;
	private Integer pageSize ;
	private String orderBy ;
	private String orderByType ;
}
