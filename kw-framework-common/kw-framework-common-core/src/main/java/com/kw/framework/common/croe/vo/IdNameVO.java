package com.kw.framework.common.croe.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class IdNameVO implements Serializable {
    private Long id ;

    private String name ;
}
