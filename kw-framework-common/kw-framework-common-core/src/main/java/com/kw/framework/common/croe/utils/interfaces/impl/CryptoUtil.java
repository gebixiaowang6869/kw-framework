package com.kw.framework.common.croe.utils.interfaces.impl;

public interface CryptoUtil {

    public  String encrypt(String data) throws Exception ;

    public String decrypt(String encryptedData) throws Exception ;
}
