package com.kw.framework.common.croe.context;

public class UserContext {

    private static ThreadLocal<String> USER_NAME = new ThreadLocal();

    private static ThreadLocal<Long> USER_ID = new ThreadLocal();

    private static ThreadLocal<Integer> TENANT = new ThreadLocal();


    public UserContext() {
    }

    public static void setTenantName(String name) {
        USER_NAME.set(name);
    }

    public static void setUserId(Long id) {
        USER_ID.set(id);
    }

    public static String getTenantName() {
        return USER_NAME.get();
    }

    public static Long getUserId() {
        return USER_ID.get();
    }

    public static void setTenant(Integer tenantId) {
        TENANT.set(tenantId);
    }

    public static Integer getTenant() {
        return TENANT.get();
    }


    public static void clean() {
        USER_NAME.remove();
        USER_ID.remove();
        TENANT.remove();
    }

}
