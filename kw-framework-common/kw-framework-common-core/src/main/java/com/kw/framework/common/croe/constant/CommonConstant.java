package com.kw.framework.common.croe.constant;

public class CommonConstant {

	public final static Integer DELETED_FLAG = 1 ;
	public final static Integer UN_DELETE_FLAG = 0 ;

	public final static Integer STATUS_ON_FLAG = 1 ;
	public final static Integer STATUS_OFF_FLAG = 0 ;

	/**
	 * 日志跟踪标识
	 */
	public static final String TRACE_ID = "TRACE_ID";

	public static final String JWT_KEY = "kwdl";

	/**
	 * 编码
	 */
	String UTF8 = "UTF-8";
}
