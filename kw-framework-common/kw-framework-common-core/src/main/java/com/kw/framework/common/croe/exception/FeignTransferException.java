package com.kw.framework.common.croe.exception;

import com.kw.framework.common.croe.enums.ResultEnum;


public class FeignTransferException  extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private String msg;




    public FeignTransferException(String msg) {
        super(msg);
        this.msg = msg;
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
