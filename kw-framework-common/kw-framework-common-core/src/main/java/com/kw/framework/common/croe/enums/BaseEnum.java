package com.kw.framework.common.croe.enums;

public interface BaseEnum<T>{
        T getCode();
        String getMessage();
}
