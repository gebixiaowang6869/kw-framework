package com.kw.framework.common.croe.utils;

import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Set;

public class AssertUtil {
    public static void maxLength(String str, int maxLength, String message) {
        if (StringUtils.isEmpty(str)) {
            throw new IllegalArgumentException("参数不允许为空");
        }
        if (str.length() > maxLength) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void assertFieldName(String fieldName, String message) {
        if (StringUtils.isEmpty(fieldName)) {
            throw new IllegalArgumentException(message);
        }
        boolean isFieldName = fieldName.matches("^[a-zA-Z]+$");
        if (!isFieldName) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void notNull(Object obj, String message) {
        if (obj == null) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void assertNotNull(Object obj, String message) {
        if (obj == null) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void assertNotEmpty(String str, String message) {
        if (StringUtils.isEmpty(str)) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void empty(String str, String message) {
        if (StringUtils.isEmpty(str)) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void isEmpty(List<?> list, String message) {
        if (!list.isEmpty()) {
            return;
        }
        throw new IllegalArgumentException(message);
    }

    public static void assertNotEmpty(List<?> list, String message) {
        if (!list.isEmpty()) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void assertNotEmpty(Set<?> set, String message) {
        if ((set == null) || (set.isEmpty())) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void assertTrue(boolean flag, String message) {
        if (!flag) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void greatZero(Integer num, String message) {
        if ((num == null) || (num.intValue() <= 0)) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void greatZero(Long num, String message) {
        if ((num == null) || (num.longValue() <= 0L)) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void isGreaterZero(Integer num, String message) {
        greatZero(num, message);
    }

    public static void isEquals(int num1, int num2, String message) {
        if (num1 == num2) {
            return;
        }
        throw new IllegalArgumentException(message);
    }

    public static void equalsIgnoreCase(String str1, String str2, String message) {
        if (str1.equalsIgnoreCase(str2)) {
            return;
        }
        throw new IllegalArgumentException(message);
    }

    public static void isGreaterEqualZero(Integer num, String message) {
        if (num.intValue() >= 0) {
            return;
        }
        throw new IllegalArgumentException(message);
    }
}

