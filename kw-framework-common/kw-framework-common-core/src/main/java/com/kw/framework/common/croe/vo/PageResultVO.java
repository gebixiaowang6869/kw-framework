package com.kw.framework.common.croe.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PageResultVO<T> implements Serializable {
	private Long pageTotal ;
	private Long total ;
	private Integer pageNo ;
	private Integer pageSize ;
	private List<T> records ;
}
