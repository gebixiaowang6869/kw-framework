package com.kw.framework.common.croe.constant;

/**
 * @author wangkeng
 * @create 2021-07-16 8:57
 */
public class RedisKey {

    public final static String ADMIN_USER_PERMISSION = "member:admin:user:tenentId:{}permission:{}";

    public final static String REDIS_LOCK = "lock:";
}
