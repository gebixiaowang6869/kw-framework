package com.kw.framework.common.croe.exception;

import cn.hutool.core.util.StrUtil;
import com.kw.framework.common.croe.vo.Result;
import com.kw.framework.common.croe.enums.ResultEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.UnexpectedTypeException;
import java.nio.file.AccessDeniedException;
import java.sql.SQLTransientConnectionException;
import java.util.Iterator;
import java.util.List;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @Value("${spring.profiles.active:dev}")
    private String active ;

	/**
     * 校验错误拦截处理
     *
     * @param exception 错误信息集合
     * @return 错误信息
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Result<String> validationBodyException(MethodArgumentNotValidException exception) {
        BindingResult result = exception.getBindingResult();
        String message = "";
        if (result.hasErrors()) {
            List<ObjectError> errors = result.getAllErrors();
            if (errors != null) {
                errors.forEach(p -> {
                    FieldError fieldError = (FieldError) p;
                    log.error("Data check failure : object{" + fieldError.getObjectName() + "},field{" + fieldError.getField() +
                            "},errorMessage{" + fieldError.getDefaultMessage() + "}");

                });
                if (errors.size() > 0) {
                    FieldError fieldError = (FieldError) errors.get(0);
                    message = fieldError.getDefaultMessage();
                }
            }
        }
        return Result.error("".equals(message) ? "请填写正确信息" : message);
    }


    /**
     * 业务错误拦截
     * @param exception
     * @return
     */
    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Result<String> accessDeniedException(AccessDeniedException exception){
        log.error("无权限访问：", exception);
        return Result.error("无权限访问,请联系管理员");
    }




    /**
     * 业务错误拦截
     * @param exception
     * @return
     */
    @ExceptionHandler(BusinessException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Result<String> businessException(BusinessException exception){
        if(exception.getEnumType()!=null){
            return Result.error(exception.getEnumType().getDesc());
        }
    	log.error("捕获业务异常：", exception);
        if(StrUtil.isNotEmpty(exception.getMessage())){
            return Result.error(exception.getMessage());
        }
    	return Result.error("业务异常");
    }

    /**
     * feign远程调用失败
     * @param exception
     * @return
     */
    @ExceptionHandler(FeignTransferException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Result<String> feignTransferException(FeignTransferException exception){
        return Result.error(exception.getMsg());
    }


    /**
     * IllegalArgumentException错误拦截
     * @param exception
     * @return
     */
    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Result<String> illegalArgumentException(IllegalArgumentException exception){
        log.error("捕获业务异常：", exception);
        return Result.error(exception.getMessage());
    }





    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Result<String> exception(Exception exception){
    	log.error("globalException：", exception);
    	if(active.equals("pro")){
    	    return Result.error("未知服务器错误");
        }
    	return Result.error(exception.getMessage());
    }
    
    
    /**
     * 拦截捕获 @RequestParam 参数校验异常
     *
     */
    @ExceptionHandler(value = {ConstraintViolationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result<String> validExceptionHandler(ConstraintViolationException ex) {
        Iterator<ConstraintViolation<?>> it = ex.getConstraintViolations().iterator();
        String message = "";
        if (it.hasNext()) {
            message = it.next().getMessageTemplate();
        }
        log.warn("拦截捕获 @RequestParam 参数校验异常{}", message);
        return Result.error(ResultEnum.BAD_REQUEST,"拦截捕获 @RequestParam 参数校验异常:"+message+"");
    }

    /**
     * 拦截捕获 @RequestBody required=true 绑定请求参数异常
     *
     */
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result<String> validExceptionHandler(HttpMessageNotReadableException ex) {
        log.error("", ex);
        return Result.error(ResultEnum.BAD_REQUEST,"没有请求体");
    }

    /**
     * 拦截捕获 @RequestBody required=true 绑定请求参数异常
     *
     */
    @ExceptionHandler(value = BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result<String> validExceptionHandler(BindException ex) {
        log.error("", ex);
        return Result.error(ResultEnum.BAD_REQUEST,"请求参数错误");
    }
    /**
     * 拦截捕获绑定请求参数异常
     *
     */
    @ExceptionHandler(value = UnexpectedTypeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result<String> validExceptionHandler(UnexpectedTypeException ex) {
    	log.error("", ex);
    	return Result.error(ResultEnum.BAD_REQUEST,"参数类型不对");
    }
    
/*    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
    public Result<String> httpRequestMethodNotSupportedExceptionHandle(HttpRequestMethodNotSupportedException ex){
    	log.error("", ex);
    	return Result.error(ResultEnum.BAD_REQUEST,"BAD_REQUEST");
    }*/
    
    @ExceptionHandler(value = SQLTransientConnectionException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Result<String> cannotGetJdbcConnectionExceptionHandle(SQLTransientConnectionException ex){
    	log.error("", ex);
     	return Result.error("数据库连接失败");
    }

}
