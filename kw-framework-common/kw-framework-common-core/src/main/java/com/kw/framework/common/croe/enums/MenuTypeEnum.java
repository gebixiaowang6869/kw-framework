package com.kw.framework.common.croe.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * 菜单类型
 */
@Getter
@AllArgsConstructor
public enum MenuTypeEnum implements BaseEnum<String>{

	/**
	 * 左侧菜单
	 */
	LEFT_MENU("0", "left"),

	/**
	 * 顶部菜单
	 */
	TOP_MENU("2", "top"),

	/**
	 * 按钮
	 */
	BUTTON("1", "button");

	/**
	 * 类型
	 */
	private final String code;

	/**
	 * 描述
	 */
	private final String message;

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public MenuTypeEnum convertCode(String code) {
		return Stream.of(values())
				.filter(bean -> bean.code.equals(code))
				.findAny()
				.orElse(null);
	}

	public MenuTypeEnum convertMessage(String message) {
		return Stream.of(values())
				.filter(bean -> bean.message.equals(message))
				.findAny()
				.orElse(null);
	}


}
