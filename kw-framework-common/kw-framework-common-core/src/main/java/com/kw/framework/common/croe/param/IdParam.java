package com.kw.framework.common.croe.param;

import lombok.Data;

import java.io.Serializable;

@Data
public class IdParam implements Serializable {

    private Long id ;
}
