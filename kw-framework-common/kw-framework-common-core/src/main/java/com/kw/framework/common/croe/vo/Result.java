package com.kw.framework.common.croe.vo;


import com.kw.framework.common.croe.enums.ResultEnum;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * 统一返回格式
 * @author wang
 * @param <T>
 */
@Slf4j
@Data
public final class Result<T> implements Serializable {

    private static final long serialVersionUID = -5816713866887895850L;
    /**
     * 错误码
     */
    private Integer status = ResultEnum.ERROR.getCode();

    /**
     * 错误信息
     */
    private String msg = null;

    /**
     * 返回结果实体
     */
    private T data = null;

    public Result() {
    }

    private Result(int code, String msg, T data) {
        this.status = code;
        this.msg = msg;
        this.data = data;
    }

    public static <T> Result<T> error(String msg) {
        log.debug("返回默认错误：, msg={}",  msg);
        return new Result<>(ResultEnum.ERROR.getCode(), msg, null);
    }

    public static <T> Result<T> error(String msg , int code){
        log.debug("返回默认错误：, msg={}",  msg);
        return new Result<>(code, msg, null);
    }

    public static Result<String> error(ResultEnum resultEnum) {
        log.debug("返回错误：code={}, msg={}", resultEnum.getCode(), resultEnum.getDesc());
        return new Result<>(resultEnum.getCode(), resultEnum.getDesc(), resultEnum.getDesc());
    }

    public static Result<String> error(ResultEnum resultEnum, String msg) {
        log.debug("返回错误：code={}, msg={}", resultEnum.getCode(), msg);
        return new Result<>(resultEnum.getCode(), msg, msg);
    }
    
    public static <T> Result<T> errorForData(ResultEnum resultEnum, T data) {
        log.debug("返回错误：code={}, msg={}, data={}", resultEnum.getCode(), resultEnum.getDesc(), data);
        return new Result<>(resultEnum.getCode(), resultEnum.getDesc(), data);
    }

    public static <T> Result<T> success(T data) {
        return new Result<>(ResultEnum.SUCCESS.getCode(), "", data);
    }

    public static  Result<String> success(){
        return new Result<>(ResultEnum.SUCCESS.getCode(), "", "SUCCESS");
    }
}
