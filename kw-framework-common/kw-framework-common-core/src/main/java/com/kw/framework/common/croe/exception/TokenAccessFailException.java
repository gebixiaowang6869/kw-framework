package com.kw.framework.common.croe.exception;

import com.kw.framework.common.croe.enums.ResultEnum;

import java.text.MessageFormat;

public class TokenAccessFailException  extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private Integer code;
    private ResultEnum enumType;

    public TokenAccessFailException(String msg) {
        super(msg);
        this.code = 500;
    }

    public TokenAccessFailException(Integer code , String msg){
        super(msg);
        this.code = code;
    }


    public TokenAccessFailException(ResultEnum baseEnumType) {
        super(baseEnumType.getDesc());
        this.code = baseEnumType.getCode();
        this.enumType = baseEnumType;
    }

    public TokenAccessFailException(String msg, Object... arguments) {
        super(MessageFormat.format(msg, arguments));
        this.code = 500;
    }

    public TokenAccessFailException(ResultEnum baseEnumType, Object... arguments) {
        super(MessageFormat.format(baseEnumType.getDesc(), arguments));
        this.code = baseEnumType.getCode();
        this.enumType = baseEnumType;
    }

    public ResultEnum getEnumType() {
        return enumType;
    }

    public void setEnumType(ResultEnum enumType) {
        this.enumType = enumType;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
