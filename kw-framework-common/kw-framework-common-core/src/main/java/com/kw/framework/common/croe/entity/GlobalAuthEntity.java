package com.kw.framework.common.croe.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class GlobalAuthEntity implements Serializable {
    private Long id ;

    private String name ;

    private String openId ;

    private String phone ;

    private String miniOpenId ;

    private Integer tenantId;
}
