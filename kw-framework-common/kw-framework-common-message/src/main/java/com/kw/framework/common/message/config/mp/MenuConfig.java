package com.kw.framework.common.message.config.mp;

import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.bean.menu.WxMenuButton;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.config.WxMpConfigStorage;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;

import static me.chanjar.weixin.common.api.WxConsts.MenuButtonType;

/**
 * Created by FirenzesEagle on 2016/6/1 0001.
 * Email:liumingbo2008@gmail.com
 */
public class MenuConfig {

    /**
     * 定义菜单结构
     *
     * @return
     */
    public static WxMenu getMenu(WxMpService wxMpService) {


        WxMenu menu = new WxMenu();
        WxMenuButton button1 = new WxMenuButton();
        button1.setName("美团红包");

        WxMenuButton button11 = new WxMenuButton();
        button11.setType(MenuButtonType.CLICK);
        button11.setName("美团外卖");
        button11.setKey("mt_1");

        WxMenuButton button12 = new WxMenuButton();
        button12.setType(MenuButtonType.CLICK);
        button12.setName("美团生鲜");
        button12.setKey("mt_2");

        button1.getSubButtons().add(button11);
        button1.getSubButtons().add(button12);


        WxMenuButton button2 = new WxMenuButton();
        button2.setName("饿了么");

        WxMenuButton button21 = new WxMenuButton();
        button21.setType(MenuButtonType.CLICK);
        button21.setName("外卖红包");
        button21.setKey("elm_2");

        WxMenuButton button22 = new WxMenuButton();
        button22.setType(MenuButtonType.CLICK);
        button22.setName("超市红包");
        button22.setKey("elm_3");


//        WxMenuButton button24 = new WxMenuButton();
//        button24.setType(MenuButtonType.VIEW);
//        button24.setName("商品管理");
//        button24.setUrl(wxMpService.getOAuth2Service().buildAuthorizationUrl("", "snsapi_base", ""));

        button2.getSubButtons().add(button21);
        button2.getSubButtons().add(button22);

        menu.getButtons().add(button1);
        menu.getButtons().add(button2);

        return menu;
    }


    public static class MainConfig {

        private String appId;
        private String appSecret;
        private String token;
        private String aesKey;

        /**
         * 为了生成自定义菜单使用的构造函数
         *
         * @param appId
         * @param appSecret
         * @param token
         * @param aesKey
         */
        protected MainConfig(String appId, String appSecret, String token, String aesKey) {
            this.appId = appId;
            this.appSecret = appSecret;
            this.token = token;
            this.aesKey = aesKey;
        }

        public WxMpConfigStorage wxMpConfigStorage() {
            WxMpDefaultConfigImpl configStorage = new WxMpDefaultConfigImpl();
            configStorage.setAppId(this.appId);
            configStorage.setSecret(this.appSecret);
            configStorage.setToken(this.token);
            configStorage.setAesKey(this.aesKey);
            return configStorage;
        }

        public WxMpService wxMpService() {
            WxMpService wxMpService = new WxMpServiceImpl();
            wxMpService.setWxMpConfigStorage(wxMpConfigStorage());
            return wxMpService;
        }

    }

//    /**
//     * 运行此main函数即可生成公众号自定义菜单，注意要修改MainConfig构造方法行代码的对应四个参数为实际值
//     *
//     * @param args
//     */
//    public static void main(String[] args) {
//        MainConfig mainConfig = new MainConfig("appid", "appsecret", "token", "aesKey");
//        WxMpService wxMpService = mainConfig.wxMpService();
//        try {
//            wxMpService.getMenuService().menuCreate(getMenu(null));
//        } catch (WxErrorException e) {
//            e.printStackTrace();
//        }
//    }

}
