package com.kw.framework.common.message.annotation;

import com.kw.framework.common.message.config.MessageConfiguration;
import com.kw.framework.common.message.config.ma.WxMaConfiguration;
import com.kw.framework.common.message.config.mp.WxMpConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({WxMaConfiguration.class, WxMpConfiguration.class})
public @interface EnableMessage {
}
