package com.kw.framework.common.message.constant;

public class MessageConst {
    /**
     * 消息推送执行器类名定义
     */
    //小程序推送消息
    public final static String MINI_APP_MESSAGE ="miniAppMessageSend";

    //公众号推送消息
    public final static String WX_PUBLIC_MESSAGE ="wxPublicMessageSend";
    /**
     * END消息推送执行器类名定义
     */


    /**
     * 小程序消息模板id定义
     */

    //开工提醒消息模板
    public final static String MINI_APP_TEMPLATE_ID_START_WORK_NOTICE = "hVGRa0LgZrS2gFc9qlwerLzk63h2lf8Df7HuF3Mrah4";
    public final static String MINI_APP_FORM_ID_START_WORK_NOTICE = "17406";

    public final static String MINI_APP_TEMPLATE_ID_UNREAD_MESSAGE = "2UlIRNQkqgkl06NsaetZc4cdPiJz0Lff0vVRy91I_tQ";
    public final static String MINI_APP_FORM_ID_UNREAD_MESSAGE = "1185";


    public final static String MINI_APP_TEMPLATE_ID_PROCESS_PASS ="GqWxDm5nst6jR287h2KOa00PGCTXH89QYsxncFOJfRA";
    public final static String MINI_APP_FORM_ID_PROCESS_PASS = "32035";

    /**
     * END小程序消息模板id定义
     */


    /**
     * 公众号消息模板id定义
     */
    public final static String WX_PUBLIC_TEMPLATE_ID_PROCESS_PASS ="D2VUcoBx-dChDqlR1iDQoCTVN5iaVskNOoDF4vsKNPk";

    public final static String WX_PUBLIC_TEMPLATE_ID_IM_PUSH = "vjkZ9CKb6R_YLNkdiz9L6Mn2PXxdS08qbhheeYaxHs0";

    /**
     * end公众号消息模板id定义
     */
}
