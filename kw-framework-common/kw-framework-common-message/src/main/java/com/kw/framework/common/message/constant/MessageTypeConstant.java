package com.kw.framework.common.message.constant;


public class MessageTypeConstant {

    //模板消息
    public final static Integer TEMPLATE_MESSAGE = 0;

    //订阅消息
    public final static Integer SUBSCRIB_MESSAGE = 1 ;
}
