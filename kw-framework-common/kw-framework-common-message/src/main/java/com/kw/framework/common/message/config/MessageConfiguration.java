package com.kw.framework.common.message.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.kw.framework.common.message.config")
public class MessageConfiguration {
}
