package com.kw.framework.common.excel;

import com.kw.framework.common.croe.exception.BusinessException;
import com.kw.framework.common.excel.listener.AbstractAnalysisEventListener;

public abstract class ExcelListener extends AbstractAnalysisEventListener<DataRow> {

    public ExcelListener(Boolean checkParamStatus) {
        super(checkParamStatus);
    }
}