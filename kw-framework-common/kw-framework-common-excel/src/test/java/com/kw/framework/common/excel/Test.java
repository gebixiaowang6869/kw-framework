package com.kw.framework.common.excel;

import com.alibaba.excel.EasyExcel;
import com.kw.framework.common.excel.imports.ExcelBaseSyncImport;
import com.kw.framework.common.excel.listener.AbstractAnalysisEventListener;
import com.kw.framework.common.excel.vo.ExcelImportErrorMsgVo;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public class Test {

    @org.junit.Test
    public void testEasyExcel(){
        ExcelBaseSyncImport<DataRow> excelBaseSyncImport = new ExcelBaseSyncImport<>();
        String fileName = "a.xlsx";
        List<DataRow> data = excelBaseSyncImport.getListByFile(fileName,DataRow.class,new AbstractAnalysisEventListener<DataRow>(false){
            @Override
            protected String checkParam(DataRow data) {
                return null;
            }
        });
        System.out.println(data.size());
    }

}
