package com.kw.framework.common.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.kw.framework.common.croe.enums.MenuTypeEnum;
import com.kw.framework.common.excel.convert.EnumToStringConvert;
import com.kw.framework.common.excel.vo.ExcelImportErrorMsgVo;
import lombok.Data;

@Data
public class DataRow extends ExcelImportErrorMsgVo {
    @ExcelProperty(index = 0)
    private String name;
    @ExcelProperty(index = 1,converter = EnumToStringConvert.class)
    private MenuTypeEnum menuTypeEnum ;
}
