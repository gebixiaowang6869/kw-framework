package com.kw.framework.common.excel.convert;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import com.kw.framework.common.croe.enums.BaseEnum;

import java.lang.reflect.Field;
import java.util.stream.Stream;

public class EnumToStringConvert implements Converter<Object> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return Object.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }


    /**
     * 导入转换
     * @param cellData            当前单元格对象
     * @param contentProperty     当前单元格属性
     * @param globalConfiguration
     * @return
     * @throws Exception
     */
    @Override
    public Object convertToJavaData(ReadCellData<?> cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        String cellMsg = cellData.getStringValue();
        Field field = contentProperty.getField();
        Class<? extends BaseEnum> c = (Class<? extends BaseEnum>) field.getType();
        return Stream.of(c.getEnumConstants())
                    .filter(bean -> bean.getMessage().equals(cellMsg))
                    .findAny()
                    .orElse(null);
    }


    /**
     * 导出转化
     * @param value               当前值
     * @param contentProperty     当前单元格属性
     * @param globalConfiguration
     * @return
     * @throws Exception
     */
    @Override
    public WriteCellData<?> convertToExcelData(Object value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        BaseEnum baseEnum = (BaseEnum) value;
        return new WriteCellData(baseEnum.getMessage());
    }
}
