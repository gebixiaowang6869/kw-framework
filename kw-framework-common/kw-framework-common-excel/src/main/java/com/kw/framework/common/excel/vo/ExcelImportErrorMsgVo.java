package com.kw.framework.common.excel.vo;

import lombok.Data;

@Data
public class ExcelImportErrorMsgVo {

    private String errorMsg ;
}
