package com.kw.framework.common.excel.convert;

import com.kw.framework.common.croe.enums.BaseEnum;
import com.kw.framework.common.croe.enums.MenuTypeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
 * 对excel导入时，处理枚举转化
*/


@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnumFiledConvert {

    Class<? extends BaseEnum> name();

}
