package com.kw.framework.common.excel.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.kw.framework.common.excel.listener.AbstractAnalysisEventListener;
import com.kw.framework.common.excel.vo.ExcelImportErrorMsgVo;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

public interface BaseExcelController<T extends ExcelImportErrorMsgVo> {
    default List<T> getListByFile(String fileName , Class clazz  , AbstractAnalysisEventListener<T> listener){
        EasyExcel.read(fileName, clazz, listener)
                .sheet()
                .doRead();
        return listener.getData();
    }

    default List<T> getListByInputStream(Class clazz , InputStream file , AbstractAnalysisEventListener<T> listener){
        EasyExcel.read(file, clazz, listener)
                .sheet()
                .doRead();
        return listener.getData();
    }

    default void exportUserExcel(HttpServletResponse response,String fileName,String sheetName,List<?> data,Class clazz) throws IOException {
        OutputStream outputStream = response.getOutputStream();
        try {
            this.setExcelResponseProp(response, fileName);
            // 模拟根据条件在数据库查询数据
            //这个实现方式非常简单直接，使用EasyExcel的write方法将查询到的数据进行处理，以流的形式写出即可
            EasyExcel.write(outputStream, clazz)
                    .excelType(ExcelTypeEnum.XLSX)
                    .sheet(sheetName)
                    .doWrite(data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            outputStream.flush();
            outputStream.close();
        }
    }

    /**
     * 设置响应结果
     *
     * @param response    响应结果对象
     * @param rawFileName 文件名
     * @throws UnsupportedEncodingException 不支持编码异常
     */
    default void setExcelResponseProp(HttpServletResponse response, String rawFileName) throws UnsupportedEncodingException {
        //设置内容类型
        response.setContentType("application/vnd.vnd.ms-excel");
        //设置编码格式
        response.setCharacterEncoding("utf-8");
        //设置导出文件名称（避免乱码）
        String fileName = URLEncoder.encode(rawFileName.concat(".xlsx"), "UTF-8");
        // 设置响应头
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName);
    }


}
