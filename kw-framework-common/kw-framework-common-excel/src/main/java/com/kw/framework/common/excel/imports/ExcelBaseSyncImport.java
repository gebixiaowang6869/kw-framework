package com.kw.framework.common.excel.imports;

import com.alibaba.excel.EasyExcel;
import com.kw.framework.common.excel.listener.AbstractAnalysisEventListener;
import com.kw.framework.common.excel.vo.ExcelImportErrorMsgVo;

import java.io.InputStream;
import java.util.List;

/**
 * 同步导入处理
 */
public class ExcelBaseSyncImport<T extends ExcelImportErrorMsgVo> {

    public List<T> getListByFile(String fileName ,Class clazz  ,AbstractAnalysisEventListener<T> listener){
        EasyExcel.read(fileName, clazz, listener)
                .sheet()
                .doRead();
        return listener.getData();
    }

    public List<T> getListByInputStream(Class clazz ,InputStream file , AbstractAnalysisEventListener<T> listener){
        EasyExcel.read(file, clazz, listener)
                .sheet()
                .doRead();
       return listener.getData();
    }
}
