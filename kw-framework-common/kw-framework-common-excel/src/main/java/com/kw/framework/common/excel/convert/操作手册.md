在导出时需要将继承了BaseEnum的枚举类导出成messgae时需要在枚举类字段上加上注解：
@ExcelProperty(value = "表头字段名",index = 1 ,converter = EnumToStringConvert.class)
demo:
    @TableField(typeHandler = DefaultEnumTypeHandler.class)
    @ExcelProperty(value = "类型",index = 2 ,converter = EnumToStringConvert.class)
    private MenuTypeEnum type;

在导入时需要将导入字段转成继承了BaseEnum的枚举类时，需要加上以下注解：
@ExcelProperty(index = 1,converter = EnumToStringConvert.class)
private MenuTypeEnum menuTypeEnum ;
其中导入的excel中枚举类对应的列填入的内容为BaseEnum中的message