package com.kw.framework.common.excel.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.kw.framework.common.excel.vo.ExcelImportErrorMsgVo;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractAnalysisEventListener<T extends ExcelImportErrorMsgVo> extends AnalysisEventListener<T> {

    /**
     * 是否校验入参
     */
    Boolean checkParamStatus ;
    List<T> data = new ArrayList<>();

    public AbstractAnalysisEventListener(Boolean checkParamStatus){
        this.checkParamStatus = checkParamStatus;
    }
    public List<T> getData(){
        return data;
    }
    @Override
    public void invoke(T data, AnalysisContext context) {
        if(checkParamStatus){
            this.data.add(buildMsg(data,checkParam(data)));
        }else{
            this.data.add(data);
        }
    }

    protected abstract String checkParam(T data);

    private T buildMsg(T data,String msg){
        data.setErrorMsg(msg);
        return data;
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 所有数据解析完成后做的事情
    }

}
