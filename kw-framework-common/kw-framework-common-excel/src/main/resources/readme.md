 ·常用注解
@ExcelProperty：用于标识Excel中的字段，可以指定字段在Excel中的列索引或列名。例如：@ExcelProperty("姓名")，@ExcelProperty(index = 0)。

@ExcelIgnore：用于标识不需要导入或导出的字段。

@ExcelIgnoreUnannotated：用于标识未被 @ExcelProperty 注解标识的字段是否被忽略。

@ExcelHead：用于标识 Excel 表头的样式。可以设置表头的高度、字体样式、背景颜色等。

@ExcelColumnWidth：用于设置 Excel 列的宽度。

@ExcelDateTimeFormat：用于设置日期时间字段的格式化规则。

@ExcelBooleanFormat：用于设置布尔类型字段在 Excel 中的显示文本。

@ExcelNumberFormat：用于设置数字字段的格式化规则。