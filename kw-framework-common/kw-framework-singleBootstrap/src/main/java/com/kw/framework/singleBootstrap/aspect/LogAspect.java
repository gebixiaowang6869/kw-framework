package com.kw.framework.singleBootstrap.aspect;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 * 入参，出参，响应时间记录
 * @author admin
 *
 */
@Aspect
@Slf4j
public class LogAspect {

	/**
	 * 指定切面
	 */
	@Pointcut("execution (* com.kw.*.*.controller..*.*(..))")
/*	@Pointcut("@annotation(org.springframework.web.bind.annotation.GetMapping) ||" +
			"@annotation(org.springframework.web.bind.annotation.PostMapping) ||"+
			"@annotation(org.springframework.web.bind.annotation.PutMapping) ||"+
			"@annotation(org.springframework.web.bind.annotation.DeleteMapping)")*/
	public void controllerPointCut() {
		// 仅仅是为了使用PointCut
	}

	
	/**
	 * 实现around拦截
	 *
	 * @return
	 * @throws Throwable
	 */
	@Around("controllerPointCut()")
	public Object around(ProceedingJoinPoint point) throws Throwable {

		// 开始时间
		long start = System.currentTimeMillis();

		RequestAttributes ra = RequestContextHolder.getRequestAttributes();
		ServletRequestAttributes sra = (ServletRequestAttributes) ra;
		HttpServletRequest request = sra.getRequest();

		String url = request.getRequestURL().toString();
		String method = request.getMethod();
        String uri = request.getRequestURI();
		String queryString = request.getQueryString();
		Object[] args = point.getArgs();
		String params = "";
		// 获取请求参数集合并进行遍历拼接
		if (args.length > 0) {
			if ("POST".equals(method) || "PUT".equals(method) || "DELETE".equals(method)) {
				Object object = args[0];
				// request/response无法使用toJSON
				if (!(object instanceof HttpServletRequest) && !(object instanceof HttpServletResponse)) {
					Map<String, Object> map = getKeyAndValue(object);
					params = JSONUtil.toJsonStr(map);
				}
			} else if ("GET".equals(method)) {
				params = queryString;
			}
		}
		log.info("\n【请求地址】：{}\n【请求类型】：{}\n【请求参数】：{}", url,method,params);

		// 执行方法
		// result的值就是被拦截方法的返回值
		Object result = point.proceed();
		long end = System.currentTimeMillis();
		MethodSignature signature = (MethodSignature) point.getSignature();
		// 请求的方法名
		String className = point.getTarget().getClass().getName();
		String methodName = signature.getName();
		log.info("\n【接口执行时间】接口：{}\n【执行时间】：{}毫秒\n【返回值】：{}\n【ip】:{}", uri, (end - start),JSONUtil.toJsonStr(result),getIpAddr(request));
		
		return result;

	}

	private static Map<String, Object> getKeyAndValue(Object obj) {
		Map<String, Object> map = new HashMap<String, Object>();
		// 得到类对象
		Class userCla = (Class) obj.getClass();
		/* 得到类中的所有属性集合 */
		Field[] fs = userCla.getDeclaredFields();
		for (int i = 0; i < fs.length; i++) {
			Field f = fs[i];
			f.setAccessible(true); // 设置些属性是可以访问的
			Object val = new Object();
			try {
				val = f.get(obj);
				// 得到此属性的值
				map.put(f.getName(), val);// 设置键值
			} catch (IllegalArgumentException e) {
				log.error("LogAspectError1", e);
			} catch (IllegalAccessException e) {
				log.error("LogAspectError2", e);
			}

		}
		return map;
	}
	
	
	public static String getIpAddr(HttpServletRequest request) {
        String ipAddress = request.getHeader("x-forwarded-for");
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();
            String localIp = "127.0.0.1";
            String localIpv6 = "0:0:0:0:0:0:0:1";
            if (ipAddress.equals(localIp) || ipAddress.equals(localIpv6)) {
                // 根据网卡取本机配置的IP
                InetAddress inet = null;
                try {
                    inet = InetAddress.getLocalHost();
                    ipAddress = inet.getHostAddress();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
        }
        // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        String ipSeparate = ",";
        int ipLength = 15;
        if (ipAddress != null && ipAddress.length() > ipLength) {
            if (ipAddress.indexOf(ipSeparate) > 0) {
                ipAddress = ipAddress.substring(0, ipAddress.indexOf(ipSeparate));
            }
        }
        return ipAddress;
    }
}
