package com.kw.framework.singleBootstrap;

import com.kw.framework.common.croe.exception.GlobalExceptionHandler;
import com.kw.framework.common.swagger.annotation.EnableKwSwagger2;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@SpringBootApplication
@Import(GlobalExceptionHandler.class)
@EnableCaching
@EnableKwSwagger2
public @interface BootstrapAnnotation {
}
