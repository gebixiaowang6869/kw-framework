package com.kw.framework.mybatis.constants;

public class ExpressionConstants {
    public final static String EQ = "eq";
    public final static String IN = "in";

    public final static String NE = "ne";
}
