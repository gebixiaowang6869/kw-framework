package com.kw.framework.mybatis.annotation;

import com.kw.framework.mybatis.config.DataConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 引入mybatis基础配置
 * @author kw
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({DataConfiguration.class})
public @interface EnableMybaticPlus {

}
