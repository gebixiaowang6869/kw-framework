package com.kw.framework.mybatis.entity;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 实体的基础
 * @author kw
 */
@Data
@Accessors(chain = true)
public class BaseEntity<T> extends Model implements Serializable {


	/**
	 * 实体编号（唯一标识）
	 */
	@TableField
	@TableId(value = "id", type = IdType.AUTO)
	protected Long id;

	//@ApiModelProperty(value = "创建者")
	@TableField(fill = FieldFill.INSERT)
	protected Long createBy;

	//@ApiModelProperty(value = "创建时间")
	@TableField(fill = FieldFill.INSERT)
	protected LocalDateTime createTime;

	//@ApiModelProperty(value = "更新者")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	protected Long updateBy;

	//@ApiModelProperty(value = "更新时间")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	protected LocalDateTime updateTime;

	//@ApiModelProperty(value = "备注信息")
	//protected String remarks;

	//@ApiModelProperty(value = "删除标记")
	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	protected Integer delFlag;


}
