package com.kw.framework.mybatis.utils;


import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;

/**
 *  snowflake 算法获取id
 * @author kw
 */
public class IdUtils extends IdUtil {


    /**
     * 分布式系统中 获取id
     * @return
     */
    public static long getNextId() {
        int workerId = RandomUtil.randomInt(32);
        int datacenterId = RandomUtil.randomInt(32);
        Snowflake snowflake = cn.hutool.core.util.IdUtil.createSnowflake(workerId, datacenterId);
        return snowflake.nextId();
    }
}
