package com.kw.framework.mybatis.base;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kw.framework.mybatis.entity.BaseEntity;
;import java.util.List;


public interface IBaseService<T extends BaseEntity> extends IService<T> {


    void deleteByIds(String ids);

    void deleteByIdList(List<Long> idList);


}
