package com.kw.framework.mybatis.interfaces;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.kw.framework.common.croe.enums.BaseEnum;
import com.kw.framework.common.croe.enums.EnumCodeType;
import com.kw.framework.common.croe.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 枚举，字符串互转
 * @param <E>
 */

@MappedTypes(value = { BaseEnum.class })
@MappedJdbcTypes(value = {JdbcType.VARCHAR,JdbcType.CHAR,JdbcType.INTEGER})
@Slf4j
public class DefaultEnumTypeHandler<E extends Enum<E> & BaseEnum<?>> extends BaseTypeHandler<E> {

    private Class<E> type;
    private E[] baseEnum;
    private EnumCodeType enumCodeType;

    public DefaultEnumTypeHandler(Class<E> type) {
        if (type == null) {
            throw new IllegalArgumentException("Type argument cannot be null");
        }
        this.type = type;
        this.baseEnum = type.getEnumConstants();

        Class<?> clazz = (Class<?>) ((ParameterizedType) getInterfaceBaseEnum(type)).getActualTypeArguments()[0];

        this.enumCodeType = clazz.getName().equals("java.lang.String") ? EnumCodeType.String : EnumCodeType.Integer;

        if (this.baseEnum == null) {
            throw new IllegalArgumentException(type.getSimpleName() + " does not represent an enum type.");
        }
    }

    private Type getInterfaceBaseEnum(Class<E> type) {
        Type[] types = type.getGenericInterfaces();
        for (Type t : types) {
            if (t.getTypeName().contains(BaseEnum.class.getName())) {
                return t;
            }
        }
        throw new IllegalArgumentException(type.getSimpleName() + " does not represent an enum type.");
    }

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, E parameter, JdbcType jdbcType) throws SQLException {
        if (enumCodeType == EnumCodeType.String) {
            ps.setString(i, (String) parameter.getCode());
        } else {
            if (jdbcType == JdbcType.VARCHAR) {
                ps.setString(i, String.valueOf(parameter.getCode()));
            } else if (jdbcType != null) {
                ps.setObject(i, parameter.getCode(), jdbcType.TYPE_CODE);
            } else {
                Class<?> codeType = parameter.getCode().getClass();
                if (codeType.equals(String.class)) {
                    ps.setString(i, String.valueOf(parameter.getCode()));
                } else if (codeType.equals(Integer.class)) {
                    ps.setInt(i, (Integer) parameter.getCode());
                } else {
                    throw new BusinessException("系统错误");
                }

            }
        }
    }

    @Override
    public E getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return getEnum(rs, enumCodeType.getObject(rs, columnName));
    }

    @Override
    public E getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        // 根据数据库存储类型决定获取类型，本例子中数据库中存放String类型
        return getEnum(rs, enumCodeType.getObject(rs, columnIndex));
    }

    private E getEnum(ResultSet rs, Object object) throws SQLException {
        if (enumCodeType == EnumCodeType.String && StringUtils.isBlank(String.valueOf(object))) {
            return null;
        }
        if (object == null || rs.wasNull())
            return null;
        return selectEnum(object);
    }

    @Override
    public E getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        // 根据数据库存储类型决定获取类型，本例子中数据库中存放String类型
        Object object = enumCodeType.getObject(cs, columnIndex);
        if (object == null || cs.wasNull())
            return null;
        return selectEnum(object);
    }

    /**
     * 枚举类型转换，由于构造函数获取了枚举的子类enums，让遍历更加高效快捷
     *
     * @param value 数据库中存储的自定义value属性
     * @return value对应的枚举类
     */
    public E selectEnum(Object value) {
        for (E e : baseEnum) {
            if (e.getCode().equals(value)) {
                return e;
            }
        }
        log.error("未知的枚举类型：{},请核对: {}", value, type.getSimpleName());
        throw new BusinessException("not found enum");
    }
}

