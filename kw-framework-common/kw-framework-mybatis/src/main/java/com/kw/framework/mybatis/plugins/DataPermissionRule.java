package com.kw.framework.mybatis.plugins;



import com.kw.framework.mybatis.entity.DataPermissionFieldEntity;
import com.kw.framework.mybatis.entity.DataPermissionJoinEntity;

import java.util.List;

/**
 * @author wk
 */
public interface DataPermissionRule {

    /**
     *
     * @param type 这里的type是根据DataPermission中传递过来，可以根据这个type来适配要返回拦截的字段
     * @return
     */
    List<DataPermissionFieldEntity> getFilterRules(Integer type) ;

    List<DataPermissionJoinEntity> getJoinChains(Integer type);

}
