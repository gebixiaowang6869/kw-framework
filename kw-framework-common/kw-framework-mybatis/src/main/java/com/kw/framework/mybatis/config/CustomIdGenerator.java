package com.kw.framework.mybatis.config;

import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import com.kw.framework.mybatis.utils.IdUtils;



/**
 * 自定义id生成器
 * @author kw
 */
public class CustomIdGenerator implements IdentifierGenerator {


    @Override
    public Number nextId(Object entity) {
        return IdUtils.getNextId();
    }
}
