package com.kw.framework.mybatis.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 扫描common-data项目下的包
 * @author wang
 *
 */
@Configuration
@ComponentScan("com.kw.framework.mybatis")
public class DataConfiguration {
	

}
