package com.kw.framework.mybatis.handler;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.kw.framework.mybatis.annotation.DataPermission;
import com.kw.framework.mybatis.constants.ExpressionConstants;
import com.kw.framework.mybatis.constants.JoinTypeConstans;
import com.kw.framework.mybatis.entity.DataPermissionFieldEntity;
import com.kw.framework.mybatis.entity.DataPermissionJoinEntity;
import com.kw.framework.mybatis.plugins.DataPermissionRule;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.HexValue;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.*;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.*;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;


/**
 * @author wk
 */
@Slf4j
public class MyDataPermissionHandler {

    @SneakyThrows
    public PlainSelect rebuildPlainSelect(PlainSelect plainSelect, String whereSegment) {
        //获取mapper名称
        String className = whereSegment.substring(0, whereSegment.lastIndexOf("."));
        //获取方法名
        String methodName = whereSegment.substring(whereSegment.lastIndexOf(".") + 1);
        //获取当前mapper 的方法
        Method[] methods = Class.forName(className).getMethods();
        for (Method m : methods) {
            if (Objects.equals(m.getName(), methodName)) {
                DataPermission annotation = m.getAnnotation(DataPermission.class);

                if (annotation == null) {
                    return plainSelect;
                } else {
                    DataPermissionRule dataPermissionRule = SpringUtil.getBean(DataPermissionRule.class);
                    String[] ignoreTables = annotation.ignoreTables();
                    List<String> ignoreTableList = null;
                    if (ignoreTables != null && ignoreTables.length > 0) {
                        ignoreTableList = Arrays.asList(ignoreTables);
                    }
                    String[] joinTables = annotation.ignoreJoinTables();
                    List<String> joinTablesList = null;
                    if (joinTables != null && joinTables.length > 0) {
                        joinTablesList = Arrays.asList(joinTables);
                    }
                    try {
                        //处理需要join的语句
                        List<DataPermissionJoinEntity> joinChainEntity = dataPermissionRule.getJoinChains(annotation.type());
                        if(joinChainEntity!=null){
                            buildJoin(plainSelect,joinChainEntity,joinTablesList);
                        }

                        //处理需要加到where、on上的语句
                        List<DataPermissionFieldEntity> filterRules = dataPermissionRule.getFilterRules(annotation.type());
                        if (CollectionUtils.isNotEmpty(filterRules)) {
                            // 待执行 SQL Where 条件表达式
                            rebuildPlainSelect(plainSelect, ignoreTableList, filterRules);
                        }
                    } catch (NoSuchBeanDefinitionException e) {
                        log.info("--------no dataPermissionFilter set!----------");
                    }
                }
            }
        }
        return plainSelect;
    }
    ///////////////////////////////////////////////////以下是构建表where条件查询///////////////////////////////////////////////////////////

    private void setExpression(PlainSelect plainSelect, List<String> ignoreTableList, List<DataPermissionFieldEntity> dataPermissionSetEntity) {
        Expression where = plainSelect.getWhere();
        if (where == null) {
            where = new HexValue(" 1 = 1 ");
        }
        plainSelect.setWhere(buildExpression(where,(Table) plainSelect.getFromItem(),dataPermissionSetEntity,ignoreTableList));
    }

    private void setOnExpression(Join e, List<String> ignoreTableList, List<DataPermissionFieldEntity> dataPermissionSetEntity) {
        Expression onExpression = e.getOnExpression();
        e.setOnExpression(buildExpression(onExpression,(Table) e.getRightItem(),dataPermissionSetEntity,ignoreTableList));
    }

    public Expression buildWhere(Expression where ,DataPermissionFieldEntity k,String mainTableName){
        if (k.getExpressionType().equals(ExpressionConstants.EQ)) {
            EqualsTo selfEqualsTo = new EqualsTo();
            selfEqualsTo.setLeftExpression(new Column(mainTableName + "." + k.getFieldName()));
            selfEqualsTo.setRightExpression(new LongValue(k.getIds().get(0)));
            where = new AndExpression(where, selfEqualsTo);
        }else if (k.getExpressionType().equals(ExpressionConstants.NE)) {
            NotEqualsTo selfEqualsTo = new NotEqualsTo();
            selfEqualsTo.setLeftExpression(new Column(mainTableName + "." + k.getFieldName()));
            selfEqualsTo.setRightExpression(new LongValue(k.getIds().get(0)));
            where = new AndExpression(where, selfEqualsTo);
        } else if (k.getExpressionType().equals(ExpressionConstants.IN)) {
            ItemsList itemsList = new ExpressionList(k.getIds().stream().map(LongValue::new).collect(Collectors.toList()));
            InExpression inExpression = new InExpression(new Column(mainTableName + "." + k.getFieldName()), itemsList);
            where = new AndExpression(where, inExpression);
        }
        return where ;
    }


    private Expression buildExpression(Expression where, Table fromItem, List<DataPermissionFieldEntity> dataPermissionSetEntity, List<String> ignoreTableList){
        // 有别名用别名，无别名用表名，防止字段冲突报错
        String mainTableName = fromItem.getAlias() == null ? fromItem.getName() : fromItem.getAlias().getName();
        for (DataPermissionFieldEntity k : dataPermissionSetEntity) {
            if(CollectionUtil.isNotEmpty(k.getUseTables()) && k.getUseTables().contains(fromItem.getName())){
                where = buildWhere(where,k,mainTableName);
            }else if (CollectionUtil.isEmpty(k.getUseTables())&&(CollectionUtil.isEmpty(ignoreTableList)||!ignoreTableList.contains(fromItem.getName()))) {
                where = buildWhere(where,k,mainTableName);
            }
        }
        return where ;
    }

    private void rebuildPlainSelect(PlainSelect plainSelect, List<String> ignoreTableList, List<DataPermissionFieldEntity> dataPermissionSetEntity) {
        //重构where条件
        setExpression(plainSelect, ignoreTableList, dataPermissionSetEntity);
        //重构join相关的sql
        List<Join> joins = plainSelect.getJoins();
        if (CollectionUtil.isNotEmpty(joins)) {
            reBuildJoins(joins, ignoreTableList, dataPermissionSetEntity);
        }
        //重构sql中查询的列（其中可能包含子查询）
        List<SelectItem> selectItems = plainSelect.getSelectItems();
        buildSelectItems(selectItems, ignoreTableList,  dataPermissionSetEntity);
    }

    private void reBuildJoins(List<Join> joins, List<String> ignoreTableList, List<DataPermissionFieldEntity> dataPermissionSetEntity) {
        for (Join e : joins) {
            if (e.getRightItem() instanceof SubSelect) {
                SubSelect select = (SubSelect) e.getRightItem();
                buildSelectBody(select,ignoreTableList,dataPermissionSetEntity);
            } else if (e.getRightItem() instanceof Table) {
                setOnExpression(e, ignoreTableList, dataPermissionSetEntity);
            }
        }
    }

    private void buildSelectItems(List<SelectItem> selectItems, List<String> ignoreTableList, List<DataPermissionFieldEntity> dataPermissionSetEntity){
        selectItems.forEach(e->{
            if(e instanceof SelectExpressionItem){
                SelectExpressionItem item = (SelectExpressionItem) e;
                Expression expression = item.getExpression();
                if(expression instanceof SubSelect){
                    SubSelect select = (SubSelect)expression;
                    buildSelectBody(select,ignoreTableList,dataPermissionSetEntity);
                }
            }
        });
    }

    private void buildSelectBody( SubSelect select, List<String> ignoreTableList, List<DataPermissionFieldEntity> dataPermissionSetEntity){
        SelectBody selectBody = select.getSelectBody();
        if (selectBody instanceof PlainSelect) {
            this.rebuildPlainSelect((PlainSelect) selectBody, ignoreTableList, dataPermissionSetEntity);
        } else if (selectBody instanceof SetOperationList) {
            SetOperationList setOperationList = (SetOperationList) selectBody;
            List<SelectBody> selectBodyList = setOperationList.getSelects();
            selectBodyList.forEach(s -> this.rebuildPlainSelect((PlainSelect) s, ignoreTableList, dataPermissionSetEntity));
        }
    }

    ///////////////////////////////////////////////////以下是构建表关联规则///////////////////////////////////////////////////////////

    private void buildJoin(PlainSelect plainSelect, List<DataPermissionJoinEntity> joinChainEntity, List<String> ignoreTableList) {
        Table fromItem = (Table) plainSelect.getFromItem();
        Alias fromItemAlias = fromItem.getAlias();
        String mainTableName = fromItemAlias == null ? fromItem.getName() : fromItemAlias.getName();
        List<Join> joins = plainSelect.getJoins();
        joins = joins == null? new ArrayList<>():joins ;

        if(CollectionUtil.isNotEmpty(ignoreTableList)&&!ignoreTableList.contains(fromItem.getName())){

            Map<String,String> tableToAlias = new HashMap<>(2);

            tableToAlias.put(fromItem.getName(),mainTableName);
            joinChainEntity.forEach(e->{
                buildTableToAliasMap(tableToAlias,e);
            });
            if(CollectionUtil.isEmpty(joins)){
                joins.forEach(e->{
                    if (e.getRightItem() instanceof Table) {
                        Table select = (Table) e.getRightItem();
                        Alias alias = fromItem.getAlias();
                        String mainName = alias == null ? select.getName() : alias.getName();
                        tableToAlias.put(select.getName(),mainName);
                    }
                });
            }
            List<Join> finalJoins = joins;
            joinChainEntity.forEach(e->{
                if(StrUtil.isEmpty(e.getPrefixTableName())){
                    e.setPrefixTableName(fromItem.getName());
                }
                finalJoins.add(createJoin(tableToAlias,e));
            });
            plainSelect.setJoins(joins);
        }
        //重构join相关的sql
        if (CollectionUtil.isNotEmpty(joins)) {
            joinRebuildJoins(joins, ignoreTableList, joinChainEntity);
        }
        //重构sql中查询的列（其中可能包含子查询）
        List<SelectItem> selectItems = plainSelect.getSelectItems();
        joinBuildSelectItems(selectItems, ignoreTableList,  joinChainEntity);
    }

    private void buildTableToAliasMap(Map<String,String> tableToAlias, DataPermissionJoinEntity joinChainEntity){
        tableToAlias.put(joinChainEntity.getTableName(),joinChainEntity.getTableName()+new Random().nextInt(100));
    }

    private Join createJoin(Map<String,String> tableToAlias , DataPermissionJoinEntity joinChainEntity){
        Join join = new Join();
        EqualsTo selfEqualsTo = new EqualsTo();
        selfEqualsTo.setLeftExpression(new Column(tableToAlias.get(joinChainEntity.getPrefixTableName())+ "." + joinChainEntity.getPrefixField()));
        selfEqualsTo.setRightExpression(new Column(tableToAlias.get(joinChainEntity.getTableName())+"."+joinChainEntity.getField()));
        join.setOnExpression(selfEqualsTo);
        Table table = new Table();
        table.setAlias(new Alias(tableToAlias.get(joinChainEntity.getTableName())));
        table.setName(joinChainEntity.getTableName());
        join.setRightItem(table);
        switch (joinChainEntity.getJoinType()) {
            case JoinTypeConstans.LEFT:
                join.setLeft(true);
                break;
            case JoinTypeConstans.RIGHT:
                join.setRight(true);
                break;
            case JoinTypeConstans.INNER:
                join.setInner(true);
                break;
        }
        return join ;
    }


    private void joinRebuildJoins(List<Join> joins, List<String> ignoreTableList, List<DataPermissionJoinEntity> joinChainEntity) {
        for (Join e : joins) {
            if (e.getRightItem() instanceof SubSelect) {
                SubSelect select = (SubSelect) e.getRightItem();
                joinBuildSelectBody(select,ignoreTableList,joinChainEntity);
            }
        }
    }


    private void joinBuildSelectBody( SubSelect select, List<String> ignoreTableList, List<DataPermissionJoinEntity> joinChainEntity){
        SelectBody selectBody = select.getSelectBody();
        if (selectBody instanceof PlainSelect) {
            this.buildJoin((PlainSelect) selectBody,joinChainEntity, ignoreTableList );
        } else if (selectBody instanceof SetOperationList) {
            SetOperationList setOperationList = (SetOperationList) selectBody;
            List<SelectBody> selectBodyList = setOperationList.getSelects();
            selectBodyList.forEach(s -> this.buildJoin((PlainSelect) s,joinChainEntity, ignoreTableList));
        }
    }

    private void joinBuildSelectItems(List<SelectItem> selectItems, List<String> ignoreTableList, List<DataPermissionJoinEntity> joinChainEntity){
        selectItems.forEach(e->{
            if(e instanceof SelectExpressionItem){
                SelectExpressionItem item = (SelectExpressionItem) e;
                Expression expression = item.getExpression();
                if(expression instanceof SubSelect){
                    SubSelect select = (SubSelect)expression;
                    joinBuildSelectBody(select,ignoreTableList,joinChainEntity);
                }
            }
        });
    }
}