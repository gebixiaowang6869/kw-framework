package com.kw.framework.mybatis.service;

import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariPoolMXBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.management.JMX;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.sql.Connection;

@Component
@Slf4j
public class HikariMonitorService {


    @Resource
    private HikariDataSource hikariDataSource;


    @Autowired
    HikariDataSource hikaridatasource ;
    public HikariPoolMXBean HikariMonitor() {
        HikariPoolMXBean proxy = null;
        try {
            if (null == proxy) {
                MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
                ObjectName poolName = new ObjectName("com.zaxxer.hikari:type=Pool (" + hikariDataSource.getPoolName() + ")");
                proxy = JMX.newMXBeanProxy(mBeanServer, poolName, HikariPoolMXBean.class);
            } else {
                log.info("{}, {}, {}, {}", proxy.getTotalConnections(), proxy.getActiveConnections(), proxy.getIdleConnections(), proxy.getThreadsAwaitingConnection());
            }
        } catch (Throwable cause) {
            log.error("fail》》", cause);
        }
        log.info("{}, {}, {}, {}", proxy.getTotalConnections(), proxy.getActiveConnections(), proxy.getIdleConnections(), proxy.getThreadsAwaitingConnection());
        return proxy;
    }


}
