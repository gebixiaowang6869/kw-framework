package com.kw.framework.mybatis.entity;

import com.kw.framework.mybatis.constants.ExpressionConstants;
import lombok.Data;

import java.io.Serializable;

@Data
public class DataPermissionSetEntity implements Serializable {
    private ExpressionConstants expressionType ;
    private String fieldName ;
}
