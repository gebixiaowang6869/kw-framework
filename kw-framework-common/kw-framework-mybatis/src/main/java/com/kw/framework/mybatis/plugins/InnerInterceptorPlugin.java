package com.kw.framework.mybatis.plugins;

import com.baomidou.mybatisplus.extension.plugins.inner.InnerInterceptor;

import java.util.List;

/**
 * author: kw
 */
public interface InnerInterceptorPlugin {
    public List<InnerInterceptor> getInnerInterceptors();
}
