package com.kw.framework.mybatis.handler;


import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import com.kw.framework.common.croe.context.UserContext;
import com.kw.framework.common.croe.exception.TokenAccessFailException;
import com.kw.framework.mybatis.properties.TenantProperties;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.NullValue;

import java.util.List;

/**
 * 多租户控制器
 */
public class MultiTenantHandler implements TenantLineHandler {

    private final TenantProperties properties;

    public MultiTenantHandler(TenantProperties properties) {
        this.properties = properties;
    }

    /**
     * 获取租户 ID 值表达式，只支持单个 ID 值
     * <p>
     *
     * @return 租户 ID 值表达式
     */
    @Override
    public Expression getTenantId() {
        Integer tenantId = null ;
        try{
            tenantId = UserContext.getTenant();
        }catch (TokenAccessFailException e){
            return new NullValue();
        }
        if(tenantId==null){
            return new NullValue();
        }
        return new LongValue(tenantId);
    }

    /**
     * 获取租户字段名
     * <p>
     * 默认字段名叫: tenant_id
     *
     * @return 租户字段名
     */
    @Override
    public String getTenantIdColumn() {
        return properties.getColumn();
    }

    /**
     * 根据表名判断是否忽略拼接多租户条件
     * <p>
     * 默认都要进行解析并拼接多租户条件
     *
     * @param tableName 表名
     * @return 是否忽略, true:表示忽略，false:需要解析并拼接多租户条件
     */
    @Override
    public boolean ignoreTable(String tableName) {

        if( UserContext.getTenant() == null ){
            return true ;
        }

        //忽略指定用户对租户的数据过滤
        List<String> ignoreLoginNames=properties.getIgnoreLoginNames();
        String loginName = UserContext.getTenantName();

        if(null!=ignoreLoginNames && loginName!=null && ignoreLoginNames.contains(loginName)){
            return true;
        }

        //忽略指定表对租户数据的过滤
        List<String> ignoreTables = properties.getIgnoreTables();
        if (null != ignoreTables && ignoreTables.contains(tableName)) {
            return true;
        }

        return false;
    }
}