package com.kw.framework.mybatis.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wk
 */
@Data
@Builder
@AllArgsConstructor
public class DataPermissionJoinEntity implements Serializable {

    private String tableName ;

    private String field ;

    private String prefixField ;

    private String prefixTableName ;

    private String joinType ;


}
