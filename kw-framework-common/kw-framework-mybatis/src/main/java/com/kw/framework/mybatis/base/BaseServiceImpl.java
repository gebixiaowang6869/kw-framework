package com.kw.framework.mybatis.base;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kw.framework.common.croe.utils.AssertUtil;
import com.kw.framework.mybatis.entity.BaseEntity;

import java.util.Arrays;
import java.util.List;


public class BaseServiceImpl<M extends BaseMapper<T>, T extends BaseEntity> extends ServiceImpl<M, T> implements IBaseService<T> {


    @Override
    public void deleteByIds(String ids) {
        AssertUtil.assertNotEmpty(ids,"请传入需要删除的id");
        List<String> idList = Arrays.asList(ids.split(StrUtil.COMMA));
        this.removeByIds(idList);
    }

    @Override
    public void deleteByIdList(List<Long> idList) {
        this.removeByIds(idList);
    }

    @Override
    public Class<T> currentMapperClass() {
        return (Class)this.getResolvableType().as(BaseServiceImpl.class).getGeneric(new int[]{0}).getType();
    }
    @Override
    public Class<T> currentModelClass() {
        return (Class)this.getResolvableType().as(BaseServiceImpl.class).getGeneric(new int[]{1}).getType();
    }
}
