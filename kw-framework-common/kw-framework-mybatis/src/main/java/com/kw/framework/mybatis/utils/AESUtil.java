package com.kw.framework.mybatis.utils;

import com.kw.framework.common.croe.utils.interfaces.impl.CryptoUtil;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.Base64;


public class AESUtil{
    private static String key = "1234567890abcdef"; // 加密密钥

    public static String encrypt(String data) throws Exception {
        Key secretKey = new SecretKeySpec(key.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] encryptedData = cipher.doFinal(data.getBytes());
        return Base64.getEncoder().encodeToString(encryptedData); // 将二进制数据转化为Base64字符串
    }

    public static String decrypt(String encryptedData) throws Exception {
        System.out.println("----------------------111----------------------");
        Key secretKey = new SecretKeySpec(key.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decryptedData = cipher.doFinal(Base64.getDecoder().decode(encryptedData)); // 将Base64字符串转化为二进制数据
        return new String(decryptedData);
    }
}