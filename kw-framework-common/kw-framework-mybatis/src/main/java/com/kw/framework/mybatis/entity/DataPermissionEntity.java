package com.kw.framework.mybatis.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author wk
 */
@Data
public class DataPermissionEntity implements Serializable {

    public List<DataPermissionJoinEntity> joinEntityList;

    public List<DataPermissionFieldEntity> fieldEntityList ;
}
