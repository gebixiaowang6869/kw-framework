package com.kw.framework.mybatis.properties;

import java.util.Map;

public interface DataPermissionProperties {

    public Map<String,String> tables();

}
