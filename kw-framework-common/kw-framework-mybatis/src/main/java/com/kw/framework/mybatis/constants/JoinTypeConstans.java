package com.kw.framework.mybatis.constants;

/**
 * @author wk
 */
public class JoinTypeConstans {
    public final static String LEFT = "left";
    public final static String RIGHT = "right";
    public final static String INNER = "inner";
}
