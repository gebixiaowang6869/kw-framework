package com.kw.framework.mybatis.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author wk
 */
@Data
public class DataPermissionFieldEntity implements Serializable {
    private String expressionType ;
    private String fieldName ;
    private List<Long> ids ;
    /**
     * 此字段有值时只在此字段里有的表上会加上条件查询
     * 没值时则作用于全部表（出去DataPermission中指定忽略的表ignoreTables)
     */
    private List<String> useTables ;
}
