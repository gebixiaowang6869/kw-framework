package com.kw.framework.mybatis.config;


import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.InnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.TenantLineInnerInterceptor;
import com.kw.framework.common.croe.constant.CommonConstant;
import com.kw.framework.mybatis.handler.MultiTenantHandler;
import com.kw.framework.mybatis.handler.MybatisPlusMetaObjectHandler;
import com.kw.framework.mybatis.plugins.InnerInterceptorPlugin;
import com.kw.framework.mybatis.properties.TenantProperties;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;


/**
 * @Author kw
 * @Description 集成mybatisplus,注意扫包这里规定好
 *
 */
@Configuration
//@AutoConfigureAfter(InnerInterceptorPlugin.class)
@Slf4j
public class MybatisPlusConfig implements ApplicationContextAware {


    private ApplicationContext applicationContext ;


    private final static Long MAX_LIMIT = 1000L ;


    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer(){
        MapperScannerConfigurer scannerConfigurer = new MapperScannerConfigurer();
        scannerConfigurer.setBasePackage("com.kw.*.*.mapper");//这里自己读取配置文件获取package
        return scannerConfigurer;
    }

    @Bean
    @ConditionalOnMissingBean
    public IdentifierGenerator idGenerator() {
        return new CustomIdGenerator();
    }

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(TenantProperties tenantProperties) {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();

        if (Boolean.TRUE.equals(tenantProperties.getEnable())) {
            // 启用多租户拦截
            interceptor.addInnerInterceptor(new TenantLineInnerInterceptor(new MultiTenantHandler(tenantProperties)));
        }

        try{
            InnerInterceptorPlugin innerInterceptorPlugin = applicationContext.getBean(InnerInterceptorPlugin.class);
            List<InnerInterceptor> innerInterceptors = innerInterceptorPlugin.getInnerInterceptors();
            innerInterceptors.forEach(e->{
                interceptor.addInnerInterceptor(e);
                log.info("-----------------------"+e.toString()+"------------------");
            });

        }catch (NoSuchBeanDefinitionException e){
            log.info("-----------------no extra mybatis interceptor set!----------------") ;
        }

        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor(DbType.MYSQL);
        paginationInnerInterceptor.setMaxLimit(MAX_LIMIT);
        //分页插件
        interceptor.addInnerInterceptor(paginationInnerInterceptor);
        return interceptor;
    }



    @Bean
    public GlobalConfig globalConfig() {
        GlobalConfig globalConfig = new GlobalConfig();
        GlobalConfig.DbConfig dbConfig = new GlobalConfig.DbConfig();
        dbConfig.setLogicDeleteField("del_flag")
                .setLogicDeleteValue(String.valueOf(CommonConstant.DELETED_FLAG))
                .setLogicNotDeleteValue(String.valueOf(CommonConstant.UN_DELETE_FLAG));
        globalConfig.setDbConfig(dbConfig);
        return globalConfig;
    }

    /**
     * 审计字段自动填充
     * @return {@link MetaObjectHandler}
     */
    @Bean
    public MybatisPlusMetaObjectHandler mybatisPlusMetaObjectHandler() {
        return new MybatisPlusMetaObjectHandler();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext ;
    }
}
