package com.kw.framework.mybatis.handler;

import com.kw.framework.mybatis.utils.AESUtil;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 字段加密处理器 使用方式，在feild上加上@TableField(typeHandler=AESTypeHandler.class)
 */
@MappedTypes(value = { String.class })
@MappedJdbcTypes(value = JdbcType.VARCHAR)
public class AESTypeHandler extends BaseTypeHandler<String> {


    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, String parameter, JdbcType jdbcType) throws SQLException {
        try {
            String encryptedData = AESUtil.encrypt(parameter);
            ps.setString(i, encryptedData);
        } catch (Exception e) {
            throw new RuntimeException("Error encrypting parameter " + parameter, e);
        }
    }

    @Override
    public String getNullableResult(ResultSet rs, String columnName) throws SQLException {
        try {
            String encryptedData = rs.getString(columnName);
            if (encryptedData != null) {
                return AESUtil.decrypt(encryptedData);
            }
            return null;
        } catch (Exception e) {
            throw new RuntimeException("Error decrypting result for column " + columnName, e);
        }
    }

    @Override
    public String getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        try {
            String encryptedData = rs.getString(columnIndex);
            if (encryptedData != null) {
                return AESUtil.decrypt(encryptedData);
            }
            return null;
        } catch (Exception e) {
            throw new RuntimeException("Error decrypting result for column " + columnIndex, e);
        }
    }

    @Override
    public String getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        try {
            String encryptedData = cs.getString(columnIndex);
            if (encryptedData != null) {
                return AESUtil.decrypt(encryptedData);
            }
            return null;
        } catch (Exception e) {
            throw new RuntimeException("Error decrypting result for column " + columnIndex, e);
        }
    }
}